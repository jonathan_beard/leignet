# Visitors

- The visitor pattern in C++ is a bit odd given C++ doesn't quite have reflection. 
- If you're not familiar with the visitor pattern, [visitor pattern](https://en.wikipedia.org/wiki/Visitor_pattern).
- For our implementation the pattern starts with the call to visit instruction. 
```cpp
/**
 * use this to kick off the process. 
 */
void visit_instruction( trace_parser::node_base &node, 
                        trace_client::ref_t &ref,
                        trace_client::reference_list &list,
                        void *data );
```
- The visitor itself (called above) is implemented as follows:
```cpp
void 
trace_parser::visitor::visit_instruction( trace_parser::node_base      &node, 
                                          trace_client::ref_t          &ref, 
                                          trace_client::reference_list &list,
                                          void                         *data )
{
    
    /** 
     * We get the function pointer for the calling visitor with respect to 
     * the current instruction. The fun/interesting bit here is that the 
     * dominator for each instruction can change depending on what implementations
     * are available in each visitor. 
     */
    auto &dom_node = node.get_dominator_for_visitor( (*this) /** visitor **/ );
    /**
     * 
    dom_node.accept( (*this), ref, list, data );
    return;
}
```
- We first look up the node that has a visitor implementation for our current node (instruction). 
As an example, take the node graph from [node](node.md), if we encounter an _ldr_ instruction,
and our visitor has only implemented as far as _MemoryOp_ then the dominator node returned will be
that of a _MemoryOp_. With the node for memory op in hand, the visitor then calls the **accept** 
function of that node which in turn looks up the function pointer for this specific visitor (you can have
multiple visitors). 
```cpp
void 
trace_parser::node_base::accept( trace_parser::visitor &v, 
                                 trace_client::ref_t &ref, 
                                 trace_client::reference_list &ref_list,
                                 void *data )
{
    /** this is the visit **/
    per_visitor_node_visit_function[ v.visitor_id ]( (*this) /** type **/, 
                                                     ref, 
                                                     ref_list, 
                                                     data ); 
}

```
Note that the visitor\_id is used to lookup the correct visitor specific 
function to use. This variable is constant and statically defined when 
constructing a new derived visitor pass, no need to modify it. 
This one is a direct array look-up of a function pointer and call of that
function pointer. When you call the function pointer you're giving the function 
several parameters that it can use. 

- Specifically the function signature is:
```cpp
    using visit_func_t = std::function< void ( trace_parser::node_base&, 
                                               trace_client::ref_t&, 
                                               trace_client::reference_list&,
                                               void*) >;
```

where the arguments are:

| **node\_base**        | A reference to the dominator node for the current instruction (the one that actually has an implementation). | 
| :---                  | :---                                                                                                         | 
| **ref\_t**            | A reference to the current reference object. Those are defined [here](docs/ref.md)                           |
| **reference\_list**   | A reference list descripts an iterator structure, bsaically a list of instructions in the current memory frame. [here](include/reference_list.hpp) |
|  **\*void**           | tbd                                                                                                          | 


- When defining a new visitor for a given task, a good example to look at is the [trace\_visitor](./../parser/visitor/include/trace_visitor.hpp), 
let's walk through the implementation file (in src of same path). 

```cpp
trace_parser::trace_visitor::trace_visitor() : trace_parser::visitor()
{
    /** 
     * This function registers the hash function for each 
     * implemented node type within this visitor class and 
     * the static function that implements this functionality. 
     */
    register_function_type< trace_parser::node_base_node  >( 
        trace_parser::trace_visitor::visit_base 
    ); 
    register_function_type< trace_parser::instruction_node >(
        trace_parser::trace_visitor::visit_insn 
    );
    register_function_type< trace_parser::mem_scalar_node >( 
        trace_parser::trace_visitor::visit_mem_scalar 
    ); 
}

/**
 * base visitor definition, you should always have one of these at a minimum, 
 * and if it's the only visitor then all instructions will be mapped to here. 
 */
void 
trace_parser::trace_visitor::visit_base(   trace_parser::node_base &node, 
                                           trace_client::ref_t &ref, 
                                           trace_client::reference_list &list,
                                           void *data )
{
    UNUSED( node );
    UNUSED( list );
    auto *stream = reinterpret_cast< std::ostream* >( data );
    (*stream) << "ignoring non-mem op (token: " << std::hex << ref.token.all << std::dec << "): ";
    (*stream) << ref << "\n";
    /** this is the base one, doesn't really do much. **/
    return;
}


/**
 * basic instruction visitor - this one defines an "implementation" that
 * captures all non-memory operations. 
 */
void 
trace_parser::trace_visitor::visit_insn(   
                                trace_parser::node_base &node, 
                                trace_client::ref_t &ref, 
                                trace_client::reference_list &list,
                                void *data )
{
    UNUSED( node );
    UNUSED( list );
    auto *stream = reinterpret_cast< std::ostream* >( data );
    (*stream) << "caught by insn: " << ref << "\n";
    /** this is the base one, doesn't really do much. **/
    return;
}

/**
 * memory visitor, this one is interesting not because it catches/implements
 * all memory access (other than SIMD) but because it also utilizes the local
 * reference list object. 
 */
void 
trace_parser::trace_visitor::visit_mem_scalar( trace_parser::node_base  &node, 
                              trace_client::ref_t &ref, 
                              trace_client::reference_list &list,
                              void *data )
{
    UNUSED( node );
    UNUSED( list );
    auto *stream = reinterpret_cast< std::ostream* >( data );
    (*stream) << "caught by mem_scalar: " << ref << "\n";
    /** increment past memory references that come with the op **/
    /** 
     * If there is a memory instruction, the following reference will 
     * always be a field describing the access itself, the VA, if it's a 
     * read/write, etc.
     *
     * It should also be noted here that this additional field for memory
     * references will always follow on the same "record" and never be 
     * separated from the instruction that it is describing, this is 
     * handled in the client.cpp file. 
     */
    list.inc();
    return;
}
```


