# Nodes
- ISAs in general have "families" of instructions that implement more specific/or detailed functionality. 
- As an example, in general ISAs have ALU operations. Those ALU operations have two sub-classes, scalar and SIMD. 
These two sub-classes are then dividied into further classes, e.g., integer, floating point, etc. 
- We use this relationship to build a graph (see Perl script). 
- Our nodes are a description of the relationship of instructions to each other. 
![](../images/NodeRelationship.jpg)

- Each instruction is a "node" in a graph. 
- There is a defined relationship between instrucitons, i.e., instruction families
naturally inherit from each other. 
- Each instruction is defined by DynamoRIO in opcode files, ```opcode_api.h```,
you can grab the latest version of this from the dynamorio build directory (build 
directory given the aarch64 version is built dynamically with DynamoRIO. 
- We provide a Perl script in ```<root>/tracecollect/parser/node/tools```
called ```build_nodes.pl```. This script is called automatically when you
run _cmake_ on the build tree to build the collection tools. If you would 
like to modify the relationship between instructions, you simply modify 
the two structures ```node_tree_structure_non_terminals``` which defines the
non-terminal characters (non-leaf nodes) in the graph, and ```mem_instructions```. 
For _x86_ there are additional fields, ```x86_mov_insn_to_duplicate_as_mem``` that
should indicate which operations are duplicated across register move and memory
load/store operations depending on the operand types. 
- Final classification of instructions from the ```opcode_api.h``` files goes from 
the Perl script to a _csv_ intermediate. This intermediate can be used to ensure
that all instructions are labeled correctly before passing back to the Perl script
at runtime. 
- The structure ```node_tree_structure_non_terminals``` really defines the graph
and relationship between instructions. This graph can be visualized using 
the same Perl script using the open source utility GraphViz.

With respect to the image above:

1. The graph must define a base/root node.
2. The operations are dvided into families via ```build_nodes.pl```. In this
case we've defined _MemoryOp_ and _ALUOp_ as the internal nodes. 
3. More specific nodes are implemented that catch things such as all scalar 
memory operations. 
4. Lastly, you can catch in the visitor specific implementations. 
