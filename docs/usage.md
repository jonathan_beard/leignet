# How to use basic trace tool
- After you build the tool, in the ```build/src``` directory, there
is a binary called ```tracetool```. This simple tool enables the user
to collect a multithreaded trace with an arbitrary number of threads
simply by running:
```
./tracetool <binary name> <binary arguments>
```
The trace by default is sent to stdout, however this can be 
redirected to where the user would like by piping or capturing
the output. The user could also set an environmental parameter
to set a target file for the trace output as in 
```bash
export TRACE_OUTPUT="target_file.dat"
```
The user can also modify the trace instructions by constructing their
own visitor. By default the tool uses the "trace_visitor" class which
simply prints memory and non-memory instructions. 

# How to build a tool using the DynamoRIO plugin and IPC Bridge
- there are a few phases to this, one, the tool needs to launch
DynamoRIO along with the appropriate flags. Our Dynamorio plugin, 
_libtraceclient.so_ provides raw instruction trace information 
to a lock-free user-space IPC bridge. As an example, in our test
cases, we fork the application. One of the new processes will 
_exec_ DynamoRIO in order to launch it. The other will run the 
tool that we create. The code:

```cpp
    static const auto dr_bin_path( "/bin64/drrun" );
    static const std::string dr_client_flag( "-c" );
    /** 
     * DR_CLIENT_PATH must be set to where the libtraceclient.so is
     * located.
     */
    static const std::string  client_path( DR_CLIENT_PATH );

    auto *directory = getcwd( nullptr, 0 );
    
    std::stringstream application;
    /**
     * DR_DIR_PATH - must be set to where the dynamorio binary is located,
     * you could make this dynamic via env param with getenv() but currently
     * it is a compile time constant in all the examples. 
     */
    application << DR_DIR_PATH << dr_bin_path;
    std::stringstream client;
    client << "/" << client_path;
    
    std::stringstream test_app;
    test_app << directory << "/" << argv[ 0 ];

    /**
     * args
     *   0 - application name (dynamorio) - application.str().c_str();
     *   1 - -max_bb_instrs 
     *   2 - 64
     *   3 - -max_trace_bbs
     *   4 - 64
     *   5 - "-c" - dr_client_flag
     *   6 - client - client.str().c_str()
     *   7 - "-ipc_handle" - to tell the app that the next arg is the handle
     *   8 - varg - thehandle
     *   9 - "--" - separator 
     *  10 - test_app - test_app.str().c_str()
     *  11 - '1' - dummy argument for client to know it's the forked one
     *  12 - '\0'
     */
     char **args = nullptr;
     args = (char **) malloc( sizeof( char** ) * 13 );
     //MEMORY LEAK, but...we're exec'ing, so, not like it'll stick around
     args[ 0  ] = strdup( application.str().c_str() );
     args[ 1  ] = strdup( "-max_bb_instrs" );
     args[ 2  ] = strdup( "64" );
     args[ 3  ] = strdup( "-max_trace_bbs" );
     args[ 4  ] = strdup( "64" );
     args[ 5  ] = strdup( dr_client_flag.c_str() );
     args[ 6  ] = strdup( client.str().c_str() );
     args[ 7  ] = strdup( "-ipc_handle" );
     args[ 8  ] = strdup( std::to_string( ipc_handle ).c_str() );
     args[ 9  ] = strdup( "--" );
     args[ 10 ] = strdup( test_app.str().c_str() );
     /** 
      * here's where the arguments go, if you need more arguments you'll need 
      * to allocate more entries. There's an example of a more flexible
      * version in tracetool.cpp.
      */
     args[ 11 ] = strdup( "1" );
     args[ 12 ] = nullptr; 
     
     if( execve( application.str().c_str() /** dynamorio **/,
             (char * const*) args,
             envp ) == -1 )
     {
        std::cerr << "something really bad has happened, exit\n";
        exit( EXIT_FAILURE );
     }

```
When building your own client, it's important to note that 
**DR\_CLIENT\_PATH** must be set to the current DynamoRIO binary,
and **DR\_DIR\_PATH** should provide the path to our _libtraceclient.so_
library. On exec, this process becomes DynamoRIO which in turn 
launches your application. 

- Each thread that is launched on the application side gets a channel on the bridge that is 
named with the thread-id of that thread, although we provide an 
iterator for the user and compartmentalize quite a few things so 
likely this is a detail that doesn't necessarily matter. 
- Now for the tool side. For this it's probably easier to 
provide comments in a code example then follow-up with a discussion 
section. 
```cpp   
            /**
             * we encapsulate the tool side of this process
             * inside a driver. The driver should get the same
             * IPC handle that is given to DynamoRIO as an argument. 
             * The driver opens the IPC bridge, and manages the 
             * instruction traces coming from each thread. 
             */
            trace_client::driver d( ipc_handle );
            /**
             * the trace visitor is used to provide the 
             * actions of each instruction. We can have
             * multiple visitors defined, but we need to 
             * make sure we instantiate them here so we
             * can "pre-process" the node-list of instructions
             * prior to receiving traces. 
             */
            trace_parser::trace_visitor v;
            /** 
             * The node list is a list of instructions, basically
             * defininig a set of relationships between all the 
             * instructions in the instruction set architecture. 
             * The visitor class that is defined above only has 
             * to implement non-leaf instructions that are needed, 
             * e.g., you can choose to implement the "root" instruction
             * that everything derives from, or a memory op (that'll 
             * capture all the memory operations, or you can implement
             * specific actions for simd or scalar memory ops. The flexibility
             * of this approach derives from the fact that you only
             * have to provide some kind of action or function implementation
             * for what you need and not more. 
             */
            auto &node_list = d.get_node_list();
            v.pre_process_tree( node_list ); 
            
            d.init();

            bool threads_available = true;
            
            /**
             * basically this will be true as long as master 
             * thread in traced application is running, there
             * will always be at least one thread running. We'll
             * set thread_available = false once the thread_list
             * is emptied. 
             */
            while( threads_available )
            {
                /** 
                 * call to check the IPC buffer for new threads. You can receive a container with 
                 * new added threads or simply ignore this. The threads will be added to a driver
                 * internal list that can be iterated over using the syntax below.
                 * **check\_for\_threads** is thread-safe and protected by a mutex. 
                 */
                d.check_for_threads();
                /** 
                 * iterate for each thread we currently have. when threads are complete, 
                 * they'll be automatically removed by the **check\_completed\_threads**
                 * call at the end. It is important that this function is not placed inside
                 * the iterator list. 
                 */
                for( auto &th : d )
                {
                    trace_client::ref_t     r;
                    trace_parser::node_base *n = nullptr;
                    /** 
                     * This is basically zero copy, we're accessing the instruction directly from where
                     * it is defined up to the point where the ref_t structure is copied back to the 
                     * tool so that it can be accessed on follow-on operations (as an example, if you
                     * wanted to implement a re-order buffer, or instruciton queue. Likewise a pointer
                     * is passed to the node object that allows the user to keep a reference to the node
                     * type that the instruction represented by ref_t points to. This pointer can also 
                     * be stored for future usage in subsequent actions along with that ref_t (as an example,
                     * if you wanted to define a simple dispatch/execute/retire set of visitors, you could 
                     * do so. 
                     */
                    if( d.thread_has_next( th.first ) && d.get_next( th.first, &n, r ) )
                    {
                        v.visit_instruction( *n              /** node **/,
                                             r              /** reference **/,
                                             *th.second     /** ref_list **/,
                                             nullptr        /** additional data **/ );
                                             
                    }
                }
                /** 
                 * This (**check\_completed\_threads**) could be moved inside the "while" loop above, but it's 
                 * pulled out here for clarity. This function modifies the internal container of the driver so 
                 * it should be used outside the scope of any action using the container. This function is 
                 * thread-safe (meaning you can call it from multiple threads and nothing bad will happen. 
                 */
                threads_available = d.check_completed_threads();
            } /** end worker while loop, threads_available controls **/
            /**
             * technically this should just return, but just in case, wait till 
             * child  has finished then exit.
             */
            int status( 0 );
            waitpid( -1, &status, 0 );
            fprintf( stderr, "done\n" );
            //driver cleans up at end of scope
```            
