# Ref
- A reference is basically a cache-line (64B) aligned data packet that represents
an instruction or memory reference meta data or command from the application side. 
```cpp
enum mem_access_t : std::uint8_t {
    REF_TYPE_READ = 0,
    REF_TYPE_WRITE = 1,
};

struct ref_t 
{
    trace_client::token       token;

    /** 
     * r(0), w(1)
     * basically read = 0, write = 1, for tokens that map to 
     * insn_mem (described below).
     */
    std::uint8_t        dir             = 0; 
    /* opcode/insn size, e.g., always 4 for AArch64 */
    std::uint16_t       op_size         = 0; 
    /* memory size, e.g., width of access in bytes */
    std::uint16_t       mem_size        = 0; 
    /** address accessed **/
    std::uintptr_t      addr            = 0;
    /* mem ref addr or instr pc */
    std::uintptr_t      pc              = 0;
    char                op_name[ 16 ]   = {'\0'};
}; 
```

- A key feature of the ref\_t struct is the "token" field, this
consists of 
```cpp
union token
{
    using token_type_t= std::uint64_t;
    
    constexpr token() = default;

    constexpr token( const trace_client::cmds           &&cmd,
                     const trace_client::trace_type_t   &&t,
                     const std::int32_t                 &&op ) : 
                        cmd( cmd ),
                        type( t  ),
                        op( op ){}

    constexpr token( const token_type_t &&t ) : all( t ){}

#if   defined __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#elif defined __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpedantic"
#endif
    struct
    {
        /** 
         * command, everything under this is a subset, see 
         * specific fields in trace_client::cmds to see the 
         * sub-types. 
         */
         trace_client::cmds          cmd             = trace_client::cmd_not_set; 
        /**
         * this is another 8b unsigned field and the 
         * contents are subsets of the cmd (basically
         * more specific cmds. 
         */
         trace_client::trace_type_t  type            = trace_client::trace_type_not_set;
        /**
         * this isn't currently used, but, not to say 
         * that we won't need an additional 16b in the
         * future. 
         */
         std::uint16_t       reserved        = 0;
        /** opcode **/
         std::int32_t        op              = 0;
    }; 
    token_type_t       all;
#if   defined __GNUC__    
#pragma GCC diagnostic pop
#elif defined __clang__
#pragma clang diagnostic pop
#endif
};
```

- Within the token there are some additional key fields

| what | description    | 
| :--- | :---           |
| cmd | one of ( cmd\_not\_set, instruction, data\_operation, marker, or end\_of\_trace ). Each of these is described [here](../include/token.hpp) |
| type | one of either ( insn\_not\_mem, or insn\_mem ) | 
| reserved | for future usage |
| op | raw opcode |
