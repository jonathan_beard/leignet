# IPC Buffer
While the IPC buffer is part of a separate open-source library (Apache 2.0 license),
we will cover a bit of it here so the functions of the tooling end of the trace tool
can be better understood. 

## Summary

![](../images/ipcbufferbasic.jpg)

- The IPC buffer class allows lock-free zero-copy sharing between
multiple producers, multiple consumers and multiple threads through
"channels". 
- A channel represents one logical data stream, this stream can be
subscribed to by adding and opening a channel on the buffer as 
either a producer or consumer of that channel. The buffer can 
support an arbitrary number of simultaneous channels. 
- Adding channels currently is not lock free, threads must acquire
a lock (hidden in IPC library) before setting up a channel. 
- Users of a channel receive a thread-local storage segment. This 
facilitates allocation on the channel through the IPC buffer's 
built in heap allocation utility. 
- Individual allocations are limited to 1MiB in size, however, 
if needed, this can be increased. 
- While each record size is limited to 1MiB, the overall buffer size
is currently set to 1GiB, and expandable to an arbitrary size. The 
allocator allocates in 4KiB increments, although this too is hidden, 
the programmer can use the allocator just like any other malloc. 
- After the allocator, the buffer keeps a set of channel index 
linked list, this is currently protected by semaphores. These 
channel index structures maintain the state for each channel. While
The channel is logically contiguous it's actually interleaved 
amongst the main shared multi-process slab. 
- Each thread-local segment maintains credits for the channel, 
this means that allocations are lock-free as well while the producer
has credits (while de-allocation currently requires acquisition of a 
semaphore). This also means that receiving data only requires periodic
head/tail pointer checks. 
- There currently are two channel types that we care about, the 
shared segment and the spsc record channel type. This is because
logically only one thread will be processing a stream from one 
trace thread (this implies you can build a multi-threaded trace
tool rather easily with this setup). 
- Also, rather importantly, the IPC buffer also registers signal
handlers that cleanly terminate the shared memory segments, and 
semaphores on exception (and/or user termination). 

For the _traceclient_ we use one shared segment channel (by default 
this shared segment is set with the identifier in [sharedsegment](../include/shm_segment.hpp),
which implements the allocation function and several flags, specifically,
it sets:
```cpp
namespace trace_client
{
struct shm_segment
{
    shm_segment();
    ~shm_segment();
    
    static void init( void *ptr, const std::uint32_t num_cores );

    static void add_new_thread( trace_client::shm_segment *seg );

    static void remove_thread( trace_client::shm_segment *seg );

    std::uint64_t                   sim_time                =   0;
    std::uint64_t                   num_cycles              =   0;
    std::uint32_t                   num_cores               =   0;
    std::atomic< std::uint32_t >    trace_threads_attached  =   0; 
    std::atomic< std::uint32_t >    new_thread_flag         =   0;
};
} /** end namespace trace_client **/

```
## Details

### API Calls of Interest
```cpp
    auto *tls_producer      = ipc::buffer::get_tls_structure( buffer, thread_id );
```
### Thread Local Info
```cpp
struct thread_local_data
{
    /**
     * self explanatory, but, this semaphore
     * is to guard allocations and ensure 
     * buffers handed out are unique. 
     */
    ipc::sem::sem_obj_t allocate_semaphore;
    /**
     * index_semaphore - local copy of semaphore
     * that is really only needed when opening
     * and closing a new thread, this is set 
     * when you start-up and closed when you
     * exit, otherwise, don't touch this. 
     */
    ipc::sem::sem_obj_t index_semaphore;


    
    /**
     * calling thread id from the producer side,
     * not the consumer. 
     */
    ipc::thread_id_t thread_id = 0;
    /**
     * this is the thread local address space
     * correct pointer into the buffer. It's 
     * needed for so many things that we just
     * keep it here. 
     */
    ipc::buffer *buffer = nullptr;
    
    /**
     * contains all channels, including ones that
     * are shared segment channels. 
     */
    std::map< ipc::channel_id_t,
              ipc::channel_info* >  channel_map;
    
    /**
     * does not contain shared segment channels. 
     */
    std::map< ipc::channel_id_t, 
              ipc::local_allocation_info > channel_local_allocation;

};
```

```cpp
struct local_allocation_info
{
    local_allocation_info() = default;

    local_allocation_info( const ipc::direction_t d ) : dir( d ){}
    
    local_allocation_info( const local_allocation_info &other ) : 
        local_allocation( other.local_allocation ),
        blocks_available( other.blocks_available ),
        dir( other.dir ){}
    

    /** 
     * this is your free space that you're pushing into the 
     * FIFO. This region is lock-free. It is with respect
     * to block offset, not buffer offset (e.g., block modulo
     * block_size). So if your buffer is 1GiB, then you have
     * (1 << (30 - block_size)) offsets possible here.
     */
    ipc::ptr_offset_t    local_allocation   = ipc::invalid_ptr_offset;

    /**
     * this is a reference to how much space is available
     * that has already been allocated by this thread. If
     * This value is zero, more space must be allocated. 
     */
    std::size_t         blocks_available  = 0;
    ipc::direction_t    dir               = ipc::dir_not_set;
};
```

The most important 
