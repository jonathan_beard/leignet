/**
 * visitor.cpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "visitor.hpp"
#include "utildefs.hpp"
#include "utility.hpp"

#include <iostream>
#include <map>
#include <cassert>


trace_parser::visitor::id_t trace_parser::visitor::visitor_count = 0;

trace_parser::visitor::visitor() : visitor_id( trace_parser::visitor::visitor_count++ )
{
    assert( visitor_id < trace_parser::node_base::max_visitor_count );
    /** 
     * we did have a base node registered, but there are use cases 
     * where we would like to register custom functions with the 
     * node_base call type. 
     */
}


void 
trace_parser::visitor::visit_instruction( trace_parser::node_base      &node, 
                                          trace_client::ref_t          &ref, 
                                          trace_client::reference_list &list,
                                          void                         *data )
{
    
    auto &dom_node = node.get_dominator_for_visitor( (*this) /** visitor **/ );
    dom_node.accept( (*this), ref, list, data );
    return;
}


void
trace_parser::visitor::pre_process_tree( trace_parser::node_map_t &nodes )
{
    /** for each node in the tree **/
    for( auto &map_pair : nodes )
    {
        /** get pointer to node **/
        auto *node = map_pair.second;
        /** lookup closest relation to what functions are actually available **/
        const auto closest_node_to_impl /** this value is the class hash-type **/   = 
            trace_parser::node_base::relation_tree.get_closest_to( 
                map_pair.first /** curr node **/,
                func_map );
        /** 
         * by definition, if you've found a "closest node" then it was 
         * in func_map and therefore implemented, the implementation 
         * should be pointed ot by the "func_map_impl" which you then
         * pass to the dominator of that node on the graph. We use the 
         * term dominator here even though it's a tree....technically it
         * doesn't have to be a tree given multiple inheritance so dom
         * is appropriate we think. 
         */
        node->set_dominator_for_visitor( (*this) /** visitor **/,
                                         (*nodes[ closest_node_to_impl ]),
                                         func_map_impl[ closest_node_to_impl ]  );
    }
    /** exit scope **/
    return;
}
