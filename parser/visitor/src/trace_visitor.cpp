/**
 * trace_trace_visitor.cpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "trace_visitor.hpp"
#include "utildefs.hpp"
#include "utility.hpp"

trace_parser::trace_visitor::trace_visitor() : trace_parser::visitor()
{
    register_function_type< trace_parser::node_base_node  >( 
        trace_parser::trace_visitor::visit_base 
    ); 
    register_function_type< trace_parser::instruction_node >(
        trace_parser::trace_visitor::visit_insn 
    );
    register_function_type< trace_parser::mem_scalar_node >( 
        trace_parser::trace_visitor::visit_mem_scalar 
    ); 
    register_function_type< trace_parser::mem_simd_node >(
        trace_parser::trace_visitor::visit_mem_simd
    ); 
}

void 
trace_parser::trace_visitor::visit_base(   trace_parser::node_base &node, 
                                           trace_client::ref_t &ref, 
                                           trace_client::reference_list &list,
                                           void *data )
{
    UNUSED( node );
    UNUSED( list );
    auto *stream = reinterpret_cast< std::ostream* >( data );
    (*stream) << "ignoring non-mem op (token: " << std::hex << ref.token.all << std::dec << "): ";
    (*stream) << ref << "\n";
    /** this is the base one, doesn't really do much. **/
    return;
}

void 
trace_parser::trace_visitor::visit_insn(   
                                trace_parser::node_base &node, 
                                trace_client::ref_t &ref, 
                                trace_client::reference_list &list,
                                void *data )
{
    UNUSED( node );
    UNUSED( list );
    auto *stream = reinterpret_cast< std::ostream* >( data );
    (*stream) << "caught by insn: " << ref << "\n";
    /** this is the base one, doesn't really do much. **/
    return;
}
    
void 
trace_parser::trace_visitor::visit_mem_scalar( trace_parser::node_base  &node, 
                              trace_client::ref_t &ref, 
                              trace_client::reference_list &list,
                              void *data )
{
    UNUSED( node );
    UNUSED( list );
    auto *stream = reinterpret_cast< std::ostream* >( data );
    (*stream) << "caught by mem_scalar: " << ref << "\n";
    /** increment past memory references that come with the op **/
    list.inc();
    return;
}


void 
trace_parser::trace_visitor::visit_mem_simd( trace_parser::node_base &node, 
                                       trace_client::ref_t &ref, 
                                       trace_client::reference_list &list,
                                       void *data )
{
    UNUSED( node );
    UNUSED( list );
    auto *stream = reinterpret_cast< std::ostream* >( data );
    (*stream) << "caught by mem_simd: " << ref << "\n";
    /** increment past memory references that come with the op **/
    list.inc();
    return;
}
