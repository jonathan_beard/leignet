/**
 * visit_defs.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef VISIT_DEFS_HPP
#define VISIT_DEFS_HPP  1

#include <functional>
#include "reference.hpp"
#include "reference_list.hpp"

namespace trace_parser
{
    //prevent circular reference, delcare here
    class node_base;
    /**
     * all "visit" functions must have this signature. 
     */
    using visit_func_t = std::function< void ( trace_parser::node_base&, 
                                               trace_client::ref_t&, 
                                               trace_client::reference_list&,
                                               void*) >;

} /** end namespce trace_parser **/

#endif /* END VISIT_DEFS_HPP */
