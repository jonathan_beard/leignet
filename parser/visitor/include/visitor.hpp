/**
 * visitor.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef VISITOR_HPP
#define VISITOR_HPP  1
#include <cstdint>
#include <cassert>
#include "node_base.hpp"
#include "reference.hpp"
#include "function_map.hpp"
#include "getnodes.hpp"
#include "base_node_list.hpp"
#include "reference_list.hpp"

namespace trace_parser
{

/**
 * all subsequent visitors should derive this class. 
 */
class visitor
{
public:
    using id_t = std::int64_t;

    visitor();
    virtual ~visitor() = default;

    /**
     * use this to kick off the process. 
     */
    void visit_instruction( trace_parser::node_base &node, 
                            trace_client::ref_t &ref,
                            trace_client::reference_list &list,
                            void *data );


    /**
     * pre-populate the visitor tree. 
     */
    virtual void pre_process_tree( trace_parser::node_map_t &nodes );
    

    /** 
     * this is the "id" for this visitor, keep in mind 
     * the lookup array in node_base needs to change if it
     * goes greater than 16. 
     */
    const  id_t visitor_id; 

protected:
    template < class T > void register_function_type( trace_parser::visit_func_t f )
    {
        /** 
         * ignore the return, we don't care if two are inserted, we'll 
         * always call the derived type by design, we just need to know
         * that there's a catcher somewhere. 
         */
        T t;
        const auto ret = func_map.insert( t.get_hashcode() );
        assert( ret.second == true );
        func_map_impl.insert( std::make_pair( t.get_hashcode(), f ) );
    }
    
    static id_t visitor_count;

    trace_parser::function_map      func_map;
    trace_parser::function_map_impl func_map_impl;
};

} /** end namespace trace_parser **/
#endif /* END VISITOR_HPP */
