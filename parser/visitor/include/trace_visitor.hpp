/**
 * trace_visitor.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef TRACE_VISITOR_HPP
#define TRACE_VISITOR_HPP  1
#include "visitor.hpp"
namespace trace_parser
{
class trace_visitor : public visitor
{
public:
    /** 
     * constructor registers the visitor functions
     * we've defined below. 
     */

    trace_visitor();
    /** default destructor **/
    virtual ~trace_visitor() = default;
    
    /**
     * these are all called on double dispatch. 
     */
    static void 
    visit_base( trace_parser::node_base &node, 
                trace_client::ref_t &ref,
                trace_client::reference_list &list,
                void *data );

    /** 
     * visit_insn - basic visitor object. 
     */
    static  void 
    visit_insn(   trace_parser::node_base  &node, 
                  trace_client::ref_t &ref, 
                  trace_client::reference_list &list,
                  void *data );
    
    /**
     * special handling for mem_nodes
     */
    static void 
    visit_mem_scalar( trace_parser::node_base &node, 
                      trace_client::ref_t &ref, 
                      trace_client::reference_list &list,
                      void *data );

    static 
    void visit_mem_simd( trace_parser::node_base &node, 
                         trace_client::ref_t &ref, 
                         trace_client::reference_list &list,
                         void *data );

};

} /** end namespace trace_parser **/
#endif /* END TRACE_VISITOR_HPP */
