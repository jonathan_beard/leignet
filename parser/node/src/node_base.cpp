/**
 * node_base.cpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <utility>
#include <cassert>
#include "visitor.hpp"
#include "node_base.hpp"
#include <type_traits>

trace_parser::node_base::node_base() : 
    node_token( 0 )
{
    trace_parser::node_base::node_number++;
    //don't add relation again, no need
}

//this is the constructor that everyone else will call
trace_parser::node_base::node_base( const trace_client::token node_token ) : 
    node_token( node_token )
{
    trace_parser::node_base::node_number++;
    //don't add relation again, no need
}

trace_parser::node_base::~node_base()
{
    trace_parser::node_base::node_number--;
}

void 
trace_parser::node_base::accept( trace_parser::visitor &v, 
                                 trace_client::ref_t &ref, 
                                 trace_client::reference_list &ref_list,
                                 void *data )
{
    /** this is the visit **/
    per_visitor_node_visit_function[ v.visitor_id ]( (*this) /** type **/, 
                                                     ref, 
                                                     ref_list, 
                                                     data ); 
}

std::size_t
trace_parser::node_base::get_hashcode()
{
    return( (std::size_t) (this)->node_token.all );
}
    

void 
trace_parser::node_base::set_dominator_for_visitor( trace_parser::visitor &v, 
                                                    trace_parser::node_base &n,
                                                    trace_parser::visit_func_t &f )
{
    assert( v.visitor_id < trace_parser::node_base::max_visitor_count ); 
    //this is the node to jump to, or N
    parent_visitor_wormhole[ v.visitor_id ] = &n;
    //check to see if visitor function is populated, if not, populate it
    if( n.per_visitor_node_visit_function[ v.visitor_id ] == nullptr )
    {
        n.per_visitor_node_visit_function[ v.visitor_id ] = f;
    }
    return;
}
    
trace_parser::node_base&
trace_parser::node_base::get_dominator_for_visitor( trace_parser::visitor &v )
{
    auto *ptr = parent_visitor_wormhole[ v.visitor_id ];
    assert( ptr != nullptr );
    return( *ptr );
}

trace_parser::class_tree    trace_parser::node_base::relation_tree = {};
std::int64_t                trace_parser::node_base::node_number = 0;
