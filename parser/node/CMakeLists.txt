set( CMAKE_INCLUDE_CURRENT_DIR ON )
    
subproject_enable_includes()

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_FLAGS_RELEASE "-O3 -mtune=native")

include_directories( include )
add_subdirectory( src )

if( BUILD_AARCH64 )
message( STATUS "building Aarch64 instructions" )
execute_process( COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/tools/build_nodes.pl generate_nodes ${CMAKE_CURRENT_SOURCE_DIR}/tools/aarch64_categorized.csv aarch64 ${CMAKE_CURRENT_SOURCE_DIR}/include/base_node_list.hpp ${CMAKE_CURRENT_SOURCE_DIR}/src/getnodes.cpp )
elseif( BUILD_x86 )
message( STATUS "building x86 instructions" )
execute_process( COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/tools/build_nodes.pl generate_nodes ${CMAKE_CURRENT_SOURCE_DIR}/tools/x86_categorized.csv x86 ${CMAKE_CURRENT_SOURCE_DIR}/include/base_node_list.hpp ${CMAKE_CURRENT_SOURCE_DIR}/src/getnodes.cpp )
endif()




subproject_finalize_includes()
