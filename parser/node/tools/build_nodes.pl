#!/usr/bin/env perl
use strict;
use warnings;


sub expand_array($);
sub print_terminals_table($);
sub construct_array($);
sub arrayize_file($);
sub parse_x86_line($);
sub build_graph($$$$);

##
# params 
# 1) filename of condition codes,
# 2) hashmap for dynamorio opcode->pneumonic mapping
# 3) hashmap for category -> opcode mapping. 
## 
sub generate_tables($$$);
sub make_list_from_category_file($);

##
# params:
# cmd
# type
# opcode
# derived name
# my name
# is_mov_as_mem
# class_name_array 
##
sub get_node_string($$$$$$$);

##
# params:
# file to update
# node_class_list
##
sub update_base_node_list($$);

sub generate_inst_impl($);

##
# params:
# file to update
# node_class_names
##
sub update_get_node_function($$);

my %node_tree_structure_non_terminals =
(
    "node_base"         => ["instruction"],
    "instruction"       => ["maint",    "scalar",   "simd",         "jump", 
                            "loop",     "convert",  "cpu_state",    "io", 
                            "nop",      "atomic",   "barrier",      "accel"  ],
    "cpu_state"         => ["interrupt","stop","wait","exception"],
    "scalar"            => ["alu_scalar", "mem_scalar", "mov_scalar", 
                            "conditional_scalar" ],
    "atomic"            => ["atomic_scalar", "atomic_simd"],
    "mov_scalar"        => ["convert_scalar"],
    "conditional_scalar" => ["cond_mov"],
    "simd"              => ["alu_simd", "mem_simd", "mov_simd", "conditional_simd" ],
    "mov_simd"          => ["mov_pack_unpack", "convert_simd"],
    "mem_scalar"        => ["prefetch", "mem_barrier", "exclusive", "atomic_swap",
                            "nontemporal_mem_scalar", "call"],
    "mem_simd"          => ["gather", "scatter"]                            
);

##
# these must be nonterminals
##
my %mem_instructions = 
(
    "mem_scalar"=>0,   
    "atomic_scalar"=>0,    
    "atomic_simd"=>0,  
    "mem_simd"=>0, 
    "mem_barrier"=>0,  
    "exclusive"=>0,        
    "atomic_swap"=>0,  
    "nontemporal_mem_scalar"=>0, 
    "gather"=>0,       
    "scatter"=>0,          
    "prefetch"=>0,
    "call"=>0
);

##
# x86 duplicates quite a few ops with
# mem/register variants. SIMD don't seem
# to follow this same trend. 
##
my %x86_mov_insn_to_duplicate_as_mem = 
(
    "mov_scalar"=>"mem_scalar", 
    "convert_scalar"=>"mem_scalar", 
    "cond_mov"=>"mem_scalar", 
    "mov_simd"=>"mem_simd", 
    "mov_pack_unpack"=>"mem_scalar",
    "conditional_scalar" => "mem_scalar",
    "alu_scalar" => "mem_scalar"
);

##
# the key must be a non-terminal character defined above, 
# the terminals are the data fields, the data field itself
# must be an array, inside that array, array references 
# reference ranges of fields, individual numbers reference 
# single instructions. Each of these, range or singleton
# will be generated as a sub-class of the non-terminal. 
##
my %opcode_hash;
my %category_hash;

##
# right now let's just get this working. 
## 
my ( $op, $file, $arch, $base_file, $getnode_file) = @ARGV;


if( $op eq "generate_nodes" )
{
if( $arch eq "aarch64"  )
{
    ## 
    # add aarch64 specific non-terminals, the LHS must correspond
    # to a non-terminal, the range represents terminals that will
    # be generated under these non-terminals. 
    ##
    generate_tables( $file, \%opcode_hash, \%category_hash ); 
    my ($class_string_decl, $class_string_names ) = 
    build_graph( \%node_tree_structure_non_terminals,
                 \%opcode_hash,
                 \%category_hash,
                 0 );
    update_base_node_list( $base_file, $class_string_decl );
    update_get_node_function( $getnode_file, $class_string_names );
}

elsif( $arch eq "x86" )
{
    ## 
    # add x86 specific non-terminals, the LHS must correspond
    # to a non-terminal, the range represents terminals that will
    # be generated under these non-terminals. 
    ##
    generate_tables( $file, \%opcode_hash, \%category_hash ); 
    my ($class_string_decl, $class_string_names ) = 
    build_graph( \%node_tree_structure_non_terminals,
                 \%opcode_hash,
                 \%category_hash,
                 1 );
    update_base_node_list( $base_file, $class_string_decl );
    update_get_node_function( $getnode_file, $class_string_names );
}
else
{
    print( "Unknown architecture, please check and try again.\n" );
    exit( -1 );
}
} ## end generate nodes
elsif( $op eq "print_opcode_csv" ) # make printed list to go to excel
{
    construct_array( arrayize_file( $file) );
}
elsif( $op eq "print_graphviz" )
{
    #construct terminals just like in node printing
    # print graphviz
    # exit
    # print_terminals_table( \%terminals_dictionary );
}
exit( 0 );


##
# expand the (2,5) style ranges into multiple values, 
# return list as an array reference. 
##
sub expand_array($) { [ ( $_[0]->[ 0 ]..$_[0]->[ -1 ] ) ] }

sub flatten($){
    my @output = ();
    foreach my $var ( @{ $_[0] } )
    {
        if( ref($var) eq 'ARRAY' )
        {
            push( @output, @{ &expand_array( $var ) } );
        }
        else
        {
            push( @output, $var );
        }
    }
    return( \@output );
}

##
# simply print all the terminal values in each node
# category. 
##
sub print_terminals_table($)
{
    foreach my $k ( sort keys %{ $_[0] } )
    {
        print "$k -> { ".join( ", ", @{ &flatten( $_[0]->{ $k } ) } )." }\n";
    }
}

sub arrayize_file($)
{
    chomp( $_[0] );
    open(FH,'<',$_[0]);
    my @arr = <FH>;
    close(FH);
    return( \@arr );
}

sub construct_array($)
{
    my $lines = shift;
    my $found = 0;
    for( my $i = 0; $i < @{ $lines } + 0 ; $i++ )
    {
        chomp( $lines->[ $i ] );
        if( $lines->[ $i ] eq "enum {" )
        { 
            $found = 1; 
        }
        elsif( $lines->[ $i ] eq "};" )
        {     
            $found = 0; 
        }
        elsif( $found == 1 )
        { 
            my $line = parse_x86_line( $lines->[ $i ]."\n" );
            if( length( $line ) > 0 )
            {
                print $line."\n";
            }
        }
    }
}

sub parse_x86_line($)
{
##
# /*   4 */ OP_add,      /**< IA-32/AMD64 add opcode. */
##
my $line = shift;
$line =~ /\/\*\s+(?<found>\d+)\s+\*\/\s+OP_(?<opcode>\S+),/;
if( $+{found} )
{ 
    return( lc( "$+{found},$+{opcode}") );
}
else
{
    return( "" );
}
}


sub generate_tables($$$)
{
    my ($filename, $opcode_hashmap, $category_hashmap ) = @_;
    my $categorized_list = make_list_from_category_file( $filename );
    foreach my $entry ( @{ $categorized_list } )
    {
        ##
        # map opcode at index 0 to pneumonic
        ##
        $opcode_hashmap->{ int($entry->[ 0 ]) } = $entry->[ 1 ];
        ##
        # map category to opcode, but, remember, opcode
        # is an array, must be an array. 
        ##
        if( ! exists( $category_hashmap->{ $entry->[ 2 ] } ) )
        {
            $category_hashmap->{ $entry->[ 2 ] } = [ int($entry->[ 0 ]) ];
        }
        else
        {
            push( @{ $category_hashmap->{ $entry->[ 2 ] } }, int($entry->[ 0 ]) );
        }            
    }
}

sub make_list_from_category_file($)
{
    chomp( $_[0] );
    open(FH,'<',$_[0]);
    my @arr = <FH>;
    close(FH);
    my @output;
    ## discard first line
    shift @arr;
    foreach my $line ( @arr )
    {
        ##
        # get rid of newline 
        ##
        chomp( $line );
        $line =~ s/\s+//g;
        my @temp = split( ',', $line );
        push( @output, \@temp );
    }
    return( \@output );
}


sub build_graph($$$$)
{
    ##
    # if mov_as_mem == 1, then all
    # mov_simd are duplicatedd with appropriate flag under mem_simd
    # mov_scalar are duplicated with appropriate flag under mem_scalar
    ##
    my @output;
    my @class_names;
    my( $nonterminal, $opcode_hash, $cat_hash, $mov_as_mem  ) = @_;
    my %allcats;
    ##
    # fill queue
    ##
    my $base = "node_base";

    my @next_queue;
    my @nt_queue = @{ $nonterminal->{ $base } };
    my $nont_opcode_key = -1;
    while( @nt_queue + 0 > 0 )
    {
        my $derived = shift( @nt_queue );
        push( @next_queue, $derived );
        $allcats{ $base     } = 1;
        $allcats{ $derived  } = 1;
        ##
        # params:
        # cmd
        # type
        # opcode
        # derived name
        # my name
        ##
        my $class_str = get_node_string("trace_client::instruction",
                                        "trace_client::insn_not_mem",
                                        $nont_opcode_key,
                                        $base,
                                        $derived,
                                        0,
                                        \@class_names );
        $nont_opcode_key -= 1;
        push( @output, $class_str );
        if( @nt_queue + 0 == 0 )
        {
            do
            {
                my $next = shift( @next_queue );
                if( exists( $nonterminal->{ $next } ) )
                {
                    $base = $next;
                    push( @nt_queue, @{ $nonterminal->{ $next } } );
                    goto OUT;
                }
                else
                {
                    $allcats{ $next  } = 1;
                }

            }while( @next_queue + 0 > 0 );
OUT:            
        }
    }
    ##
    # double check to make sure all instructions are 
    # added to a category and that we have all the 
    # categories here. 
    ##
    foreach my $opcode_cat( keys %{ $cat_hash } ){ 
        if( not exists( $allcats{ $opcode_cat } ) ){ 
            print "error: \'$opcode_cat\' doesn't exist in our map, exiting.\n";
            exit( -1 );
        }
    }
    ## 
    # now that we have that out of the way, generate edges
    # for each terminal in the graph. 
    ##
    foreach my $cat( sort keys %{ $cat_hash } )
    {
        foreach my $terminal( @{ $cat_hash->{ $cat } } )
        {
            #print "$cat -> $opcode_hash->{ $terminal };\n"; 
            # params:
            # cmd
            # type
            # opcode
            # derived name
            # my name
            ##
            my $type = "trace_client::insn_not_mem"; 
            my $duplicate_mov = 0;
            if( exists( $mem_instructions{ $cat } ) )
            {
                $type = "trace_client::insn_mem";
            }
            if( $mov_as_mem == 1 && exists( $x86_mov_insn_to_duplicate_as_mem{ $cat } ) )
            {
                $duplicate_mov = 1; 
            }

            my $class_str = get_node_string("trace_client::instruction",
                                            $type,
                                            $terminal, 
                                            $cat,
                                            $opcode_hash->{ $terminal },
                                            0, 
                                            \@class_names );
            push( @output, $class_str );
            if( $duplicate_mov == 1 )
            {
                $class_str = get_node_string("trace_client::instruction",
                                             "trace_client::insn_mem",
                                             $terminal, 
                                             $x86_mov_insn_to_duplicate_as_mem{ $cat },
                                             $opcode_hash->{ $terminal },
                                             1,
                                             \@class_names );
                push( @output, $class_str );
            }
        }
    }
    return( \@output, \@class_names );
}

##
# params:
# cmd
# type
# opcode
# derived name
# my name
# is_mov_as_mem
# array with just class name
##
sub get_node_string($$$$$$$)
{
    my ( $cmd, $type, $opcode, $derived, $self, $mov_as_mem, $class_name_arr ) = @_;
    my $class_name = "";
    if( $mov_as_mem == 1 )
    {
        $class_name = "$self\_mov\_as\_mem\_node";
    }
    else
    {
        $class_name = "$self\_node";
    }
    my $str = 
    "using $class_name  = trace_parser::generic_node< 
                          trace_client::build_token< $cmd,
                                                     $type,
                                                     $opcode >(),
                                                     $derived\_node /** derives **/ >;";

    push( @{ $class_name_arr }, $class_name );
    return( $str );
}

##
# params:
# file to update
# node_class_list
##
sub update_base_node_list($$)
{
my $start = "BEGIN AUTO-POPULATED REGION";
my $end   = "END AUTO_POPULATED REGION";
my ($file, $list) = @_;

chomp( $file );
#input
open(FH,'<',$file);
my @arr = <FH>;
close(FH);

my $line_index = 0;

my $skip = 0;

open(FH,'>',$file);
for( ; $line_index < (@arr + 0); $line_index++ )
{
    print FH $arr[ $line_index ], if( $skip == 0 );
    if( $arr[ $line_index ] =~ /$start/ )
    { 
        $skip = 1;
        foreach my $class ( @{ $list } )
        {
            print FH $class."\n";
        }
    }
    elsif( $arr[ $line_index ] =~ /$end/ )
    {
        $skip = 0;
        print FH $arr[ $line_index ];
    }
}
close(FH);
}


sub generate_inst_impl($)
{
    my ($name) = @_;
    my $output = "      auto *$name\_obj  = new trace_parser::$name();\n";
       $output.= "      output.insert( std::make_pair( $name\_obj->node_token.all, 
                                                       $name\_obj ) );\n";
    return( $output );
}

##
# params:
# file to update
# node_class_names
##
sub update_get_node_function($$)
{

my $start = "AUTOGENERATED, FILLED EXTENSION NODES GO HERE"; 
my $end   = "END AUTOGENERATED EXTENSION NODES";
my ($file, $list) = @_;

chomp( $file );
#input
open(FH,'<',$file);
my @arr = <FH>;
close(FH);

my $line_index = 0;

my $skip = 0;

open(FH,'>',$file);
for( ; $line_index < (@arr + 0); $line_index++ )
{
    print FH $arr[ $line_index ], if( $skip == 0 );
    if( $arr[ $line_index ] =~ /$start/ )
    { 
        $skip = 1;
        foreach my $class ( @{ $list } )
        {
            print FH generate_inst_impl( $class );
        }
    }
    elsif( $arr[ $line_index ] =~ /$end/ )
    {
        $skip = 0;
        print FH $arr[ $line_index ];
    }
}
close(FH);
}
