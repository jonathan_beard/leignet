/**
 * getnodes.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef GETNODES_HPP
#define GETNODES_HPP  1
#include <unordered_map>
#include <map>
#include "node_base.hpp"
#include "token.hpp"

namespace trace_parser
{

using node_map_t = 
    std::unordered_map< trace_client::token::token_type_t, trace_parser::node_base* >;

struct helper
{
/**
 * this function will be both manually and automatically 
 * generated. The base nodes we'll add manually, the rest
 * are auto-generated from a perl script. Specifically the
 * really cool thing about this is that this function gives
 * the programmer/parser the ability to jump directly into 
 * the instruction within the tree, then from there jump
 * directly to the most 'detailed' implementation available
 * for the visitor class. So, two lookups then direct execution
 * that allows the implementer flexibility of implementing a new
 * trace tool or simulation core without having to implement 
 * all the instructions and also the flexibility to basically 
 * swap out the visitor to swap out the core. 
 */
static node_map_t get_nodes();  
};

} /** end namespace trace_parser **/
#endif /* END GETNODES_HPP */
