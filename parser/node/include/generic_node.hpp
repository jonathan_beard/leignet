/**
 * generic_node.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef GENERIC_NODE_HPP
#define GENERIC_NODE_HPP  1
#include "node_base.hpp"
#include <iostream>

namespace trace_parser
{

template < trace_client::token::token_type_t t, 
           class derives_from > class generic_node : public derives_from
{
public:
    generic_node() : derives_from( trace_client::token( t ) )
    {
        //fprintf( stderr, "%s: %" PRIu8 " - %" PRIu8 " - %" PRIu16 " - %" PRIi32 "\n", 
        //    typeid( (*this) ).name(),
        //    (this)->node_token.cmd,
        //    (this)->node_token.type,
        //    (this)->node_token.reserved,
        //    (this)->node_token.op );
        //FIXME - if we're doing this we probably need to redesign the node
        //structure, this could all be constexpr and build at compile time, 
        //just needs about a week more work. - jcb 20 Oct. 2021
        derives_from x;
        const auto base_class       = x.get_hashcode();;
        const auto derived_class    = (std::size_t)t;

        trace_parser::node_base::relation_tree.add_relation( base_class, derived_class );
    }

    generic_node( const trace_client::token tok ) : derives_from( tok )
    {
        // basically pass through        
    }

    constexpr static std::size_t token_hash = (std::size_t) t;
};

} /** end namespace trace_parser **/

#endif /* END GENERIC_NODE_HPP */
