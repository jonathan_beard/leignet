/**
 * node_base.hpp - These nodes really just give you the relationship
 * between instructions. 
 *
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NODE_BASE_HPP
#define NODE_BASE_HPP  1

#include <array>
#include "token.hpp"
#include "class_tree.hpp"
#include "reference.hpp"
#include "reference_list.hpp"
#include "visit_defs.hpp"

namespace trace_parser
{
/** pre-declare, found in default_visitor.hpp **/
class visitor;

class node_base
{
public:
    
    node_base();

    node_base( const trace_client::token node_token );

    virtual ~node_base();

    /** 
     * accept - this function is called which then dispatches
     * to the right visitor function (note, the base visitor
     * function is always the first one to be called. 
     */
    void accept( trace_parser::visitor  &v, 
                 trace_client::ref_t    &ref, 
                 trace_client::reference_list &ref_list,
                 void *data );

    /** 
     * get_hashcode - returns the hashcode for the dervied class calling
     * this function. 
     */
    virtual std::size_t get_hashcode() final;
    
    /**
     * set_dominator_for_visitor - for this node, this is the node that implements 
     * the closest instruction for this particular visitor. 
     */
    void set_dominator_for_visitor( trace_parser::visitor &v, 
                                    trace_parser::node_base &n,
                                    trace_parser::visit_func_t &f);
    
    /**
     * get_dominator_for_visitor - returns the node that is the closest implementation 
     * availalbe within the given visitor. 
     */
    trace_parser::node_base&
        get_dominator_for_visitor( trace_parser::visitor &v );
    

    

    /** variables, data structures **/
    /**
     * this is the token taken from the parse stream, first 
     * 64b of the reference, defined in reference.hpp. It is 
     * const, because for these node structures it will never
     * change after instantiation. Default values are basically
     * fine here so, base with no initialization will call 
     * default constructor. 
     */
    const trace_client::token node_token;


    /**
     * this is the class "relation" tree that defines the 
     * relationship between instructions. There is only one
     * for all the instruction types, but within this tree, 
     * we define all the relationships. This tree is indexed
     * on the class hash-code type, so the values here may 
     * not be constant across different machines and compilation
     * instances, so, don't try to write them down and use them
     * statically.
     */
    static trace_parser::class_tree relation_tree; 
    
    static constexpr std::int8_t max_visitor_count = 64;

protected:


    /** 
     * this structure is to enable direct mapping from the actual
     * instruction to the node type which the visitor implements. 
     * e.g., you have at hand a 'strex' instruction, but you've only
     * implemented a visitor for 'str', then hitting the 'strex' 
     * pushes you directly to the 'str' and that visitor impl is 
     * called for 'strex'
     */
    std::array< trace_parser::node_base*, max_visitor_count >
        parent_visitor_wormhole;
    
    /**
     * this structure contains the visitor function to call
     * from the pre-processed visitor map. So basically you 
     * go from leaf-node->wormhole->implemented_node->function.
     */
    std::array< trace_parser::visit_func_t, max_visitor_count >
        per_visitor_node_visit_function = { nullptr };

    static std::int64_t node_number;

};


} /** end namespace trace_parser **/
#endif /* END NODE_BASE_HPP */
