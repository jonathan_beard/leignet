/**
 * base_node_list.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef BASE_NODE_LIST_HPP
#define BASE_NODE_LIST_HPP  1
#include "generic_node.hpp"
#include "ipc_cmds.hpp"

namespace trace_parser
{

using node_base_node  = trace_parser::node_base;



/** BEGIN AUTO-POPULATED REGION **/
using instruction_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -1 >(),
                                                     node_base_node /** derives **/ >;
using maint_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -2 >(),
                                                     instruction_node /** derives **/ >;
using scalar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -3 >(),
                                                     instruction_node /** derives **/ >;
using simd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -4 >(),
                                                     instruction_node /** derives **/ >;
using jump_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -5 >(),
                                                     instruction_node /** derives **/ >;
using loop_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -6 >(),
                                                     instruction_node /** derives **/ >;
using convert_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -7 >(),
                                                     instruction_node /** derives **/ >;
using cpu_state_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -8 >(),
                                                     instruction_node /** derives **/ >;
using io_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -9 >(),
                                                     instruction_node /** derives **/ >;
using nop_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -10 >(),
                                                     instruction_node /** derives **/ >;
using atomic_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -11 >(),
                                                     instruction_node /** derives **/ >;
using barrier_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -12 >(),
                                                     instruction_node /** derives **/ >;
using accel_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -13 >(),
                                                     instruction_node /** derives **/ >;
using alu_scalar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -14 >(),
                                                     scalar_node /** derives **/ >;
using mem_scalar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -15 >(),
                                                     scalar_node /** derives **/ >;
using mov_scalar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -16 >(),
                                                     scalar_node /** derives **/ >;
using conditional_scalar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -17 >(),
                                                     scalar_node /** derives **/ >;
using alu_simd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -18 >(),
                                                     simd_node /** derives **/ >;
using mem_simd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -19 >(),
                                                     simd_node /** derives **/ >;
using mov_simd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -20 >(),
                                                     simd_node /** derives **/ >;
using conditional_simd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -21 >(),
                                                     simd_node /** derives **/ >;
using interrupt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -22 >(),
                                                     cpu_state_node /** derives **/ >;
using stop_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -23 >(),
                                                     cpu_state_node /** derives **/ >;
using wait_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -24 >(),
                                                     cpu_state_node /** derives **/ >;
using exception_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -25 >(),
                                                     cpu_state_node /** derives **/ >;
using atomic_scalar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -26 >(),
                                                     atomic_node /** derives **/ >;
using atomic_simd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -27 >(),
                                                     atomic_node /** derives **/ >;
using prefetch_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -28 >(),
                                                     mem_scalar_node /** derives **/ >;
using mem_barrier_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -29 >(),
                                                     mem_scalar_node /** derives **/ >;
using exclusive_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -30 >(),
                                                     mem_scalar_node /** derives **/ >;
using atomic_swap_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -31 >(),
                                                     mem_scalar_node /** derives **/ >;
using nontemporal_mem_scalar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -32 >(),
                                                     mem_scalar_node /** derives **/ >;
using call_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -33 >(),
                                                     mem_scalar_node /** derives **/ >;
using convert_scalar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -34 >(),
                                                     mov_scalar_node /** derives **/ >;
using cond_mov_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -35 >(),
                                                     conditional_scalar_node /** derives **/ >;
using gather_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -36 >(),
                                                     mem_simd_node /** derives **/ >;
using scatter_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -37 >(),
                                                     mem_simd_node /** derives **/ >;
using mov_pack_unpack_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -38 >(),
                                                     mov_simd_node /** derives **/ >;
using convert_simd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     -39 >(),
                                                     mov_simd_node /** derives **/ >;
using aesimc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     624 >(),
                                                     accel_node /** derives **/ >;
using aesenc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     625 >(),
                                                     accel_node /** derives **/ >;
using aesenclast_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     626 >(),
                                                     accel_node /** derives **/ >;
using aesdec_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     627 >(),
                                                     accel_node /** derives **/ >;
using aesdeclast_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     628 >(),
                                                     accel_node /** derives **/ >;
using aeskeygenassist_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     629 >(),
                                                     accel_node /** derives **/ >;
using vaeskeygenassist_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     874 >(),
                                                     accel_node /** derives **/ >;
using sha1msg1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1404 >(),
                                                     accel_node /** derives **/ >;
using sha1msg2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1405 >(),
                                                     accel_node /** derives **/ >;
using sha1nexte_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1406 >(),
                                                     accel_node /** derives **/ >;
using sha1rnds4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1407 >(),
                                                     accel_node /** derives **/ >;
using sha256msg1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1408 >(),
                                                     accel_node /** derives **/ >;
using sha256msg2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1409 >(),
                                                     accel_node /** derives **/ >;
using sha256rnds2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1410 >(),
                                                     accel_node /** derives **/ >;
using crc32_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     576 >(),
                                                     accel_node /** derives **/ >;
using add_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     4 >(),
                                                     alu_scalar_node /** derives **/ >;
using add_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     4 >(),
                                                     mem_scalar_node /** derives **/ >;
using or_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     5 >(),
                                                     alu_scalar_node /** derives **/ >;
using or_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     5 >(),
                                                     mem_scalar_node /** derives **/ >;
using adc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     6 >(),
                                                     alu_scalar_node /** derives **/ >;
using adc_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     6 >(),
                                                     mem_scalar_node /** derives **/ >;
using sbb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     7 >(),
                                                     alu_scalar_node /** derives **/ >;
using sbb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     7 >(),
                                                     mem_scalar_node /** derives **/ >;
using and_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     8 >(),
                                                     alu_scalar_node /** derives **/ >;
using and_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     8 >(),
                                                     mem_scalar_node /** derives **/ >;
using daa_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     9 >(),
                                                     alu_scalar_node /** derives **/ >;
using daa_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     9 >(),
                                                     mem_scalar_node /** derives **/ >;
using sub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     10 >(),
                                                     alu_scalar_node /** derives **/ >;
using sub_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     10 >(),
                                                     mem_scalar_node /** derives **/ >;
using das_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     11 >(),
                                                     alu_scalar_node /** derives **/ >;
using das_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     11 >(),
                                                     mem_scalar_node /** derives **/ >;
using xor_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     12 >(),
                                                     alu_scalar_node /** derives **/ >;
using xor_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     12 >(),
                                                     mem_scalar_node /** derives **/ >;
using aaa_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     13 >(),
                                                     alu_scalar_node /** derives **/ >;
using aaa_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     13 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     14 >(),
                                                     alu_scalar_node /** derives **/ >;
using cmp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     14 >(),
                                                     mem_scalar_node /** derives **/ >;
using aas_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     15 >(),
                                                     alu_scalar_node /** derives **/ >;
using aas_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     15 >(),
                                                     mem_scalar_node /** derives **/ >;
using inc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     16 >(),
                                                     alu_scalar_node /** derives **/ >;
using inc_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     16 >(),
                                                     mem_scalar_node /** derives **/ >;
using dec_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     17 >(),
                                                     alu_scalar_node /** derives **/ >;
using dec_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     17 >(),
                                                     mem_scalar_node /** derives **/ >;
using imul_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     25 >(),
                                                     alu_scalar_node /** derives **/ >;
using imul_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     25 >(),
                                                     mem_scalar_node /** derives **/ >;
using test_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     60 >(),
                                                     alu_scalar_node /** derives **/ >;
using test_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     60 >(),
                                                     mem_scalar_node /** derives **/ >;
using aam_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     80 >(),
                                                     alu_scalar_node /** derives **/ >;
using aam_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     80 >(),
                                                     mem_scalar_node /** derives **/ >;
using aad_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     81 >(),
                                                     alu_scalar_node /** derives **/ >;
using aad_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     81 >(),
                                                     mem_scalar_node /** derives **/ >;
using bt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     185 >(),
                                                     alu_scalar_node /** derives **/ >;
using bt_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     185 >(),
                                                     mem_scalar_node /** derives **/ >;
using shld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     186 >(),
                                                     alu_scalar_node /** derives **/ >;
using shld_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     186 >(),
                                                     mem_scalar_node /** derives **/ >;
using bts_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     188 >(),
                                                     alu_scalar_node /** derives **/ >;
using bts_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     188 >(),
                                                     mem_scalar_node /** derives **/ >;
using shrd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     189 >(),
                                                     alu_scalar_node /** derives **/ >;
using shrd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     189 >(),
                                                     mem_scalar_node /** derives **/ >;
using btr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     192 >(),
                                                     alu_scalar_node /** derives **/ >;
using btr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     192 >(),
                                                     mem_scalar_node /** derives **/ >;
using btc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     197 >(),
                                                     alu_scalar_node /** derives **/ >;
using btc_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     197 >(),
                                                     mem_scalar_node /** derives **/ >;
using bsf_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     198 >(),
                                                     alu_scalar_node /** derives **/ >;
using bsf_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     198 >(),
                                                     mem_scalar_node /** derives **/ >;
using bsr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     199 >(),
                                                     alu_scalar_node /** derives **/ >;
using bsr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     199 >(),
                                                     mem_scalar_node /** derives **/ >;
using xadd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     201 >(),
                                                     alu_scalar_node /** derives **/ >;
using xadd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     201 >(),
                                                     mem_scalar_node /** derives **/ >;
using rol_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     253 >(),
                                                     alu_scalar_node /** derives **/ >;
using rol_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     253 >(),
                                                     mem_scalar_node /** derives **/ >;
using ror_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     254 >(),
                                                     alu_scalar_node /** derives **/ >;
using ror_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     254 >(),
                                                     mem_scalar_node /** derives **/ >;
using rcl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     255 >(),
                                                     alu_scalar_node /** derives **/ >;
using rcl_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     255 >(),
                                                     mem_scalar_node /** derives **/ >;
using rcr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     256 >(),
                                                     alu_scalar_node /** derives **/ >;
using rcr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     256 >(),
                                                     mem_scalar_node /** derives **/ >;
using shl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     257 >(),
                                                     alu_scalar_node /** derives **/ >;
using shl_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     257 >(),
                                                     mem_scalar_node /** derives **/ >;
using shr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     258 >(),
                                                     alu_scalar_node /** derives **/ >;
using shr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     258 >(),
                                                     mem_scalar_node /** derives **/ >;
using sar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     259 >(),
                                                     alu_scalar_node /** derives **/ >;
using sar_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     259 >(),
                                                     mem_scalar_node /** derives **/ >;
using not_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     260 >(),
                                                     alu_scalar_node /** derives **/ >;
using not_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     260 >(),
                                                     mem_scalar_node /** derives **/ >;
using neg_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     261 >(),
                                                     alu_scalar_node /** derives **/ >;
using neg_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     261 >(),
                                                     mem_scalar_node /** derives **/ >;
using mul_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     262 >(),
                                                     alu_scalar_node /** derives **/ >;
using mul_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     262 >(),
                                                     mem_scalar_node /** derives **/ >;
using div_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     263 >(),
                                                     alu_scalar_node /** derives **/ >;
using div_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     263 >(),
                                                     mem_scalar_node /** derives **/ >;
using idiv_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     264 >(),
                                                     alu_scalar_node /** derives **/ >;
using idiv_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     264 >(),
                                                     mem_scalar_node /** derives **/ >;
using sqrtps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     325 >(),
                                                     alu_scalar_node /** derives **/ >;
using sqrtps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     325 >(),
                                                     mem_scalar_node /** derives **/ >;
using sqrtss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     326 >(),
                                                     alu_scalar_node /** derives **/ >;
using sqrtss_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     326 >(),
                                                     mem_scalar_node /** derives **/ >;
using sqrtpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     327 >(),
                                                     alu_scalar_node /** derives **/ >;
using sqrtpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     327 >(),
                                                     mem_scalar_node /** derives **/ >;
using sqrtsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     328 >(),
                                                     alu_scalar_node /** derives **/ >;
using sqrtsd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     328 >(),
                                                     mem_scalar_node /** derives **/ >;
using rsqrtps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     329 >(),
                                                     alu_scalar_node /** derives **/ >;
using rsqrtps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     329 >(),
                                                     mem_scalar_node /** derives **/ >;
using rsqrtss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     330 >(),
                                                     alu_scalar_node /** derives **/ >;
using rsqrtss_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     330 >(),
                                                     mem_scalar_node /** derives **/ >;
using rcpps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     331 >(),
                                                     alu_scalar_node /** derives **/ >;
using rcpps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     331 >(),
                                                     mem_scalar_node /** derives **/ >;
using rcpss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     332 >(),
                                                     alu_scalar_node /** derives **/ >;
using rcpss_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     332 >(),
                                                     mem_scalar_node /** derives **/ >;
using andps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     333 >(),
                                                     alu_scalar_node /** derives **/ >;
using andps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     333 >(),
                                                     mem_scalar_node /** derives **/ >;
using andpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     334 >(),
                                                     alu_scalar_node /** derives **/ >;
using andpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     334 >(),
                                                     mem_scalar_node /** derives **/ >;
using andnps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     335 >(),
                                                     alu_scalar_node /** derives **/ >;
using andnps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     335 >(),
                                                     mem_scalar_node /** derives **/ >;
using andnpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     336 >(),
                                                     alu_scalar_node /** derives **/ >;
using andnpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     336 >(),
                                                     mem_scalar_node /** derives **/ >;
using orps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     337 >(),
                                                     alu_scalar_node /** derives **/ >;
using orps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     337 >(),
                                                     mem_scalar_node /** derives **/ >;
using orpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     338 >(),
                                                     alu_scalar_node /** derives **/ >;
using orpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     338 >(),
                                                     mem_scalar_node /** derives **/ >;
using xorps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     339 >(),
                                                     alu_scalar_node /** derives **/ >;
using xorps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     339 >(),
                                                     mem_scalar_node /** derives **/ >;
using xorpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     340 >(),
                                                     alu_scalar_node /** derives **/ >;
using xorpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     340 >(),
                                                     mem_scalar_node /** derives **/ >;
using addps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     341 >(),
                                                     alu_scalar_node /** derives **/ >;
using addps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     341 >(),
                                                     mem_scalar_node /** derives **/ >;
using addss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     342 >(),
                                                     alu_scalar_node /** derives **/ >;
using addss_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     342 >(),
                                                     mem_scalar_node /** derives **/ >;
using addpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     343 >(),
                                                     alu_scalar_node /** derives **/ >;
using addpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     343 >(),
                                                     mem_scalar_node /** derives **/ >;
using addsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     344 >(),
                                                     alu_scalar_node /** derives **/ >;
using addsd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     344 >(),
                                                     mem_scalar_node /** derives **/ >;
using mulps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     345 >(),
                                                     alu_scalar_node /** derives **/ >;
using mulps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     345 >(),
                                                     mem_scalar_node /** derives **/ >;
using mulss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     346 >(),
                                                     alu_scalar_node /** derives **/ >;
using mulss_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     346 >(),
                                                     mem_scalar_node /** derives **/ >;
using mulpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     347 >(),
                                                     alu_scalar_node /** derives **/ >;
using mulpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     347 >(),
                                                     mem_scalar_node /** derives **/ >;
using mulsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     348 >(),
                                                     alu_scalar_node /** derives **/ >;
using mulsd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     348 >(),
                                                     mem_scalar_node /** derives **/ >;
using fadd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     399 >(),
                                                     alu_scalar_node /** derives **/ >;
using fadd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     399 >(),
                                                     mem_scalar_node /** derives **/ >;
using fmul_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     400 >(),
                                                     alu_scalar_node /** derives **/ >;
using fmul_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     400 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcom_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     401 >(),
                                                     alu_scalar_node /** derives **/ >;
using fcom_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     401 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcomp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     402 >(),
                                                     alu_scalar_node /** derives **/ >;
using fcomp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     402 >(),
                                                     mem_scalar_node /** derives **/ >;
using fsub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     403 >(),
                                                     alu_scalar_node /** derives **/ >;
using fsub_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     403 >(),
                                                     mem_scalar_node /** derives **/ >;
using fsubr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     404 >(),
                                                     alu_scalar_node /** derives **/ >;
using fsubr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     404 >(),
                                                     mem_scalar_node /** derives **/ >;
using fdiv_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     405 >(),
                                                     alu_scalar_node /** derives **/ >;
using fdiv_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     405 >(),
                                                     mem_scalar_node /** derives **/ >;
using fdivr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     406 >(),
                                                     alu_scalar_node /** derives **/ >;
using fdivr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     406 >(),
                                                     mem_scalar_node /** derives **/ >;
using fiadd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     414 >(),
                                                     alu_scalar_node /** derives **/ >;
using fiadd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     414 >(),
                                                     mem_scalar_node /** derives **/ >;
using fimul_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     415 >(),
                                                     alu_scalar_node /** derives **/ >;
using fimul_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     415 >(),
                                                     mem_scalar_node /** derives **/ >;
using ficom_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     416 >(),
                                                     alu_scalar_node /** derives **/ >;
using ficom_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     416 >(),
                                                     mem_scalar_node /** derives **/ >;
using ficomp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     417 >(),
                                                     alu_scalar_node /** derives **/ >;
using ficomp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     417 >(),
                                                     mem_scalar_node /** derives **/ >;
using fisub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     418 >(),
                                                     alu_scalar_node /** derives **/ >;
using fisub_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     418 >(),
                                                     mem_scalar_node /** derives **/ >;
using fisubr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     419 >(),
                                                     alu_scalar_node /** derives **/ >;
using fisubr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     419 >(),
                                                     mem_scalar_node /** derives **/ >;
using fidiv_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     420 >(),
                                                     alu_scalar_node /** derives **/ >;
using fidiv_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     420 >(),
                                                     mem_scalar_node /** derives **/ >;
using fidivr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     421 >(),
                                                     alu_scalar_node /** derives **/ >;
using fidivr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     421 >(),
                                                     mem_scalar_node /** derives **/ >;
using fabs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     433 >(),
                                                     alu_scalar_node /** derives **/ >;
using fabs_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     433 >(),
                                                     mem_scalar_node /** derives **/ >;
using fxam_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     435 >(),
                                                     alu_scalar_node /** derives **/ >;
using fxam_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     435 >(),
                                                     mem_scalar_node /** derives **/ >;
using f2xm1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     443 >(),
                                                     alu_scalar_node /** derives **/ >;
using f2xm1_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     443 >(),
                                                     mem_scalar_node /** derives **/ >;
using fyl2x_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     444 >(),
                                                     alu_scalar_node /** derives **/ >;
using fyl2x_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     444 >(),
                                                     mem_scalar_node /** derives **/ >;
using fptan_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     445 >(),
                                                     alu_scalar_node /** derives **/ >;
using fptan_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     445 >(),
                                                     mem_scalar_node /** derives **/ >;
using fpatan_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     446 >(),
                                                     alu_scalar_node /** derives **/ >;
using fpatan_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     446 >(),
                                                     mem_scalar_node /** derives **/ >;
using fprem1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     448 >(),
                                                     alu_scalar_node /** derives **/ >;
using fprem1_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     448 >(),
                                                     mem_scalar_node /** derives **/ >;
using fdecstp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     449 >(),
                                                     alu_scalar_node /** derives **/ >;
using fdecstp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     449 >(),
                                                     mem_scalar_node /** derives **/ >;
using fincstp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     450 >(),
                                                     alu_scalar_node /** derives **/ >;
using fincstp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     450 >(),
                                                     mem_scalar_node /** derives **/ >;
using fprem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     451 >(),
                                                     alu_scalar_node /** derives **/ >;
using fprem_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     451 >(),
                                                     mem_scalar_node /** derives **/ >;
using fyl2xp1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     452 >(),
                                                     alu_scalar_node /** derives **/ >;
using fyl2xp1_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     452 >(),
                                                     mem_scalar_node /** derives **/ >;
using fsqrt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     453 >(),
                                                     alu_scalar_node /** derives **/ >;
using fsqrt_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     453 >(),
                                                     mem_scalar_node /** derives **/ >;
using fsincos_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     454 >(),
                                                     alu_scalar_node /** derives **/ >;
using fsincos_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     454 >(),
                                                     mem_scalar_node /** derives **/ >;
using frndint_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     455 >(),
                                                     alu_scalar_node /** derives **/ >;
using frndint_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     455 >(),
                                                     mem_scalar_node /** derives **/ >;
using fscale_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     456 >(),
                                                     alu_scalar_node /** derives **/ >;
using fscale_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     456 >(),
                                                     mem_scalar_node /** derives **/ >;
using fsin_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     457 >(),
                                                     alu_scalar_node /** derives **/ >;
using fsin_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     457 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcos_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     458 >(),
                                                     alu_scalar_node /** derives **/ >;
using fcos_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     458 >(),
                                                     mem_scalar_node /** derives **/ >;
using fucompp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     463 >(),
                                                     alu_scalar_node /** derives **/ >;
using fucompp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     463 >(),
                                                     mem_scalar_node /** derives **/ >;
using fucomi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     470 >(),
                                                     alu_scalar_node /** derives **/ >;
using fucomi_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     470 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcomi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     471 >(),
                                                     alu_scalar_node /** derives **/ >;
using fcomi_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     471 >(),
                                                     mem_scalar_node /** derives **/ >;
using fucom_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     473 >(),
                                                     alu_scalar_node /** derives **/ >;
using fucom_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     473 >(),
                                                     mem_scalar_node /** derives **/ >;
using fucomp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     474 >(),
                                                     alu_scalar_node /** derives **/ >;
using fucomp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     474 >(),
                                                     mem_scalar_node /** derives **/ >;
using faddp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     475 >(),
                                                     alu_scalar_node /** derives **/ >;
using faddp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     475 >(),
                                                     mem_scalar_node /** derives **/ >;
using fmulp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     476 >(),
                                                     alu_scalar_node /** derives **/ >;
using fmulp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     476 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcompp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     477 >(),
                                                     alu_scalar_node /** derives **/ >;
using fcompp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     477 >(),
                                                     mem_scalar_node /** derives **/ >;
using fsubrp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     478 >(),
                                                     alu_scalar_node /** derives **/ >;
using fsubrp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     478 >(),
                                                     mem_scalar_node /** derives **/ >;
using fsubp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     479 >(),
                                                     alu_scalar_node /** derives **/ >;
using fsubp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     479 >(),
                                                     mem_scalar_node /** derives **/ >;
using fdivrp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     480 >(),
                                                     alu_scalar_node /** derives **/ >;
using fdivrp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     480 >(),
                                                     mem_scalar_node /** derives **/ >;
using fdivp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     481 >(),
                                                     alu_scalar_node /** derives **/ >;
using fdivp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     481 >(),
                                                     mem_scalar_node /** derives **/ >;
using fucomip_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     482 >(),
                                                     alu_scalar_node /** derives **/ >;
using fucomip_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     482 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcomip_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     483 >(),
                                                     alu_scalar_node /** derives **/ >;
using fcomip_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     483 >(),
                                                     mem_scalar_node /** derives **/ >;
using pavgusb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     499 >(),
                                                     alu_scalar_node /** derives **/ >;
using pavgusb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     499 >(),
                                                     mem_scalar_node /** derives **/ >;
using popcnt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     539 >(),
                                                     alu_scalar_node /** derives **/ >;
using popcnt_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     539 >(),
                                                     mem_scalar_node /** derives **/ >;
using lzcnt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     544 >(),
                                                     alu_scalar_node /** derives **/ >;
using lzcnt_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     544 >(),
                                                     mem_scalar_node /** derives **/ >;
using ptest_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     548 >(),
                                                     alu_scalar_node /** derives **/ >;
using ptest_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     548 >(),
                                                     mem_scalar_node /** derives **/ >;
using pcmpeqq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     556 >(),
                                                     alu_scalar_node /** derives **/ >;
using pcmpeqq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     556 >(),
                                                     mem_scalar_node /** derives **/ >;
using pextrb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     577 >(),
                                                     alu_scalar_node /** derives **/ >;
using pextrb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     577 >(),
                                                     mem_scalar_node /** derives **/ >;
using pextrd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     578 >(),
                                                     alu_scalar_node /** derives **/ >;
using pextrd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     578 >(),
                                                     mem_scalar_node /** derives **/ >;
using roundss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     582 >(),
                                                     alu_scalar_node /** derives **/ >;
using roundss_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     582 >(),
                                                     mem_scalar_node /** derives **/ >;
using roundsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     583 >(),
                                                     alu_scalar_node /** derives **/ >;
using roundsd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     583 >(),
                                                     mem_scalar_node /** derives **/ >;
using bextr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1041 >(),
                                                     alu_scalar_node /** derives **/ >;
using bextr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1041 >(),
                                                     mem_scalar_node /** derives **/ >;
using blcfill_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1042 >(),
                                                     alu_scalar_node /** derives **/ >;
using blcfill_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1042 >(),
                                                     mem_scalar_node /** derives **/ >;
using blci_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1043 >(),
                                                     alu_scalar_node /** derives **/ >;
using blci_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1043 >(),
                                                     mem_scalar_node /** derives **/ >;
using blcic_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1044 >(),
                                                     alu_scalar_node /** derives **/ >;
using blcic_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1044 >(),
                                                     mem_scalar_node /** derives **/ >;
using blcmsk_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1045 >(),
                                                     alu_scalar_node /** derives **/ >;
using blcmsk_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1045 >(),
                                                     mem_scalar_node /** derives **/ >;
using blcs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1046 >(),
                                                     alu_scalar_node /** derives **/ >;
using blcs_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1046 >(),
                                                     mem_scalar_node /** derives **/ >;
using blsfill_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1047 >(),
                                                     alu_scalar_node /** derives **/ >;
using blsfill_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1047 >(),
                                                     mem_scalar_node /** derives **/ >;
using blsic_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1048 >(),
                                                     alu_scalar_node /** derives **/ >;
using blsic_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1048 >(),
                                                     mem_scalar_node /** derives **/ >;
using t1mskc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1049 >(),
                                                     alu_scalar_node /** derives **/ >;
using t1mskc_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1049 >(),
                                                     mem_scalar_node /** derives **/ >;
using tzmsk_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1050 >(),
                                                     alu_scalar_node /** derives **/ >;
using tzmsk_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1050 >(),
                                                     mem_scalar_node /** derives **/ >;
using andn_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1055 >(),
                                                     alu_scalar_node /** derives **/ >;
using andn_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1055 >(),
                                                     mem_scalar_node /** derives **/ >;
using blsr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1056 >(),
                                                     alu_scalar_node /** derives **/ >;
using blsr_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1056 >(),
                                                     mem_scalar_node /** derives **/ >;
using blsmsk_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1057 >(),
                                                     alu_scalar_node /** derives **/ >;
using blsmsk_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1057 >(),
                                                     mem_scalar_node /** derives **/ >;
using blsi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1058 >(),
                                                     alu_scalar_node /** derives **/ >;
using blsi_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1058 >(),
                                                     mem_scalar_node /** derives **/ >;
using tzcnt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1059 >(),
                                                     alu_scalar_node /** derives **/ >;
using tzcnt_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1059 >(),
                                                     mem_scalar_node /** derives **/ >;
using bzhi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1060 >(),
                                                     alu_scalar_node /** derives **/ >;
using bzhi_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1060 >(),
                                                     mem_scalar_node /** derives **/ >;
using pext_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1061 >(),
                                                     alu_scalar_node /** derives **/ >;
using pext_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1061 >(),
                                                     mem_scalar_node /** derives **/ >;
using pdep_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1062 >(),
                                                     alu_scalar_node /** derives **/ >;
using pdep_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1062 >(),
                                                     mem_scalar_node /** derives **/ >;
using sarx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1063 >(),
                                                     alu_scalar_node /** derives **/ >;
using sarx_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1063 >(),
                                                     mem_scalar_node /** derives **/ >;
using shlx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1064 >(),
                                                     alu_scalar_node /** derives **/ >;
using shlx_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1064 >(),
                                                     mem_scalar_node /** derives **/ >;
using shrx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1065 >(),
                                                     alu_scalar_node /** derives **/ >;
using shrx_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1065 >(),
                                                     mem_scalar_node /** derives **/ >;
using rorx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1066 >(),
                                                     alu_scalar_node /** derives **/ >;
using rorx_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1066 >(),
                                                     mem_scalar_node /** derives **/ >;
using mulx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1067 >(),
                                                     alu_scalar_node /** derives **/ >;
using mulx_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1067 >(),
                                                     mem_scalar_node /** derives **/ >;
using adox_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1105 >(),
                                                     alu_scalar_node /** derives **/ >;
using adox_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1105 >(),
                                                     mem_scalar_node /** derives **/ >;
using adcx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1106 >(),
                                                     alu_scalar_node /** derives **/ >;
using adcx_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1106 >(),
                                                     mem_scalar_node /** derives **/ >;
using vpmullq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1324 >(),
                                                     alu_scalar_node /** derives **/ >;
using vpmullq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1324 >(),
                                                     mem_scalar_node /** derives **/ >;
using syscall_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     95 >(),
                                                     alu_scalar_node /** derives **/ >;
using syscall_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     95 >(),
                                                     mem_scalar_node /** derives **/ >;
using sysret_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     97 >(),
                                                     alu_scalar_node /** derives **/ >;
using sysret_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     97 >(),
                                                     mem_scalar_node /** derives **/ >;
using lea_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     61 >(),
                                                     alu_scalar_node /** derives **/ >;
using lea_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     61 >(),
                                                     mem_scalar_node /** derives **/ >;
using punpcklbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     126 >(),
                                                     alu_simd_node /** derives **/ >;
using punpcklwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     127 >(),
                                                     alu_simd_node /** derives **/ >;
using punpckldq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     128 >(),
                                                     alu_simd_node /** derives **/ >;
using packsswb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     129 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpgtb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     130 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpgtw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     131 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpgtd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     132 >(),
                                                     alu_simd_node /** derives **/ >;
using packuswb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     133 >(),
                                                     alu_simd_node /** derives **/ >;
using punpckhbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     134 >(),
                                                     alu_simd_node /** derives **/ >;
using punpckhwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     135 >(),
                                                     alu_simd_node /** derives **/ >;
using punpckhdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     136 >(),
                                                     alu_simd_node /** derives **/ >;
using packssdw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     137 >(),
                                                     alu_simd_node /** derives **/ >;
using punpcklqdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     138 >(),
                                                     alu_simd_node /** derives **/ >;
using punpckhqdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     139 >(),
                                                     alu_simd_node /** derives **/ >;
using pshufw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     144 >(),
                                                     alu_simd_node /** derives **/ >;
using pshufd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     145 >(),
                                                     alu_simd_node /** derives **/ >;
using pshufhw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     146 >(),
                                                     alu_simd_node /** derives **/ >;
using pshuflw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     147 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpeqb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     148 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpeqw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     149 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpeqd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     150 >(),
                                                     alu_simd_node /** derives **/ >;
using psrlw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     206 >(),
                                                     alu_simd_node /** derives **/ >;
using psrld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     207 >(),
                                                     alu_simd_node /** derives **/ >;
using psrlq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     208 >(),
                                                     alu_simd_node /** derives **/ >;
using paddq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     209 >(),
                                                     alu_simd_node /** derives **/ >;
using pmullw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     210 >(),
                                                     alu_simd_node /** derives **/ >;
using psubusb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     212 >(),
                                                     alu_simd_node /** derives **/ >;
using psubusw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     213 >(),
                                                     alu_simd_node /** derives **/ >;
using pminub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     214 >(),
                                                     alu_simd_node /** derives **/ >;
using pand_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     215 >(),
                                                     alu_simd_node /** derives **/ >;
using paddusb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     216 >(),
                                                     alu_simd_node /** derives **/ >;
using paddusw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     217 >(),
                                                     alu_simd_node /** derives **/ >;
using pmaxub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     218 >(),
                                                     alu_simd_node /** derives **/ >;
using pandn_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     219 >(),
                                                     alu_simd_node /** derives **/ >;
using pavgb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     220 >(),
                                                     alu_simd_node /** derives **/ >;
using psraw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     221 >(),
                                                     alu_simd_node /** derives **/ >;
using psrad_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     222 >(),
                                                     alu_simd_node /** derives **/ >;
using pavgw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     223 >(),
                                                     alu_simd_node /** derives **/ >;
using pmulhuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     224 >(),
                                                     alu_simd_node /** derives **/ >;
using pmulhw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     225 >(),
                                                     alu_simd_node /** derives **/ >;
using psubsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     228 >(),
                                                     alu_simd_node /** derives **/ >;
using psubsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     229 >(),
                                                     alu_simd_node /** derives **/ >;
using pminsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     230 >(),
                                                     alu_simd_node /** derives **/ >;
using por_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     231 >(),
                                                     alu_simd_node /** derives **/ >;
using paddsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     232 >(),
                                                     alu_simd_node /** derives **/ >;
using paddsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     233 >(),
                                                     alu_simd_node /** derives **/ >;
using pmaxsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     234 >(),
                                                     alu_simd_node /** derives **/ >;
using pxor_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     235 >(),
                                                     alu_simd_node /** derives **/ >;
using psllw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     236 >(),
                                                     alu_simd_node /** derives **/ >;
using pslld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     237 >(),
                                                     alu_simd_node /** derives **/ >;
using psllq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     238 >(),
                                                     alu_simd_node /** derives **/ >;
using pmuludq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     239 >(),
                                                     alu_simd_node /** derives **/ >;
using pmaddwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     240 >(),
                                                     alu_simd_node /** derives **/ >;
using psadbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     241 >(),
                                                     alu_simd_node /** derives **/ >;
using psubb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     244 >(),
                                                     alu_simd_node /** derives **/ >;
using psubw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     245 >(),
                                                     alu_simd_node /** derives **/ >;
using psubd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     246 >(),
                                                     alu_simd_node /** derives **/ >;
using psubq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     247 >(),
                                                     alu_simd_node /** derives **/ >;
using paddb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     248 >(),
                                                     alu_simd_node /** derives **/ >;
using paddw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     249 >(),
                                                     alu_simd_node /** derives **/ >;
using paddd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     250 >(),
                                                     alu_simd_node /** derives **/ >;
using psrldq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     251 >(),
                                                     alu_simd_node /** derives **/ >;
using pslldq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     252 >(),
                                                     alu_simd_node /** derives **/ >;
using unpcklps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     299 >(),
                                                     alu_simd_node /** derives **/ >;
using unpcklpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     300 >(),
                                                     alu_simd_node /** derives **/ >;
using unpckhps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     301 >(),
                                                     alu_simd_node /** derives **/ >;
using unpckhpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     302 >(),
                                                     alu_simd_node /** derives **/ >;
using ucomiss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     319 >(),
                                                     alu_simd_node /** derives **/ >;
using ucomisd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     320 >(),
                                                     alu_simd_node /** derives **/ >;
using comiss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     321 >(),
                                                     alu_simd_node /** derives **/ >;
using comisd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     322 >(),
                                                     alu_simd_node /** derives **/ >;
using subps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     356 >(),
                                                     alu_simd_node /** derives **/ >;
using subss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     357 >(),
                                                     alu_simd_node /** derives **/ >;
using subpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     358 >(),
                                                     alu_simd_node /** derives **/ >;
using subsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     359 >(),
                                                     alu_simd_node /** derives **/ >;
using minps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     360 >(),
                                                     alu_simd_node /** derives **/ >;
using minss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     361 >(),
                                                     alu_simd_node /** derives **/ >;
using minpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     362 >(),
                                                     alu_simd_node /** derives **/ >;
using minsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     363 >(),
                                                     alu_simd_node /** derives **/ >;
using divps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     364 >(),
                                                     alu_simd_node /** derives **/ >;
using divss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     365 >(),
                                                     alu_simd_node /** derives **/ >;
using divpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     366 >(),
                                                     alu_simd_node /** derives **/ >;
using divsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     367 >(),
                                                     alu_simd_node /** derives **/ >;
using maxps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     368 >(),
                                                     alu_simd_node /** derives **/ >;
using maxss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     369 >(),
                                                     alu_simd_node /** derives **/ >;
using maxpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     370 >(),
                                                     alu_simd_node /** derives **/ >;
using maxsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     371 >(),
                                                     alu_simd_node /** derives **/ >;
using cmpps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     372 >(),
                                                     alu_simd_node /** derives **/ >;
using cmpss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     373 >(),
                                                     alu_simd_node /** derives **/ >;
using cmppd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     374 >(),
                                                     alu_simd_node /** derives **/ >;
using cmpsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     375 >(),
                                                     alu_simd_node /** derives **/ >;
using haddpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     485 >(),
                                                     alu_simd_node /** derives **/ >;
using haddps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     486 >(),
                                                     alu_simd_node /** derives **/ >;
using hsubpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     487 >(),
                                                     alu_simd_node /** derives **/ >;
using hsubps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     488 >(),
                                                     alu_simd_node /** derives **/ >;
using addsubpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     489 >(),
                                                     alu_simd_node /** derives **/ >;
using addsubps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     490 >(),
                                                     alu_simd_node /** derives **/ >;
using femms_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     497 >(),
                                                     alu_simd_node /** derives **/ >;
using unknown_3dnow_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     498 >(),
                                                     alu_simd_node /** derives **/ >;
using pfadd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     500 >(),
                                                     alu_simd_node /** derives **/ >;
using pfacc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     501 >(),
                                                     alu_simd_node /** derives **/ >;
using pfcmpge_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     502 >(),
                                                     alu_simd_node /** derives **/ >;
using pfcmpgt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     503 >(),
                                                     alu_simd_node /** derives **/ >;
using pfcmpeq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     504 >(),
                                                     alu_simd_node /** derives **/ >;
using pfmin_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     505 >(),
                                                     alu_simd_node /** derives **/ >;
using pfmax_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     506 >(),
                                                     alu_simd_node /** derives **/ >;
using pfmul_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     507 >(),
                                                     alu_simd_node /** derives **/ >;
using pfrcp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     508 >(),
                                                     alu_simd_node /** derives **/ >;
using pfrcpit1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     509 >(),
                                                     alu_simd_node /** derives **/ >;
using pfrcpit2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     510 >(),
                                                     alu_simd_node /** derives **/ >;
using pfrsqrt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     511 >(),
                                                     alu_simd_node /** derives **/ >;
using pfrsqit1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     512 >(),
                                                     alu_simd_node /** derives **/ >;
using pmulhrw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     513 >(),
                                                     alu_simd_node /** derives **/ >;
using pfsub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     514 >(),
                                                     alu_simd_node /** derives **/ >;
using pfsubr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     515 >(),
                                                     alu_simd_node /** derives **/ >;
using pi2fd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     516 >(),
                                                     alu_simd_node /** derives **/ >;
using pf2id_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     517 >(),
                                                     alu_simd_node /** derives **/ >;
using pi2fw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     518 >(),
                                                     alu_simd_node /** derives **/ >;
using pf2iw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     519 >(),
                                                     alu_simd_node /** derives **/ >;
using pfnacc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     520 >(),
                                                     alu_simd_node /** derives **/ >;
using pfpnacc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     521 >(),
                                                     alu_simd_node /** derives **/ >;
using phaddw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     524 >(),
                                                     alu_simd_node /** derives **/ >;
using phaddd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     525 >(),
                                                     alu_simd_node /** derives **/ >;
using phaddsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     526 >(),
                                                     alu_simd_node /** derives **/ >;
using pmaddubsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     527 >(),
                                                     alu_simd_node /** derives **/ >;
using phsubw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     528 >(),
                                                     alu_simd_node /** derives **/ >;
using phsubd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     529 >(),
                                                     alu_simd_node /** derives **/ >;
using phsubsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     530 >(),
                                                     alu_simd_node /** derives **/ >;
using psignb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     531 >(),
                                                     alu_simd_node /** derives **/ >;
using psignw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     532 >(),
                                                     alu_simd_node /** derives **/ >;
using psignd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     533 >(),
                                                     alu_simd_node /** derives **/ >;
using pmulhrsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     534 >(),
                                                     alu_simd_node /** derives **/ >;
using pabsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     535 >(),
                                                     alu_simd_node /** derives **/ >;
using pabsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     536 >(),
                                                     alu_simd_node /** derives **/ >;
using pabsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     537 >(),
                                                     alu_simd_node /** derives **/ >;
using pmuldq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     555 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpgtq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     565 >(),
                                                     alu_simd_node /** derives **/ >;
using pminsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     566 >(),
                                                     alu_simd_node /** derives **/ >;
using pminsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     567 >(),
                                                     alu_simd_node /** derives **/ >;
using pminuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     568 >(),
                                                     alu_simd_node /** derives **/ >;
using pminud_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     569 >(),
                                                     alu_simd_node /** derives **/ >;
using pmaxsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     570 >(),
                                                     alu_simd_node /** derives **/ >;
using pmaxsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     571 >(),
                                                     alu_simd_node /** derives **/ >;
using pmaxuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     572 >(),
                                                     alu_simd_node /** derives **/ >;
using pmaxud_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     573 >(),
                                                     alu_simd_node /** derives **/ >;
using pmulld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     574 >(),
                                                     alu_simd_node /** derives **/ >;
using phminposuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     575 >(),
                                                     alu_simd_node /** derives **/ >;
using roundps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     580 >(),
                                                     alu_simd_node /** derives **/ >;
using roundpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     581 >(),
                                                     alu_simd_node /** derives **/ >;
using dpps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     590 >(),
                                                     alu_simd_node /** derives **/ >;
using dppd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     591 >(),
                                                     alu_simd_node /** derives **/ >;
using mpsadbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     592 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpestrm_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     593 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpestri_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     594 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpistrm_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     595 >(),
                                                     alu_simd_node /** derives **/ >;
using pcmpistri_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     596 >(),
                                                     alu_simd_node /** derives **/ >;
using pclmulqdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     623 >(),
                                                     alu_simd_node /** derives **/ >;
using vucomiss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     661 >(),
                                                     alu_simd_node /** derives **/ >;
using vucomisd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     662 >(),
                                                     alu_simd_node /** derives **/ >;
using vcomiss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     663 >(),
                                                     alu_simd_node /** derives **/ >;
using vcomisd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     664 >(),
                                                     alu_simd_node /** derives **/ >;
using vsqrtps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     667 >(),
                                                     alu_simd_node /** derives **/ >;
using vsqrtss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     668 >(),
                                                     alu_simd_node /** derives **/ >;
using vsqrtpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     669 >(),
                                                     alu_simd_node /** derives **/ >;
using vsqrtsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     670 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrtps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     671 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrtss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     672 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcpps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     673 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcpss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     674 >(),
                                                     alu_simd_node /** derives **/ >;
using vandps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     675 >(),
                                                     alu_simd_node /** derives **/ >;
using vandpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     676 >(),
                                                     alu_simd_node /** derives **/ >;
using vandnps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     677 >(),
                                                     alu_simd_node /** derives **/ >;
using vandnpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     678 >(),
                                                     alu_simd_node /** derives **/ >;
using vorps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     679 >(),
                                                     alu_simd_node /** derives **/ >;
using vorpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     680 >(),
                                                     alu_simd_node /** derives **/ >;
using vxorps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     681 >(),
                                                     alu_simd_node /** derives **/ >;
using vxorpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     682 >(),
                                                     alu_simd_node /** derives **/ >;
using vaddps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     683 >(),
                                                     alu_simd_node /** derives **/ >;
using vaddss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     684 >(),
                                                     alu_simd_node /** derives **/ >;
using vaddpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     685 >(),
                                                     alu_simd_node /** derives **/ >;
using vaddsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     686 >(),
                                                     alu_simd_node /** derives **/ >;
using vmulps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     687 >(),
                                                     alu_simd_node /** derives **/ >;
using vmulss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     688 >(),
                                                     alu_simd_node /** derives **/ >;
using vmulpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     689 >(),
                                                     alu_simd_node /** derives **/ >;
using vmulsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     690 >(),
                                                     alu_simd_node /** derives **/ >;
using vsubps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     698 >(),
                                                     alu_simd_node /** derives **/ >;
using vsubss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     699 >(),
                                                     alu_simd_node /** derives **/ >;
using vsubpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     700 >(),
                                                     alu_simd_node /** derives **/ >;
using vsubsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     701 >(),
                                                     alu_simd_node /** derives **/ >;
using vminps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     702 >(),
                                                     alu_simd_node /** derives **/ >;
using vminss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     703 >(),
                                                     alu_simd_node /** derives **/ >;
using vminpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     704 >(),
                                                     alu_simd_node /** derives **/ >;
using vminsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     705 >(),
                                                     alu_simd_node /** derives **/ >;
using vdivps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     706 >(),
                                                     alu_simd_node /** derives **/ >;
using vdivss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     707 >(),
                                                     alu_simd_node /** derives **/ >;
using vdivpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     708 >(),
                                                     alu_simd_node /** derives **/ >;
using vdivsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     709 >(),
                                                     alu_simd_node /** derives **/ >;
using vmaxps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     710 >(),
                                                     alu_simd_node /** derives **/ >;
using vmaxss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     711 >(),
                                                     alu_simd_node /** derives **/ >;
using vmaxpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     712 >(),
                                                     alu_simd_node /** derives **/ >;
using vmaxsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     713 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpeqb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     732 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpeqw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     733 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpeqd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     734 >(),
                                                     alu_simd_node /** derives **/ >;
using vcmpps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     736 >(),
                                                     alu_simd_node /** derives **/ >;
using vcmpss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     737 >(),
                                                     alu_simd_node /** derives **/ >;
using vcmppd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     738 >(),
                                                     alu_simd_node /** derives **/ >;
using vcmpsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     739 >(),
                                                     alu_simd_node /** derives **/ >;
using vpextrw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     741 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsrlw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     744 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsrld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     745 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsrlq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     746 >(),
                                                     alu_simd_node /** derives **/ >;
using vpaddq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     747 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmullw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     748 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsubusb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     750 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsubusw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     751 >(),
                                                     alu_simd_node /** derives **/ >;
using vpminub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     752 >(),
                                                     alu_simd_node /** derives **/ >;
using vpand_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     753 >(),
                                                     alu_simd_node /** derives **/ >;
using vpaddusb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     754 >(),
                                                     alu_simd_node /** derives **/ >;
using vpaddusw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     755 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaxub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     756 >(),
                                                     alu_simd_node /** derives **/ >;
using vpandn_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     757 >(),
                                                     alu_simd_node /** derives **/ >;
using vpavgb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     758 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsraw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     759 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsrad_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     760 >(),
                                                     alu_simd_node /** derives **/ >;
using vpavgw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     761 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmulhuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     762 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmulhw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     763 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsubsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     768 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsubsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     769 >(),
                                                     alu_simd_node /** derives **/ >;
using vpminsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     770 >(),
                                                     alu_simd_node /** derives **/ >;
using vpor_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     771 >(),
                                                     alu_simd_node /** derives **/ >;
using vpaddsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     772 >(),
                                                     alu_simd_node /** derives **/ >;
using vpaddsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     773 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaxsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     774 >(),
                                                     alu_simd_node /** derives **/ >;
using vpxor_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     775 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsllw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     776 >(),
                                                     alu_simd_node /** derives **/ >;
using vpslld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     777 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsllq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     778 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmuludq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     779 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaddwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     780 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsadbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     781 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsubb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     783 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsubw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     784 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsubd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     785 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsubq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     786 >(),
                                                     alu_simd_node /** derives **/ >;
using vpaddb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     787 >(),
                                                     alu_simd_node /** derives **/ >;
using vpaddw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     788 >(),
                                                     alu_simd_node /** derives **/ >;
using vpaddd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     789 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsrldq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     790 >(),
                                                     alu_simd_node /** derives **/ >;
using vpslldq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     791 >(),
                                                     alu_simd_node /** derives **/ >;
using vhaddpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     794 >(),
                                                     alu_simd_node /** derives **/ >;
using vhaddps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     795 >(),
                                                     alu_simd_node /** derives **/ >;
using vhsubpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     796 >(),
                                                     alu_simd_node /** derives **/ >;
using vhsubps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     797 >(),
                                                     alu_simd_node /** derives **/ >;
using vaddsubpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     798 >(),
                                                     alu_simd_node /** derives **/ >;
using vaddsubps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     799 >(),
                                                     alu_simd_node /** derives **/ >;
using vlddqu_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     800 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     802 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     803 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     804 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaddubsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     805 >(),
                                                     alu_simd_node /** derives **/ >;
using vphsubw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     806 >(),
                                                     alu_simd_node /** derives **/ >;
using vphsubd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     807 >(),
                                                     alu_simd_node /** derives **/ >;
using vphsubsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     808 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsignb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     809 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsignw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     810 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsignd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     811 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmulhrsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     812 >(),
                                                     alu_simd_node /** derives **/ >;
using vpabsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     813 >(),
                                                     alu_simd_node /** derives **/ >;
using vpabsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     814 >(),
                                                     alu_simd_node /** derives **/ >;
using vpabsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     815 >(),
                                                     alu_simd_node /** derives **/ >;
using vpalignr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     816 >(),
                                                     alu_simd_node /** derives **/ >;
using vptest_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     820 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmuldq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     827 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpeqq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     828 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpgtq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     837 >(),
                                                     alu_simd_node /** derives **/ >;
using vpminsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     838 >(),
                                                     alu_simd_node /** derives **/ >;
using vpminsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     839 >(),
                                                     alu_simd_node /** derives **/ >;
using vpminuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     840 >(),
                                                     alu_simd_node /** derives **/ >;
using vpminud_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     841 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaxsb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     842 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaxsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     843 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaxuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     844 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaxud_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     845 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmulld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     846 >(),
                                                     alu_simd_node /** derives **/ >;
using vphminposuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     847 >(),
                                                     alu_simd_node /** derives **/ >;
using vaesimc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     848 >(),
                                                     alu_simd_node /** derives **/ >;
using vaesenc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     849 >(),
                                                     alu_simd_node /** derives **/ >;
using vaesenclast_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     850 >(),
                                                     alu_simd_node /** derives **/ >;
using vaesdec_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     851 >(),
                                                     alu_simd_node /** derives **/ >;
using vaesdeclast_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     852 >(),
                                                     alu_simd_node /** derives **/ >;
using vroundps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     856 >(),
                                                     alu_simd_node /** derives **/ >;
using vroundpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     857 >(),
                                                     alu_simd_node /** derives **/ >;
using vroundss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     858 >(),
                                                     alu_simd_node /** derives **/ >;
using vroundsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     859 >(),
                                                     alu_simd_node /** derives **/ >;
using vdpps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     866 >(),
                                                     alu_simd_node /** derives **/ >;
using vdppd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     867 >(),
                                                     alu_simd_node /** derives **/ >;
using vmpsadbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     868 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpestrm_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     869 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpestri_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     870 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpistrm_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     871 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpistri_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     872 >(),
                                                     alu_simd_node /** derives **/ >;
using vpclmulqdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     873 >(),
                                                     alu_simd_node /** derives **/ >;
using vtestps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     875 >(),
                                                     alu_simd_node /** derives **/ >;
using vtestpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     876 >(),
                                                     alu_simd_node /** derives **/ >;
using vzeroupper_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     877 >(),
                                                     alu_simd_node /** derives **/ >;
using vzeroall_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     878 >(),
                                                     alu_simd_node /** derives **/ >;
using vldmxcsr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     879 >(),
                                                     alu_simd_node /** derives **/ >;
using vstmxcsr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     880 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd132ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     893 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd132pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     894 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd213ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     895 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd213pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     896 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd231ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     897 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd231pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     898 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd132ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     899 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd132sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     900 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd213ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     901 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd213sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     902 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd231ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     903 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmadd231sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     904 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddsub132ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     905 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddsub132pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     906 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddsub213ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     907 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddsub213pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     908 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddsub231ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     909 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddsub231pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     910 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubadd132ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     911 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubadd132pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     912 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubadd213ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     913 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubadd213pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     914 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubadd231ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     915 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubadd231pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     916 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub132ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     917 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub132pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     918 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub213ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     919 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub213pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     920 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub231ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     921 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub231pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     922 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub132ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     923 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub132sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     924 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub213ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     925 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub213sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     926 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub231ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     927 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsub231sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     928 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd132ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     929 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd132pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     930 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd213ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     931 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd213pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     932 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd231ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     933 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd231pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     934 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd132ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     935 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd132sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     936 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd213ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     937 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd213sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     938 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd231ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     939 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmadd231sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     940 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub132ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     941 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub132pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     942 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub213ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     943 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub213pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     944 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub231ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     945 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub231pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     946 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub132ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     947 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub132sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     948 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub213ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     949 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub213sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     950 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub231ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     951 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsub231sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     952 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddsubps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     966 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddsubpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     967 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubaddps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     968 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubaddpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     969 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     970 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     971 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     972 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmaddsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     973 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     974 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     975 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     976 >(),
                                                     alu_simd_node /** derives **/ >;
using vfmsubsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     977 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmaddps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     978 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmaddpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     979 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmaddss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     980 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmaddsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     981 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsubps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     982 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsubpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     983 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsubss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     984 >(),
                                                     alu_simd_node /** derives **/ >;
using vfnmsubsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     985 >(),
                                                     alu_simd_node /** derives **/ >;
using vfrczps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     986 >(),
                                                     alu_simd_node /** derives **/ >;
using vfrczpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     987 >(),
                                                     alu_simd_node /** derives **/ >;
using vfrczss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     988 >(),
                                                     alu_simd_node /** derives **/ >;
using vfrczsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     989 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcomb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     991 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcomw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     992 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcomd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     993 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcomq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     994 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcomub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     995 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcomuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     996 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcomud_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     997 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcomuq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     998 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1001 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddbd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1002 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddbq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1003 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1004 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddwq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1005 >(),
                                                     alu_simd_node /** derives **/ >;
using vphadddq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1006 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddubw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1007 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddubd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1008 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddubq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1009 >(),
                                                     alu_simd_node /** derives **/ >;
using vphadduwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1010 >(),
                                                     alu_simd_node /** derives **/ >;
using vphadduwq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1011 >(),
                                                     alu_simd_node /** derives **/ >;
using vphaddudq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1012 >(),
                                                     alu_simd_node /** derives **/ >;
using vphsubbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1013 >(),
                                                     alu_simd_node /** derives **/ >;
using vphsubwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1014 >(),
                                                     alu_simd_node /** derives **/ >;
using vphsubdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1015 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacssww_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1016 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacsswd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1017 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacssdql_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1018 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacssdd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1019 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacssdqh_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1020 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacsww_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1021 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacswd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1022 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacsdql_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1023 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacsdd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1024 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmacsdqh_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1025 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmadcsswd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1026 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmadcswd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1027 >(),
                                                     alu_simd_node /** derives **/ >;
using vpperm_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1028 >(),
                                                     alu_simd_node /** derives **/ >;
using vprotb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1029 >(),
                                                     alu_simd_node /** derives **/ >;
using vprotw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1030 >(),
                                                     alu_simd_node /** derives **/ >;
using vprotd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1031 >(),
                                                     alu_simd_node /** derives **/ >;
using vprotq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1032 >(),
                                                     alu_simd_node /** derives **/ >;
using vpshlb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1033 >(),
                                                     alu_simd_node /** derives **/ >;
using vpshlw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1034 >(),
                                                     alu_simd_node /** derives **/ >;
using vpshld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1035 >(),
                                                     alu_simd_node /** derives **/ >;
using vpshlq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1036 >(),
                                                     alu_simd_node /** derives **/ >;
using vpshab_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1037 >(),
                                                     alu_simd_node /** derives **/ >;
using vpshaw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1038 >(),
                                                     alu_simd_node /** derives **/ >;
using vpshad_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1039 >(),
                                                     alu_simd_node /** derives **/ >;
using vpshaq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1040 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsllvd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1094 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsllvq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1095 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsravd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1096 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsrlvd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1097 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsrlvq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1098 >(),
                                                     alu_simd_node /** derives **/ >;
using kandw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1111 >(),
                                                     alu_simd_node /** derives **/ >;
using kandb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1112 >(),
                                                     alu_simd_node /** derives **/ >;
using kandq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1113 >(),
                                                     alu_simd_node /** derives **/ >;
using kandd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1114 >(),
                                                     alu_simd_node /** derives **/ >;
using kandnw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1115 >(),
                                                     alu_simd_node /** derives **/ >;
using kandnb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1116 >(),
                                                     alu_simd_node /** derives **/ >;
using kandnq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1117 >(),
                                                     alu_simd_node /** derives **/ >;
using kandnd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1118 >(),
                                                     alu_simd_node /** derives **/ >;
using knotw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1122 >(),
                                                     alu_simd_node /** derives **/ >;
using knotb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1123 >(),
                                                     alu_simd_node /** derives **/ >;
using knotq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1124 >(),
                                                     alu_simd_node /** derives **/ >;
using knotd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1125 >(),
                                                     alu_simd_node /** derives **/ >;
using korw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1126 >(),
                                                     alu_simd_node /** derives **/ >;
using korb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1127 >(),
                                                     alu_simd_node /** derives **/ >;
using korq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1128 >(),
                                                     alu_simd_node /** derives **/ >;
using kord_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1129 >(),
                                                     alu_simd_node /** derives **/ >;
using kxnorw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1130 >(),
                                                     alu_simd_node /** derives **/ >;
using kxnorb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1131 >(),
                                                     alu_simd_node /** derives **/ >;
using kxnorq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1132 >(),
                                                     alu_simd_node /** derives **/ >;
using kxnord_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1133 >(),
                                                     alu_simd_node /** derives **/ >;
using kxorw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1134 >(),
                                                     alu_simd_node /** derives **/ >;
using kxorb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1135 >(),
                                                     alu_simd_node /** derives **/ >;
using kxorq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1136 >(),
                                                     alu_simd_node /** derives **/ >;
using kxord_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1137 >(),
                                                     alu_simd_node /** derives **/ >;
using kaddw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1138 >(),
                                                     alu_simd_node /** derives **/ >;
using kaddb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1139 >(),
                                                     alu_simd_node /** derives **/ >;
using kaddq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1140 >(),
                                                     alu_simd_node /** derives **/ >;
using kaddd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1141 >(),
                                                     alu_simd_node /** derives **/ >;
using kortestw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1142 >(),
                                                     alu_simd_node /** derives **/ >;
using kortestb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1143 >(),
                                                     alu_simd_node /** derives **/ >;
using kortestq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1144 >(),
                                                     alu_simd_node /** derives **/ >;
using kortestd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1145 >(),
                                                     alu_simd_node /** derives **/ >;
using kshiftlw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1146 >(),
                                                     alu_simd_node /** derives **/ >;
using kshiftlb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1147 >(),
                                                     alu_simd_node /** derives **/ >;
using kshiftlq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1148 >(),
                                                     alu_simd_node /** derives **/ >;
using kshiftld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1149 >(),
                                                     alu_simd_node /** derives **/ >;
using kshiftrw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1150 >(),
                                                     alu_simd_node /** derives **/ >;
using kshiftrb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1151 >(),
                                                     alu_simd_node /** derives **/ >;
using kshiftrq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1152 >(),
                                                     alu_simd_node /** derives **/ >;
using kshiftrd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1153 >(),
                                                     alu_simd_node /** derives **/ >;
using ktestw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1154 >(),
                                                     alu_simd_node /** derives **/ >;
using ktestb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1155 >(),
                                                     alu_simd_node /** derives **/ >;
using ktestq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1156 >(),
                                                     alu_simd_node /** derives **/ >;
using ktestd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1157 >(),
                                                     alu_simd_node /** derives **/ >;
using vgetmantpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1231 >(),
                                                     alu_simd_node /** derives **/ >;
using vgetmantps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1232 >(),
                                                     alu_simd_node /** derives **/ >;
using vgetmantsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1233 >(),
                                                     alu_simd_node /** derives **/ >;
using vgetmantss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1234 >(),
                                                     alu_simd_node /** derives **/ >;
using vpabsq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1249 >(),
                                                     alu_simd_node /** derives **/ >;
using vpandd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1250 >(),
                                                     alu_simd_node /** derives **/ >;
using vpandnd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1251 >(),
                                                     alu_simd_node /** derives **/ >;
using vpandnq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1252 >(),
                                                     alu_simd_node /** derives **/ >;
using vpandq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1253 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1260 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1261 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1262 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpub_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1263 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpud_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1264 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpuq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1265 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpuw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1266 >(),
                                                     alu_simd_node /** derives **/ >;
using vpcmpw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1267 >(),
                                                     alu_simd_node /** derives **/ >;
using vplzcntd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1290 >(),
                                                     alu_simd_node /** derives **/ >;
using vplzcntq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1291 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmadd52huq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1292 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmadd52luq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1293 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaxsq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1294 >(),
                                                     alu_simd_node /** derives **/ >;
using vpmaxuq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1295 >(),
                                                     alu_simd_node /** derives **/ >;
using vpminsq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1296 >(),
                                                     alu_simd_node /** derives **/ >;
using vpminuq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1297 >(),
                                                     alu_simd_node /** derives **/ >;
using vpord_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1325 >(),
                                                     alu_simd_node /** derives **/ >;
using vporq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1326 >(),
                                                     alu_simd_node /** derives **/ >;
using vprold_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1327 >(),
                                                     alu_simd_node /** derives **/ >;
using vprolq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1328 >(),
                                                     alu_simd_node /** derives **/ >;
using vprolvd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1329 >(),
                                                     alu_simd_node /** derives **/ >;
using vprolvq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1330 >(),
                                                     alu_simd_node /** derives **/ >;
using vprord_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1331 >(),
                                                     alu_simd_node /** derives **/ >;
using vprorq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1332 >(),
                                                     alu_simd_node /** derives **/ >;
using vprorvd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1333 >(),
                                                     alu_simd_node /** derives **/ >;
using vprorvq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1334 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsllvw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1339 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsraq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1340 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsravq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1341 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsravw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1342 >(),
                                                     alu_simd_node /** derives **/ >;
using vpsrlvw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1343 >(),
                                                     alu_simd_node /** derives **/ >;
using vpternlogd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1344 >(),
                                                     alu_simd_node /** derives **/ >;
using vpternlogq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1345 >(),
                                                     alu_simd_node /** derives **/ >;
using vptestmb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1346 >(),
                                                     alu_simd_node /** derives **/ >;
using vptestmd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1347 >(),
                                                     alu_simd_node /** derives **/ >;
using vptestmq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1348 >(),
                                                     alu_simd_node /** derives **/ >;
using vptestmw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1349 >(),
                                                     alu_simd_node /** derives **/ >;
using vptestnmb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1350 >(),
                                                     alu_simd_node /** derives **/ >;
using vptestnmd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1351 >(),
                                                     alu_simd_node /** derives **/ >;
using vptestnmq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1352 >(),
                                                     alu_simd_node /** derives **/ >;
using vptestnmw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1353 >(),
                                                     alu_simd_node /** derives **/ >;
using vpxord_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1354 >(),
                                                     alu_simd_node /** derives **/ >;
using vpxorq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1355 >(),
                                                     alu_simd_node /** derives **/ >;
using vrangepd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1356 >(),
                                                     alu_simd_node /** derives **/ >;
using vrangeps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1357 >(),
                                                     alu_simd_node /** derives **/ >;
using vrangesd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1358 >(),
                                                     alu_simd_node /** derives **/ >;
using vrangess_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1359 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcp14pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1360 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcp14ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1361 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcp14sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1362 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcp14ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1363 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcp28pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1364 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcp28ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1365 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcp28sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1366 >(),
                                                     alu_simd_node /** derives **/ >;
using vrcp28ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1367 >(),
                                                     alu_simd_node /** derives **/ >;
using vreducepd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1368 >(),
                                                     alu_simd_node /** derives **/ >;
using vreduceps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1369 >(),
                                                     alu_simd_node /** derives **/ >;
using vreducesd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1370 >(),
                                                     alu_simd_node /** derives **/ >;
using vreducess_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1371 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrt14pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1376 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrt14ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1377 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrt14sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1378 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrt14ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1379 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrt28pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1380 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrt28ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1381 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrt28sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1382 >(),
                                                     alu_simd_node /** derives **/ >;
using vrsqrt28ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1383 >(),
                                                     alu_simd_node /** derives **/ >;
using mwaitx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1420 >(),
                                                     alu_simd_node /** derives **/ >;
using lfence_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     283 >(),
                                                     barrier_node /** derives **/ >;
using mfence_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     284 >(),
                                                     barrier_node /** derives **/ >;
using sfence_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     286 >(),
                                                     barrier_node /** derives **/ >;
using call_ins_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     42 >(),
                                                     call_node /** derives **/ >;
using call_ind_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     43 >(),
                                                     call_node /** derives **/ >;
using call_far_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     44 >(),
                                                     call_node /** derives **/ >;
using call_far_ind_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     45 >(),
                                                     call_node /** derives **/ >;
using ret_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     70 >(),
                                                     call_node /** derives **/ >;
using ret_far_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     71 >(),
                                                     call_node /** derives **/ >;
using leave_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     75 >(),
                                                     call_node /** derives **/ >;
using iret_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     79 >(),
                                                     call_node /** derives **/ >;
using vmcall_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     599 >(),
                                                     call_node /** derives **/ >;
using vmlaunch_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     600 >(),
                                                     call_node /** derives **/ >;
using vmresume_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     601 >(),
                                                     call_node /** derives **/ >;
using vmxoff_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     602 >(),
                                                     call_node /** derives **/ >;
using vmxon_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     605 >(),
                                                     call_node /** derives **/ >;
using vmrun_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     612 >(),
                                                     call_node /** derives **/ >;
using vmmcall_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     613 >(),
                                                     call_node /** derives **/ >;
using cmovo_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     110 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovo_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     110 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovno_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     111 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovno_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     111 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     112 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     112 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovnb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     113 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovnb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     113 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovz_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     114 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovz_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     114 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovnz_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     115 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovnz_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     115 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovbe_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     116 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovbe_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     116 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovnbe_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     117 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovnbe_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     117 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     118 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovs_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     118 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovns_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     119 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovns_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     119 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     120 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     120 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovnp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     121 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovnp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     121 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     122 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovl_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     122 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovnl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     123 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovnl_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     123 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovle_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     124 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovle_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     124 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmovnle_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     125 >(),
                                                     cond_mov_node /** derives **/ >;
using cmovnle_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     125 >(),
                                                     mem_scalar_node /** derives **/ >;
using setp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     178 >(),
                                                     cond_mov_node /** derives **/ >;
using setp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     178 >(),
                                                     mem_scalar_node /** derives **/ >;
using setnp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     179 >(),
                                                     cond_mov_node /** derives **/ >;
using setnp_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     179 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcmovb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     459 >(),
                                                     cond_mov_node /** derives **/ >;
using fcmovb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     459 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcmove_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     460 >(),
                                                     cond_mov_node /** derives **/ >;
using fcmove_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     460 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcmovbe_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     461 >(),
                                                     cond_mov_node /** derives **/ >;
using fcmovbe_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     461 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcmovu_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     462 >(),
                                                     cond_mov_node /** derives **/ >;
using fcmovu_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     462 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcmovnb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     464 >(),
                                                     cond_mov_node /** derives **/ >;
using fcmovnb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     464 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcmovne_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     465 >(),
                                                     cond_mov_node /** derives **/ >;
using fcmovne_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     465 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcmovnbe_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     466 >(),
                                                     cond_mov_node /** derives **/ >;
using fcmovnbe_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     466 >(),
                                                     mem_scalar_node /** derives **/ >;
using fcmovnu_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     467 >(),
                                                     cond_mov_node /** derives **/ >;
using fcmovnu_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     467 >(),
                                                     mem_scalar_node /** derives **/ >;
using seto_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     168 >(),
                                                     conditional_scalar_node /** derives **/ >;
using seto_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     168 >(),
                                                     mem_scalar_node /** derives **/ >;
using setno_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     169 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setno_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     169 >(),
                                                     mem_scalar_node /** derives **/ >;
using setb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     170 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     170 >(),
                                                     mem_scalar_node /** derives **/ >;
using setnb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     171 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setnb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     171 >(),
                                                     mem_scalar_node /** derives **/ >;
using setz_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     172 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setz_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     172 >(),
                                                     mem_scalar_node /** derives **/ >;
using setnz_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     173 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setnz_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     173 >(),
                                                     mem_scalar_node /** derives **/ >;
using setbe_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     174 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setbe_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     174 >(),
                                                     mem_scalar_node /** derives **/ >;
using setnbe_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     175 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setnbe_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     175 >(),
                                                     mem_scalar_node /** derives **/ >;
using sets_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     176 >(),
                                                     conditional_scalar_node /** derives **/ >;
using sets_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     176 >(),
                                                     mem_scalar_node /** derives **/ >;
using setns_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     177 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setns_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     177 >(),
                                                     mem_scalar_node /** derives **/ >;
using setl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     180 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setl_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     180 >(),
                                                     mem_scalar_node /** derives **/ >;
using setnl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     181 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setnl_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     181 >(),
                                                     mem_scalar_node /** derives **/ >;
using setle_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     182 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setle_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     182 >(),
                                                     mem_scalar_node /** derives **/ >;
using setnle_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     183 >(),
                                                     conditional_scalar_node /** derives **/ >;
using setnle_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     183 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmpxchg_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     190 >(),
                                                     conditional_scalar_node /** derives **/ >;
using cmpxchg_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     190 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmpxchg8b_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     278 >(),
                                                     conditional_scalar_node /** derives **/ >;
using cmpxchg8b_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     278 >(),
                                                     mem_scalar_node /** derives **/ >;
using xsetbv_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     632 >(),
                                                     conditional_scalar_node /** derives **/ >;
using xsetbv_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     632 >(),
                                                     mem_scalar_node /** derives **/ >;
using vpcmov_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     990 >(),
                                                     conditional_simd_node /** derives **/ >;
using vpermil2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     999 >(),
                                                     conditional_simd_node /** derives **/ >;
using vpermil2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1000 >(),
                                                     conditional_simd_node /** derives **/ >;
using cwde_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     63 >(),
                                                     convert_scalar_node /** derives **/ >;
using cwde_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     63 >(),
                                                     mem_scalar_node /** derives **/ >;
using cdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     64 >(),
                                                     convert_scalar_node /** derives **/ >;
using cdq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     64 >(),
                                                     mem_scalar_node /** derives **/ >;
using pextrw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     204 >(),
                                                     convert_scalar_node /** derives **/ >;
using pextrw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     204 >(),
                                                     mem_scalar_node /** derives **/ >;
using bswap_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     205 >(),
                                                     convert_scalar_node /** derives **/ >;
using bswap_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     205 >(),
                                                     mem_scalar_node /** derives **/ >;
using fchs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     432 >(),
                                                     convert_scalar_node /** derives **/ >;
using fchs_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     432 >(),
                                                     mem_scalar_node /** derives **/ >;
using fxtract_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     447 >(),
                                                     convert_scalar_node /** derives **/ >;
using fxtract_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     447 >(),
                                                     mem_scalar_node /** derives **/ >;
using cvtpi2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     307 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtsi2ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     308 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtpi2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     309 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtsi2sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     310 >(),
                                                     convert_simd_node /** derives **/ >;
using cvttps2pi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     311 >(),
                                                     convert_simd_node /** derives **/ >;
using cvttss2si_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     312 >(),
                                                     convert_simd_node /** derives **/ >;
using cvttpd2pi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     313 >(),
                                                     convert_simd_node /** derives **/ >;
using cvttsd2si_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     314 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtps2pi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     315 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtss2si_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     316 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtpd2pi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     317 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtsd2si_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     318 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtps2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     349 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtss2sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     350 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtpd2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     351 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtsd2ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     352 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtdq2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     353 >(),
                                                     convert_simd_node /** derives **/ >;
using cvttps2dq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     354 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtps2dq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     355 >(),
                                                     convert_simd_node /** derives **/ >;
using shufps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     376 >(),
                                                     convert_simd_node /** derives **/ >;
using shufpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     377 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtdq2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     378 >(),
                                                     convert_simd_node /** derives **/ >;
using cvttpd2dq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     379 >(),
                                                     convert_simd_node /** derives **/ >;
using cvtpd2dq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     380 >(),
                                                     convert_simd_node /** derives **/ >;
using pswapd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     522 >(),
                                                     convert_simd_node /** derives **/ >;
using pshufb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     523 >(),
                                                     convert_simd_node /** derives **/ >;
using palignr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     538 >(),
                                                     convert_simd_node /** derives **/ >;
using pblendvb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     545 >(),
                                                     convert_simd_node /** derives **/ >;
using blendvps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     546 >(),
                                                     convert_simd_node /** derives **/ >;
using blendvpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     547 >(),
                                                     convert_simd_node /** derives **/ >;
using packusdw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     558 >(),
                                                     convert_simd_node /** derives **/ >;
using extractps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     579 >(),
                                                     convert_simd_node /** derives **/ >;
using blendps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     584 >(),
                                                     convert_simd_node /** derives **/ >;
using blendpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     585 >(),
                                                     convert_simd_node /** derives **/ >;
using pblendw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     586 >(),
                                                     convert_simd_node /** derives **/ >;
using insertps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     588 >(),
                                                     convert_simd_node /** derives **/ >;
using vunpcklps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     644 >(),
                                                     convert_simd_node /** derives **/ >;
using vunpcklpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     645 >(),
                                                     convert_simd_node /** derives **/ >;
using vunpckhps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     646 >(),
                                                     convert_simd_node /** derives **/ >;
using vunpckhpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     647 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtsi2ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     653 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtsi2sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     654 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttss2si_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     657 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttsd2si_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     658 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtss2si_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     659 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtsd2si_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     660 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtps2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     691 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtss2sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     692 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtpd2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     693 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtsd2ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     694 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtdq2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     695 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttps2dq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     696 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtps2dq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     697 >(),
                                                     convert_simd_node /** derives **/ >;
using vpunpcklbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     714 >(),
                                                     convert_simd_node /** derives **/ >;
using vpunpcklwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     715 >(),
                                                     convert_simd_node /** derives **/ >;
using vpunpckldq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     716 >(),
                                                     convert_simd_node /** derives **/ >;
using vpacksswb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     717 >(),
                                                     convert_simd_node /** derives **/ >;
using vpcmpgtb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     718 >(),
                                                     convert_simd_node /** derives **/ >;
using vpcmpgtw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     719 >(),
                                                     convert_simd_node /** derives **/ >;
using vpcmpgtd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     720 >(),
                                                     convert_simd_node /** derives **/ >;
using vpackuswb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     721 >(),
                                                     convert_simd_node /** derives **/ >;
using vpunpckhbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     722 >(),
                                                     convert_simd_node /** derives **/ >;
using vpunpckhwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     723 >(),
                                                     convert_simd_node /** derives **/ >;
using vpunpckhdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     724 >(),
                                                     convert_simd_node /** derives **/ >;
using vpackssdw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     725 >(),
                                                     convert_simd_node /** derives **/ >;
using vpunpcklqdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     726 >(),
                                                     convert_simd_node /** derives **/ >;
using vpunpckhqdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     727 >(),
                                                     convert_simd_node /** derives **/ >;
using vpshufhw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     729 >(),
                                                     convert_simd_node /** derives **/ >;
using vpshufd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     730 >(),
                                                     convert_simd_node /** derives **/ >;
using vpshuflw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     731 >(),
                                                     convert_simd_node /** derives **/ >;
using vshufps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     742 >(),
                                                     convert_simd_node /** derives **/ >;
using vshufpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     743 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtdq2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     764 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttpd2dq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     765 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtpd2dq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     766 >(),
                                                     convert_simd_node /** derives **/ >;
using vpshufb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     801 >(),
                                                     convert_simd_node /** derives **/ >;
using vpblendvb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     817 >(),
                                                     convert_simd_node /** derives **/ >;
using vblendvps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     818 >(),
                                                     convert_simd_node /** derives **/ >;
using vblendvpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     819 >(),
                                                     convert_simd_node /** derives **/ >;
using vpackusdw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     830 >(),
                                                     convert_simd_node /** derives **/ >;
using vpextrb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     853 >(),
                                                     convert_simd_node /** derives **/ >;
using vpextrd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     854 >(),
                                                     convert_simd_node /** derives **/ >;
using vextractps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     855 >(),
                                                     convert_simd_node /** derives **/ >;
using vblendps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     860 >(),
                                                     convert_simd_node /** derives **/ >;
using vblendpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     861 >(),
                                                     convert_simd_node /** derives **/ >;
using vpblendw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     862 >(),
                                                     convert_simd_node /** derives **/ >;
using vinsertps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     864 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcastss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     881 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcastsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     882 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcastf128_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     883 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermilps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     886 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermilpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     887 >(),
                                                     convert_simd_node /** derives **/ >;
using vperm2f128_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     888 >(),
                                                     convert_simd_node /** derives **/ >;
using vinsertf128_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     889 >(),
                                                     convert_simd_node /** derives **/ >;
using vextractf128_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     890 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtph2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     891 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtps2ph_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     892 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcasti128_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1083 >(),
                                                     convert_simd_node /** derives **/ >;
using vinserti128_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1084 >(),
                                                     convert_simd_node /** derives **/ >;
using vextracti128_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1085 >(),
                                                     convert_simd_node /** derives **/ >;
using vperm2i128_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1088 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1089 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1090 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1091 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1092 >(),
                                                     convert_simd_node /** derives **/ >;
using vpblendd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1093 >(),
                                                     convert_simd_node /** derives **/ >;
using vpbroadcastb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1099 >(),
                                                     convert_simd_node /** derives **/ >;
using vpbroadcastw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1100 >(),
                                                     convert_simd_node /** derives **/ >;
using vpbroadcastd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1101 >(),
                                                     convert_simd_node /** derives **/ >;
using vpbroadcastq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1102 >(),
                                                     convert_simd_node /** derives **/ >;
using kunpckbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1119 >(),
                                                     convert_simd_node /** derives **/ >;
using kunpckwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1120 >(),
                                                     convert_simd_node /** derives **/ >;
using kunpckdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1121 >(),
                                                     convert_simd_node /** derives **/ >;
using valignd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1158 >(),
                                                     convert_simd_node /** derives **/ >;
using valignq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1159 >(),
                                                     convert_simd_node /** derives **/ >;
using vblendmpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1160 >(),
                                                     convert_simd_node /** derives **/ >;
using vblendmps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1161 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcastf32x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1162 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcastf32x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1163 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcastf32x8_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1164 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcastf64x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1165 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcastf64x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1166 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcasti32x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1167 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcasti32x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1168 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcasti32x8_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1169 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcasti64x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1170 >(),
                                                     convert_simd_node /** derives **/ >;
using vbroadcasti64x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1171 >(),
                                                     convert_simd_node /** derives **/ >;
using vcompresspd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1172 >(),
                                                     convert_simd_node /** derives **/ >;
using vcompressps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1173 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtpd2qq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1174 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtpd2udq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1175 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtpd2uqq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1176 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtps2qq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1177 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtps2udq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1178 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtps2uqq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1179 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtqq2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1180 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtqq2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1181 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtsd2usi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1182 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtss2usi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1183 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttpd2qq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1184 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttpd2udq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1185 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttpd2uqq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1186 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttps2qq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1187 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttps2udq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1188 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttps2uqq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1189 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttsd2usi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1190 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvttss2usi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1191 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtudq2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1192 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtudq2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1193 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtuqq2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1194 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtuqq2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1195 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtusi2sd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1196 >(),
                                                     convert_simd_node /** derives **/ >;
using vcvtusi2ss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1197 >(),
                                                     convert_simd_node /** derives **/ >;
using vdbpsadbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1198 >(),
                                                     convert_simd_node /** derives **/ >;
using vexp2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1199 >(),
                                                     convert_simd_node /** derives **/ >;
using vexp2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1200 >(),
                                                     convert_simd_node /** derives **/ >;
using vexpandpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1201 >(),
                                                     convert_simd_node /** derives **/ >;
using vexpandps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1202 >(),
                                                     convert_simd_node /** derives **/ >;
using vextractf32x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1203 >(),
                                                     convert_simd_node /** derives **/ >;
using vextractf32x8_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1204 >(),
                                                     convert_simd_node /** derives **/ >;
using vextractf64x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1205 >(),
                                                     convert_simd_node /** derives **/ >;
using vextractf64x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1206 >(),
                                                     convert_simd_node /** derives **/ >;
using vextracti32x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1207 >(),
                                                     convert_simd_node /** derives **/ >;
using vextracti32x8_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1208 >(),
                                                     convert_simd_node /** derives **/ >;
using vextracti64x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1209 >(),
                                                     convert_simd_node /** derives **/ >;
using vextracti64x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1210 >(),
                                                     convert_simd_node /** derives **/ >;
using vfixupimmpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1211 >(),
                                                     convert_simd_node /** derives **/ >;
using vfixupimmps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1212 >(),
                                                     convert_simd_node /** derives **/ >;
using vfixupimmsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1213 >(),
                                                     convert_simd_node /** derives **/ >;
using vfixupimmss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1214 >(),
                                                     convert_simd_node /** derives **/ >;
using vfpclasspd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1215 >(),
                                                     convert_simd_node /** derives **/ >;
using vfpclassps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1216 >(),
                                                     convert_simd_node /** derives **/ >;
using vfpclasssd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1217 >(),
                                                     convert_simd_node /** derives **/ >;
using vfpclassss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1218 >(),
                                                     convert_simd_node /** derives **/ >;
using vgetexppd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1227 >(),
                                                     convert_simd_node /** derives **/ >;
using vgetexpps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1228 >(),
                                                     convert_simd_node /** derives **/ >;
using vgetexpsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1229 >(),
                                                     convert_simd_node /** derives **/ >;
using vgetexpss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1230 >(),
                                                     convert_simd_node /** derives **/ >;
using vinsertf32x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1235 >(),
                                                     convert_simd_node /** derives **/ >;
using vinsertf32x8_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1236 >(),
                                                     convert_simd_node /** derives **/ >;
using vinsertf64x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1237 >(),
                                                     convert_simd_node /** derives **/ >;
using vinsertf64x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1238 >(),
                                                     convert_simd_node /** derives **/ >;
using vinserti32x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1239 >(),
                                                     convert_simd_node /** derives **/ >;
using vinserti32x8_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1240 >(),
                                                     convert_simd_node /** derives **/ >;
using vinserti64x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1241 >(),
                                                     convert_simd_node /** derives **/ >;
using vinserti64x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1242 >(),
                                                     convert_simd_node /** derives **/ >;
using vpblendmb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1254 >(),
                                                     convert_simd_node /** derives **/ >;
using vpblendmd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1255 >(),
                                                     convert_simd_node /** derives **/ >;
using vpblendmq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1256 >(),
                                                     convert_simd_node /** derives **/ >;
using vpblendmw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1257 >(),
                                                     convert_simd_node /** derives **/ >;
using vpbroadcastmb2q_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1258 >(),
                                                     convert_simd_node /** derives **/ >;
using vpbroadcastmw2d_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1259 >(),
                                                     convert_simd_node /** derives **/ >;
using vpcompressd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1268 >(),
                                                     convert_simd_node /** derives **/ >;
using vpcompressq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1269 >(),
                                                     convert_simd_node /** derives **/ >;
using vpconflictd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1270 >(),
                                                     convert_simd_node /** derives **/ >;
using vpconflictq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1271 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1272 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermi2b_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1273 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermi2d_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1274 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermi2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1275 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermi2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1276 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermi2q_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1277 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermi2w_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1278 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermt2b_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1279 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermt2d_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1280 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermt2pd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1281 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermt2ps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1282 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermt2q_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1283 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermt2w_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1284 >(),
                                                     convert_simd_node /** derives **/ >;
using vpermw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1285 >(),
                                                     convert_simd_node /** derives **/ >;
using vpexpandd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1286 >(),
                                                     convert_simd_node /** derives **/ >;
using vpexpandq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1287 >(),
                                                     convert_simd_node /** derives **/ >;
using vpextrq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1288 >(),
                                                     convert_simd_node /** derives **/ >;
using vrndscalepd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1372 >(),
                                                     convert_simd_node /** derives **/ >;
using vrndscaleps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1373 >(),
                                                     convert_simd_node /** derives **/ >;
using vrndscalesd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1374 >(),
                                                     convert_simd_node /** derives **/ >;
using vrndscaless_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1375 >(),
                                                     convert_simd_node /** derives **/ >;
using vscalefpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1384 >(),
                                                     convert_simd_node /** derives **/ >;
using vscalefps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1385 >(),
                                                     convert_simd_node /** derives **/ >;
using vscalefsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1386 >(),
                                                     convert_simd_node /** derives **/ >;
using vscalefss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1387 >(),
                                                     convert_simd_node /** derives **/ >;
using vshuff32x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1400 >(),
                                                     convert_simd_node /** derives **/ >;
using vshuff64x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1401 >(),
                                                     convert_simd_node /** derives **/ >;
using vshufi32x4_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1402 >(),
                                                     convert_simd_node /** derives **/ >;
using vshufi64x2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1403 >(),
                                                     convert_simd_node /** derives **/ >;
using sysenter_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     108 >(),
                                                     cpu_state_node /** derives **/ >;
using sysexit_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     109 >(),
                                                     cpu_state_node /** derives **/ >;
using rsm_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     187 >(),
                                                     cpu_state_node /** derives **/ >;
using xabort_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1071 >(),
                                                     cpu_state_node /** derives **/ >;
using xbegin_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1072 >(),
                                                     cpu_state_node /** derives **/ >;
using xend_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1073 >(),
                                                     cpu_state_node /** derives **/ >;
using xtest_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1074 >(),
                                                     cpu_state_node /** derives **/ >;
using hlt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     85 >(),
                                                     cpu_state_node /** derives **/ >;
using cmc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     86 >(),
                                                     cpu_state_node /** derives **/ >;
using clc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     87 >(),
                                                     cpu_state_node /** derives **/ >;
using cli_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     89 >(),
                                                     cpu_state_node /** derives **/ >;
using sti_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     90 >(),
                                                     cpu_state_node /** derives **/ >;
using cld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     91 >(),
                                                     cpu_state_node /** derives **/ >;
using std_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     92 >(),
                                                     cpu_state_node /** derives **/ >;
using lar_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     93 >(),
                                                     cpu_state_node /** derives **/ >;
using lsl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     94 >(),
                                                     cpu_state_node /** derives **/ >;
using clts_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     96 >(),
                                                     cpu_state_node /** derives **/ >;
using rdtsc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     105 >(),
                                                     cpu_state_node /** derives **/ >;
using cpuid_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     184 >(),
                                                     cpu_state_node /** derives **/ >;
using ud2b_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     196 >(),
                                                     cpu_state_node /** derives **/ >;
using pause_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     382 >(),
                                                     cpu_state_node /** derives **/ >;
using fnclex_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     468 >(),
                                                     cpu_state_node /** derives **/ >;
using fninit_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     469 >(),
                                                     cpu_state_node /** derives **/ >;
using ffree_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     472 >(),
                                                     cpu_state_node /** derives **/ >;
using stgi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     616 >(),
                                                     cpu_state_node /** derives **/ >;
using clgi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     617 >(),
                                                     cpu_state_node /** derives **/ >;
using skinit_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     618 >(),
                                                     cpu_state_node /** derives **/ >;
using rdtscp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     620 >(),
                                                     cpu_state_node /** derives **/ >;
using xgetbv_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     631 >(),
                                                     cpu_state_node /** derives **/ >;
using getsec_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1068 >(),
                                                     cpu_state_node /** derives **/ >;
using vmfunc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1069 >(),
                                                     cpu_state_node /** derives **/ >;
using invpcid_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1070 >(),
                                                     cpu_state_node /** derives **/ >;
using fwait_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     65 >(),
                                                     exception_node /** derives **/ >;
using vpgatherdd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1075 >(),
                                                     gather_node /** derives **/ >;
using vpgatherdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1076 >(),
                                                     gather_node /** derives **/ >;
using vpgatherqd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1077 >(),
                                                     gather_node /** derives **/ >;
using vpgatherqq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1078 >(),
                                                     gather_node /** derives **/ >;
using vgatherdps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1079 >(),
                                                     gather_node /** derives **/ >;
using vgatherdpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1080 >(),
                                                     gather_node /** derives **/ >;
using vgatherqps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1081 >(),
                                                     gather_node /** derives **/ >;
using vgatherqpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1082 >(),
                                                     gather_node /** derives **/ >;
using vgatherpf0dpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1219 >(),
                                                     gather_node /** derives **/ >;
using vgatherpf0dps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1220 >(),
                                                     gather_node /** derives **/ >;
using vgatherpf0qpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1221 >(),
                                                     gather_node /** derives **/ >;
using vgatherpf0qps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1222 >(),
                                                     gather_node /** derives **/ >;
using vgatherpf1dpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1223 >(),
                                                     gather_node /** derives **/ >;
using vgatherpf1dps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1224 >(),
                                                     gather_node /** derives **/ >;
using vgatherpf1qpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1225 >(),
                                                     gather_node /** derives **/ >;
using vgatherpf1qps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1226 >(),
                                                     gather_node /** derives **/ >;
using int3_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     76 >(),
                                                     interrupt_node /** derives **/ >;
using int_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     77 >(),
                                                     interrupt_node /** derives **/ >;
using into_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     78 >(),
                                                     interrupt_node /** derives **/ >;
using int1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     609 >(),
                                                     interrupt_node /** derives **/ >;
using in_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     83 >(),
                                                     io_node /** derives **/ >;
using out_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     84 >(),
                                                     io_node /** derives **/ >;
using ins_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     383 >(),
                                                     io_node /** derives **/ >;
using outs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     385 >(),
                                                     io_node /** derives **/ >;
using jo_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     26 >(),
                                                     jump_node /** derives **/ >;
using jno_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     27 >(),
                                                     jump_node /** derives **/ >;
using jb_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     28 >(),
                                                     jump_node /** derives **/ >;
using jnb_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     29 >(),
                                                     jump_node /** derives **/ >;
using jz_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     30 >(),
                                                     jump_node /** derives **/ >;
using jnz_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     31 >(),
                                                     jump_node /** derives **/ >;
using jbe_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     32 >(),
                                                     jump_node /** derives **/ >;
using jnbe_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     33 >(),
                                                     jump_node /** derives **/ >;
using js_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     34 >(),
                                                     jump_node /** derives **/ >;
using jns_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     35 >(),
                                                     jump_node /** derives **/ >;
using jp_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     36 >(),
                                                     jump_node /** derives **/ >;
using jnp_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     37 >(),
                                                     jump_node /** derives **/ >;
using jl_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     38 >(),
                                                     jump_node /** derives **/ >;
using jnl_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     39 >(),
                                                     jump_node /** derives **/ >;
using jle_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     40 >(),
                                                     jump_node /** derives **/ >;
using jnle_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     41 >(),
                                                     jump_node /** derives **/ >;
using jmp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     46 >(),
                                                     jump_node /** derives **/ >;
using jmp_short_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     47 >(),
                                                     jump_node /** derives **/ >;
using jmp_ind_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     48 >(),
                                                     jump_node /** derives **/ >;
using jmp_far_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     49 >(),
                                                     jump_node /** derives **/ >;
using jmp_far_ind_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     50 >(),
                                                     jump_node /** derives **/ >;
using jecxz_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     54 >(),
                                                     jump_node /** derives **/ >;
using jo_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     152 >(),
                                                     jump_node /** derives **/ >;
using jno_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     153 >(),
                                                     jump_node /** derives **/ >;
using jb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     154 >(),
                                                     jump_node /** derives **/ >;
using jnb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     155 >(),
                                                     jump_node /** derives **/ >;
using jz_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     156 >(),
                                                     jump_node /** derives **/ >;
using jnz_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     157 >(),
                                                     jump_node /** derives **/ >;
using jbe_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     158 >(),
                                                     jump_node /** derives **/ >;
using jnbe_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     159 >(),
                                                     jump_node /** derives **/ >;
using js_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     160 >(),
                                                     jump_node /** derives **/ >;
using jns_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     161 >(),
                                                     jump_node /** derives **/ >;
using jp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     162 >(),
                                                     jump_node /** derives **/ >;
using jnp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     163 >(),
                                                     jump_node /** derives **/ >;
using jl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     164 >(),
                                                     jump_node /** derives **/ >;
using jnl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     165 >(),
                                                     jump_node /** derives **/ >;
using jle_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     166 >(),
                                                     jump_node /** derives **/ >;
using jnle_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     167 >(),
                                                     jump_node /** derives **/ >;
using loopne_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     51 >(),
                                                     loop_node /** derives **/ >;
using loope_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     52 >(),
                                                     loop_node /** derives **/ >;
using loop_ins_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     53 >(),
                                                     loop_node /** derives **/ >;
using rep_ins_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     384 >(),
                                                     loop_node /** derives **/ >;
using rep_outs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     386 >(),
                                                     loop_node /** derives **/ >;
using rep_movs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     388 >(),
                                                     loop_node /** derives **/ >;
using rep_stos_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     390 >(),
                                                     loop_node /** derives **/ >;
using rep_lods_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     392 >(),
                                                     loop_node /** derives **/ >;
using rep_cmps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     394 >(),
                                                     loop_node /** derives **/ >;
using repne_cmps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     395 >(),
                                                     loop_node /** derives **/ >;
using rep_scas_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     397 >(),
                                                     loop_node /** derives **/ >;
using repne_scas_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     398 >(),
                                                     loop_node /** derives **/ >;
using invd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     98 >(),
                                                     maint_node /** derives **/ >;
using wbinvd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     99 >(),
                                                     maint_node /** derives **/ >;
using invlpg_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     277 >(),
                                                     maint_node /** derives **/ >;
using clflush_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     285 >(),
                                                     maint_node /** derives **/ >;
using monitor_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     492 >(),
                                                     maint_node /** derives **/ >;
using invlpga_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     619 >(),
                                                     maint_node /** derives **/ >;
using invept_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     621 >(),
                                                     maint_node /** derives **/ >;
using invvpid_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     622 >(),
                                                     maint_node /** derives **/ >;
using push_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     18 >(),
                                                     mem_scalar_node /** derives **/ >;
using push_imm_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     19 >(),
                                                     mem_scalar_node /** derives **/ >;
using pop_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     20 >(),
                                                     mem_scalar_node /** derives **/ >;
using pusha_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     21 >(),
                                                     mem_scalar_node /** derives **/ >;
using popa_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     22 >(),
                                                     mem_scalar_node /** derives **/ >;
using bound_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     23 >(),
                                                     mem_scalar_node /** derives **/ >;
using arpl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     24 >(),
                                                     mem_scalar_node /** derives **/ >;
using xchg_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     62 >(),
                                                     mem_scalar_node /** derives **/ >;
using pushf_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     66 >(),
                                                     mem_scalar_node /** derives **/ >;
using popf_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     67 >(),
                                                     mem_scalar_node /** derives **/ >;
using sahf_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     68 >(),
                                                     mem_scalar_node /** derives **/ >;
using lahf_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     69 >(),
                                                     mem_scalar_node /** derives **/ >;
using les_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     72 >(),
                                                     mem_scalar_node /** derives **/ >;
using lds_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     73 >(),
                                                     mem_scalar_node /** derives **/ >;
using enter_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     74 >(),
                                                     mem_scalar_node /** derives **/ >;
using xlat_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     82 >(),
                                                     mem_scalar_node /** derives **/ >;
using stc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     88 >(),
                                                     mem_scalar_node /** derives **/ >;
using wrmsr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     104 >(),
                                                     mem_scalar_node /** derives **/ >;
using rdmsr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     106 >(),
                                                     mem_scalar_node /** derives **/ >;
using rdpmc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     107 >(),
                                                     mem_scalar_node /** derives **/ >;
using lss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     191 >(),
                                                     mem_scalar_node /** derives **/ >;
using lfs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     193 >(),
                                                     mem_scalar_node /** derives **/ >;
using lgs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     194 >(),
                                                     mem_scalar_node /** derives **/ >;
using sldt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     265 >(),
                                                     mem_scalar_node /** derives **/ >;
using str_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     266 >(),
                                                     mem_scalar_node /** derives **/ >;
using lldt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     267 >(),
                                                     mem_scalar_node /** derives **/ >;
using ltr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     268 >(),
                                                     mem_scalar_node /** derives **/ >;
using verr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     269 >(),
                                                     mem_scalar_node /** derives **/ >;
using verw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     270 >(),
                                                     mem_scalar_node /** derives **/ >;
using sgdt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     271 >(),
                                                     mem_scalar_node /** derives **/ >;
using sidt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     272 >(),
                                                     mem_scalar_node /** derives **/ >;
using lgdt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     273 >(),
                                                     mem_scalar_node /** derives **/ >;
using lidt_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     274 >(),
                                                     mem_scalar_node /** derives **/ >;
using smsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     275 >(),
                                                     mem_scalar_node /** derives **/ >;
using lmsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     276 >(),
                                                     mem_scalar_node /** derives **/ >;
using fxsave32_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     279 >(),
                                                     mem_scalar_node /** derives **/ >;
using fxrstor32_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     280 >(),
                                                     mem_scalar_node /** derives **/ >;
using ldmxcsr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     281 >(),
                                                     mem_scalar_node /** derives **/ >;
using stmxcsr_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     282 >(),
                                                     mem_scalar_node /** derives **/ >;
using stos_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     389 >(),
                                                     mem_scalar_node /** derives **/ >;
using lods_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     391 >(),
                                                     mem_scalar_node /** derives **/ >;
using cmps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     393 >(),
                                                     mem_scalar_node /** derives **/ >;
using scas_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     396 >(),
                                                     mem_scalar_node /** derives **/ >;
using fld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     407 >(),
                                                     mem_scalar_node /** derives **/ >;
using fst_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     408 >(),
                                                     mem_scalar_node /** derives **/ >;
using fstp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     409 >(),
                                                     mem_scalar_node /** derives **/ >;
using fldenv_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     410 >(),
                                                     mem_scalar_node /** derives **/ >;
using fldcw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     411 >(),
                                                     mem_scalar_node /** derives **/ >;
using fnstenv_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     412 >(),
                                                     mem_scalar_node /** derives **/ >;
using fnstcw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     413 >(),
                                                     mem_scalar_node /** derives **/ >;
using fild_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     422 >(),
                                                     mem_scalar_node /** derives **/ >;
using fist_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     423 >(),
                                                     mem_scalar_node /** derives **/ >;
using fistp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     424 >(),
                                                     mem_scalar_node /** derives **/ >;
using frstor_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     425 >(),
                                                     mem_scalar_node /** derives **/ >;
using fnsave_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     426 >(),
                                                     mem_scalar_node /** derives **/ >;
using fnstsw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     427 >(),
                                                     mem_scalar_node /** derives **/ >;
using fbld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     428 >(),
                                                     mem_scalar_node /** derives **/ >;
using fbstp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     429 >(),
                                                     mem_scalar_node /** derives **/ >;
using fxch_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     430 >(),
                                                     mem_scalar_node /** derives **/ >;
using ftst_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     434 >(),
                                                     mem_scalar_node /** derives **/ >;
using fld1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     436 >(),
                                                     mem_scalar_node /** derives **/ >;
using fldl2t_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     437 >(),
                                                     mem_scalar_node /** derives **/ >;
using fldl2e_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     438 >(),
                                                     mem_scalar_node /** derives **/ >;
using fldpi_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     439 >(),
                                                     mem_scalar_node /** derives **/ >;
using fldlg2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     440 >(),
                                                     mem_scalar_node /** derives **/ >;
using fldln2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     441 >(),
                                                     mem_scalar_node /** derives **/ >;
using fldz_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     442 >(),
                                                     mem_scalar_node /** derives **/ >;
using fisttp_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     484 >(),
                                                     mem_scalar_node /** derives **/ >;
using lddqu_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     491 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmptrst_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     603 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmptrld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     604 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmclear_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     606 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmread_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     607 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmwrite_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     608 >(),
                                                     mem_scalar_node /** derives **/ >;
using ffreep_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     611 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmload_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     614 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmsave_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     615 >(),
                                                     mem_scalar_node /** derives **/ >;
using xsave32_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     633 >(),
                                                     mem_scalar_node /** derives **/ >;
using xrstor32_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     634 >(),
                                                     mem_scalar_node /** derives **/ >;
using xsaveopt32_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     635 >(),
                                                     mem_scalar_node /** derives **/ >;
using fxsave64_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     955 >(),
                                                     mem_scalar_node /** derives **/ >;
using fxrstor64_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     956 >(),
                                                     mem_scalar_node /** derives **/ >;
using xsave64_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     957 >(),
                                                     mem_scalar_node /** derives **/ >;
using xrstor64_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     958 >(),
                                                     mem_scalar_node /** derives **/ >;
using xsaveopt64_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     959 >(),
                                                     mem_scalar_node /** derives **/ >;
using rdrand_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     960 >(),
                                                     mem_scalar_node /** derives **/ >;
using rdfsbase_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     961 >(),
                                                     mem_scalar_node /** derives **/ >;
using rdgsbase_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     962 >(),
                                                     mem_scalar_node /** derives **/ >;
using wrfsbase_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     963 >(),
                                                     mem_scalar_node /** derives **/ >;
using wrgsbase_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     964 >(),
                                                     mem_scalar_node /** derives **/ >;
using rdseed_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     965 >(),
                                                     mem_scalar_node /** derives **/ >;
using llwpcb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1051 >(),
                                                     mem_scalar_node /** derives **/ >;
using slwpcb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1052 >(),
                                                     mem_scalar_node /** derives **/ >;
using lwpval_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1054 >(),
                                                     mem_scalar_node /** derives **/ >;
using xsavec32_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1103 >(),
                                                     mem_scalar_node /** derives **/ >;
using xsavec64_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1104 >(),
                                                     mem_scalar_node /** derives **/ >;
using bndcl_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1411 >(),
                                                     mem_scalar_node /** derives **/ >;
using bndcn_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1412 >(),
                                                     mem_scalar_node /** derives **/ >;
using bndcu_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1413 >(),
                                                     mem_scalar_node /** derives **/ >;
using bndldx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1414 >(),
                                                     mem_scalar_node /** derives **/ >;
using bndmk_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1415 >(),
                                                     mem_scalar_node /** derives **/ >;
using bndstx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1417 >(),
                                                     mem_scalar_node /** derives **/ >;
using ptwrite_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1418 >(),
                                                     mem_scalar_node /** derives **/ >;
using maskmovq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     242 >(),
                                                     mov_pack_unpack_node /** derives **/ >;
using maskmovq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     242 >(),
                                                     mem_scalar_node /** derives **/ >;
using maskmovdqu_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     243 >(),
                                                     mov_pack_unpack_node /** derives **/ >;
using maskmovdqu_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     243 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmaskmovdqu_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     782 >(),
                                                     mov_pack_unpack_node /** derives **/ >;
using vmaskmovdqu_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     782 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmaskmovps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     884 >(),
                                                     mov_pack_unpack_node /** derives **/ >;
using vmaskmovps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     884 >(),
                                                     mem_scalar_node /** derives **/ >;
using vmaskmovpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     885 >(),
                                                     mov_pack_unpack_node /** derives **/ >;
using vmaskmovpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     885 >(),
                                                     mem_scalar_node /** derives **/ >;
using vpmaskmovd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1086 >(),
                                                     mov_pack_unpack_node /** derives **/ >;
using vpmaskmovd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1086 >(),
                                                     mem_scalar_node /** derives **/ >;
using vpmaskmovq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1087 >(),
                                                     mov_pack_unpack_node /** derives **/ >;
using vpmaskmovq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1087 >(),
                                                     mem_scalar_node /** derives **/ >;
using mov_ld_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     55 >(),
                                                     mov_scalar_node /** derives **/ >;
using mov_ld_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     55 >(),
                                                     mem_scalar_node /** derives **/ >;
using mov_st_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     56 >(),
                                                     mov_scalar_node /** derives **/ >;
using mov_st_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     56 >(),
                                                     mem_scalar_node /** derives **/ >;
using mov_imm_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     57 >(),
                                                     mov_scalar_node /** derives **/ >;
using mov_imm_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     57 >(),
                                                     mem_scalar_node /** derives **/ >;
using mov_seg_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     58 >(),
                                                     mov_scalar_node /** derives **/ >;
using mov_seg_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     58 >(),
                                                     mem_scalar_node /** derives **/ >;
using mov_priv_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     59 >(),
                                                     mov_scalar_node /** derives **/ >;
using mov_priv_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     59 >(),
                                                     mem_scalar_node /** derives **/ >;
using movd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     140 >(),
                                                     mov_scalar_node /** derives **/ >;
using movd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     140 >(),
                                                     mem_scalar_node /** derives **/ >;
using movq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     141 >(),
                                                     mov_scalar_node /** derives **/ >;
using movq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     141 >(),
                                                     mem_scalar_node /** derives **/ >;
using movdqu_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     142 >(),
                                                     mov_scalar_node /** derives **/ >;
using movdqu_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     142 >(),
                                                     mem_scalar_node /** derives **/ >;
using movdqa_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     143 >(),
                                                     mov_scalar_node /** derives **/ >;
using movdqa_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     143 >(),
                                                     mem_scalar_node /** derives **/ >;
using movzx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     195 >(),
                                                     mov_scalar_node /** derives **/ >;
using movzx_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     195 >(),
                                                     mem_scalar_node /** derives **/ >;
using movsx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     200 >(),
                                                     mov_scalar_node /** derives **/ >;
using movsx_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     200 >(),
                                                     mem_scalar_node /** derives **/ >;
using movups_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     293 >(),
                                                     mov_scalar_node /** derives **/ >;
using movups_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     293 >(),
                                                     mem_scalar_node /** derives **/ >;
using movss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     294 >(),
                                                     mov_scalar_node /** derives **/ >;
using movss_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     294 >(),
                                                     mem_scalar_node /** derives **/ >;
using movupd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     295 >(),
                                                     mov_scalar_node /** derives **/ >;
using movupd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     295 >(),
                                                     mem_scalar_node /** derives **/ >;
using movsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     296 >(),
                                                     mov_scalar_node /** derives **/ >;
using movsd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     296 >(),
                                                     mem_scalar_node /** derives **/ >;
using movlps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     297 >(),
                                                     mov_scalar_node /** derives **/ >;
using movlps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     297 >(),
                                                     mem_scalar_node /** derives **/ >;
using movlpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     298 >(),
                                                     mov_scalar_node /** derives **/ >;
using movlpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     298 >(),
                                                     mem_scalar_node /** derives **/ >;
using movhps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     303 >(),
                                                     mov_scalar_node /** derives **/ >;
using movhps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     303 >(),
                                                     mem_scalar_node /** derives **/ >;
using movhpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     304 >(),
                                                     mov_scalar_node /** derives **/ >;
using movhpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     304 >(),
                                                     mem_scalar_node /** derives **/ >;
using movaps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     305 >(),
                                                     mov_scalar_node /** derives **/ >;
using movaps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     305 >(),
                                                     mem_scalar_node /** derives **/ >;
using movapd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     306 >(),
                                                     mov_scalar_node /** derives **/ >;
using movapd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     306 >(),
                                                     mem_scalar_node /** derives **/ >;
using movmskps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     323 >(),
                                                     mov_scalar_node /** derives **/ >;
using movmskps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     323 >(),
                                                     mem_scalar_node /** derives **/ >;
using movmskpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     324 >(),
                                                     mov_scalar_node /** derives **/ >;
using movmskpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     324 >(),
                                                     mem_scalar_node /** derives **/ >;
using movs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     387 >(),
                                                     mov_scalar_node /** derives **/ >;
using movs_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     387 >(),
                                                     mem_scalar_node /** derives **/ >;
using movsldup_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     494 >(),
                                                     mov_scalar_node /** derives **/ >;
using movsldup_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     494 >(),
                                                     mem_scalar_node /** derives **/ >;
using movshdup_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     495 >(),
                                                     mov_scalar_node /** derives **/ >;
using movshdup_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     495 >(),
                                                     mem_scalar_node /** derives **/ >;
using movddup_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     496 >(),
                                                     mov_scalar_node /** derives **/ >;
using movddup_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     496 >(),
                                                     mem_scalar_node /** derives **/ >;
using movntss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     540 >(),
                                                     mov_scalar_node /** derives **/ >;
using movntss_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     540 >(),
                                                     mem_scalar_node /** derives **/ >;
using movntsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     541 >(),
                                                     mov_scalar_node /** derives **/ >;
using movntsd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     541 >(),
                                                     mem_scalar_node /** derives **/ >;
using movntdqa_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     557 >(),
                                                     mov_scalar_node /** derives **/ >;
using movntdqa_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     557 >(),
                                                     mem_scalar_node /** derives **/ >;
using movsxd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     597 >(),
                                                     mov_scalar_node /** derives **/ >;
using movsxd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     597 >(),
                                                     mem_scalar_node /** derives **/ >;
using swapgs_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     598 >(),
                                                     mov_scalar_node /** derives **/ >;
using swapgs_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     598 >(),
                                                     mem_scalar_node /** derives **/ >;
using movbe_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     630 >(),
                                                     mov_scalar_node /** derives **/ >;
using movbe_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     630 >(),
                                                     mem_scalar_node /** derives **/ >;
using movq2dq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     953 >(),
                                                     mov_scalar_node /** derives **/ >;
using movq2dq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     953 >(),
                                                     mem_scalar_node /** derives **/ >;
using movdq2q_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     954 >(),
                                                     mov_scalar_node /** derives **/ >;
using movdq2q_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     954 >(),
                                                     mem_scalar_node /** derives **/ >;
using bndmov_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1416 >(),
                                                     mov_scalar_node /** derives **/ >;
using bndmov_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1416 >(),
                                                     mem_scalar_node /** derives **/ >;
using emms_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     151 >(),
                                                     mov_simd_node /** derives **/ >;
using emms_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     151 >(),
                                                     mem_simd_node /** derives **/ >;
using pinsrw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     203 >(),
                                                     mov_simd_node /** derives **/ >;
using pinsrw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     203 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovmskb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     211 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovmskb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     211 >(),
                                                     mem_simd_node /** derives **/ >;
using extrq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     542 >(),
                                                     mov_simd_node /** derives **/ >;
using extrq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     542 >(),
                                                     mem_simd_node /** derives **/ >;
using insertq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     543 >(),
                                                     mov_simd_node /** derives **/ >;
using insertq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     543 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovsxbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     549 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovsxbw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     549 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovsxbd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     550 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovsxbd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     550 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovsxbq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     551 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovsxbq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     551 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovsxwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     552 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovsxwd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     552 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovsxwq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     553 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovsxwq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     553 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovsxdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     554 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovsxdq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     554 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovzxbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     559 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovzxbw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     559 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovzxbd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     560 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovzxbd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     560 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovzxbq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     561 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovzxbq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     561 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovzxwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     562 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovzxwd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     562 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovzxwq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     563 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovzxwq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     563 >(),
                                                     mem_simd_node /** derives **/ >;
using pmovzxdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     564 >(),
                                                     mov_simd_node /** derives **/ >;
using pmovzxdq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     564 >(),
                                                     mem_simd_node /** derives **/ >;
using pinsrb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     587 >(),
                                                     mov_simd_node /** derives **/ >;
using pinsrb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     587 >(),
                                                     mem_simd_node /** derives **/ >;
using pinsrd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     589 >(),
                                                     mov_simd_node /** derives **/ >;
using pinsrd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     589 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovss_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     636 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovss_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     636 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovsd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     637 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovsd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     637 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovups_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     638 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovups_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     638 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovupd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     639 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovupd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     639 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovlps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     640 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovlps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     640 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovsldup_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     641 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovsldup_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     641 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovlpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     642 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovlpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     642 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovddup_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     643 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovddup_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     643 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovhps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     648 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovhps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     648 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovshdup_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     649 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovshdup_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     649 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovhpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     650 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovhpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     650 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovaps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     651 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovaps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     651 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovapd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     652 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovapd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     652 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovntps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     655 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovntps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     655 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovntpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     656 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovntpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     656 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovmskps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     665 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovmskps_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     665 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovmskpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     666 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovmskpd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     666 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     728 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     728 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     735 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     735 >(),
                                                     mem_simd_node /** derives **/ >;
using vpinsrw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     740 >(),
                                                     mov_simd_node /** derives **/ >;
using vpinsrw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     740 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovmskb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     749 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovmskb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     749 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovntdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     767 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovntdq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     767 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovdqu_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     792 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovdqu_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     792 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovdqa_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     793 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovdqa_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     793 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsxbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     821 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsxbw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     821 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsxbd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     822 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsxbd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     822 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsxbq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     823 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsxbq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     823 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsxwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     824 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsxwd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     824 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsxwq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     825 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsxwq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     825 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsxdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     826 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsxdq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     826 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovntdqa_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     829 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovntdqa_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     829 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovzxbw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     831 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovzxbw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     831 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovzxbd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     832 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovzxbd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     832 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovzxbq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     833 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovzxbq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     833 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovzxwd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     834 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovzxwd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     834 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovzxwq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     835 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovzxwq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     835 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovzxdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     836 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovzxdq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     836 >(),
                                                     mem_simd_node /** derives **/ >;
using vpinsrb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     863 >(),
                                                     mov_simd_node /** derives **/ >;
using vpinsrb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     863 >(),
                                                     mem_simd_node /** derives **/ >;
using vpinsrd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     865 >(),
                                                     mov_simd_node /** derives **/ >;
using vpinsrd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     865 >(),
                                                     mem_simd_node /** derives **/ >;
using lwpins_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1053 >(),
                                                     mov_simd_node /** derives **/ >;
using lwpins_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1053 >(),
                                                     mem_simd_node /** derives **/ >;
using kmovw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1107 >(),
                                                     mov_simd_node /** derives **/ >;
using kmovw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1107 >(),
                                                     mem_simd_node /** derives **/ >;
using kmovb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1108 >(),
                                                     mov_simd_node /** derives **/ >;
using kmovb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1108 >(),
                                                     mem_simd_node /** derives **/ >;
using kmovq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1109 >(),
                                                     mov_simd_node /** derives **/ >;
using kmovq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1109 >(),
                                                     mem_simd_node /** derives **/ >;
using kmovd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1110 >(),
                                                     mov_simd_node /** derives **/ >;
using kmovd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1110 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovdqa32_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1243 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovdqa32_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1243 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovdqa64_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1244 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovdqa64_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1244 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovdqu16_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1245 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovdqu16_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1245 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovdqu32_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1246 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovdqu32_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1246 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovdqu64_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1247 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovdqu64_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1247 >(),
                                                     mem_simd_node /** derives **/ >;
using vmovdqu8_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1248 >(),
                                                     mov_simd_node /** derives **/ >;
using vmovdqu8_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1248 >(),
                                                     mem_simd_node /** derives **/ >;
using vpinsrq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1289 >(),
                                                     mov_simd_node /** derives **/ >;
using vpinsrq_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1289 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovb2m_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1298 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovb2m_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1298 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovd2m_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1299 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovd2m_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1299 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovdb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1300 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovdb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1300 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovdw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1301 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovdw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1301 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovm2b_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1302 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovm2b_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1302 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovm2d_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1303 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovm2d_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1303 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovm2q_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1304 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovm2q_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1304 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovm2w_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1305 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovm2w_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1305 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovq2m_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1306 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovq2m_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1306 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovqb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1307 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovqb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1307 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovqd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1308 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovqd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1308 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovqw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1309 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovqw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1309 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsdb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1310 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsdb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1310 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsdw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1311 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsdw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1311 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsqb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1312 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsqb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1312 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsqd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1313 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsqd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1313 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovsqw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1314 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovsqw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1314 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovswb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1315 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovswb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1315 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovusdb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1316 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovusdb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1316 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovusdw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1317 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovusdw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1317 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovusqb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1318 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovusqb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1318 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovusqd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1319 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovusqd_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1319 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovusqw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1320 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovusqw_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1320 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovuswb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1321 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovuswb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1321 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovw2m_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1322 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovw2m_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1322 >(),
                                                     mem_simd_node /** derives **/ >;
using vpmovwb_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1323 >(),
                                                     mov_simd_node /** derives **/ >;
using vpmovwb_mov_as_mem_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1323 >(),
                                                     mem_simd_node /** derives **/ >;
using undecoded_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1 >(),
                                                     node_base_node /** derives **/ >;
using contd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     2 >(),
                                                     node_base_node /** derives **/ >;
using label_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     3 >(),
                                                     node_base_node /** derives **/ >;
using movntps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     102 >(),
                                                     nontemporal_mem_scalar_node /** derives **/ >;
using movntpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     103 >(),
                                                     nontemporal_mem_scalar_node /** derives **/ >;
using movnti_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     202 >(),
                                                     nontemporal_mem_scalar_node /** derives **/ >;
using movntq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     226 >(),
                                                     nontemporal_mem_scalar_node /** derives **/ >;
using movntdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     227 >(),
                                                     nontemporal_mem_scalar_node /** derives **/ >;
using ud2a_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     100 >(),
                                                     nop_node /** derives **/ >;
using nop_modrm_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     101 >(),
                                                     nop_node /** derives **/ >;
using nop_ins_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     381 >(),
                                                     nop_node /** derives **/ >;
using fnop_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     431 >(),
                                                     nop_node /** derives **/ >;
using salc_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     610 >(),
                                                     nop_node /** derives **/ >;
using prefetchnta_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     287 >(),
                                                     prefetch_node /** derives **/ >;
using prefetcht0_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     288 >(),
                                                     prefetch_node /** derives **/ >;
using prefetcht1_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     289 >(),
                                                     prefetch_node /** derives **/ >;
using prefetcht2_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     290 >(),
                                                     prefetch_node /** derives **/ >;
using prefetch_ins_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     291 >(),
                                                     prefetch_node /** derives **/ >;
using prefetchw_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     292 >(),
                                                     prefetch_node /** derives **/ >;
using vpscatterdd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1335 >(),
                                                     scatter_node /** derives **/ >;
using vpscatterdq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1336 >(),
                                                     scatter_node /** derives **/ >;
using vpscatterqd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1337 >(),
                                                     scatter_node /** derives **/ >;
using vpscatterqq_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1338 >(),
                                                     scatter_node /** derives **/ >;
using vscatterdpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1388 >(),
                                                     scatter_node /** derives **/ >;
using vscatterdps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1389 >(),
                                                     scatter_node /** derives **/ >;
using vscatterqpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1390 >(),
                                                     scatter_node /** derives **/ >;
using vscatterqps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1391 >(),
                                                     scatter_node /** derives **/ >;
using vscatterpf0dpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1392 >(),
                                                     scatter_node /** derives **/ >;
using vscatterpf0dps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1393 >(),
                                                     scatter_node /** derives **/ >;
using vscatterpf0qpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1394 >(),
                                                     scatter_node /** derives **/ >;
using vscatterpf0qps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1395 >(),
                                                     scatter_node /** derives **/ >;
using vscatterpf1dpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1396 >(),
                                                     scatter_node /** derives **/ >;
using vscatterpf1dps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1397 >(),
                                                     scatter_node /** derives **/ >;
using vscatterpf1qpd_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1398 >(),
                                                     scatter_node /** derives **/ >;
using vscatterpf1qps_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_mem,
                                                     1399 >(),
                                                     scatter_node /** derives **/ >;
using mwait_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     493 >(),
                                                     wait_node /** derives **/ >;
using monitorx_node  = trace_parser::generic_node< 
                          trace_client::build_token< trace_client::instruction,
                                                     trace_client::insn_not_mem,
                                                     1419 >(),
                                                     wait_node /** derives **/ >;
/** END AUTO_POPULATED REGION **/

} /** end namespace trace_parser **/
                    

#endif /* END BASE_NODE_LIST_HPP */
