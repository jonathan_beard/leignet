/**
 * @version: 2022-09-17T00:00:00
 * @author: Jonathan Beard
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstddef>
#include "class_tree.hpp"
#include <utility>
#include <iostream>
/** 
 * got a bit lazy given I'm adapting this
 * code from a prior project, hopefully this
 * doesn't cause any conflicts within this 
 * compilation unit. 
 */
using namespace trace_parser;

class_tree::~class_tree()
{
   for( auto it( tree.begin() ); it != tree.end(); ++it )
   {
      delete( (*it).second );
   }
}

/**
 * remember, this is pre-processing/slow path, doesn't
 * matter about speed. 
 */
void
class_tree::add_relation( const trace_client::token::token_type_t base, const trace_client::token::token_type_t derived )
{
   /**
    * if base == zero then no parent class, simply add
    * derived to the tree
    */
   if( base == 0 ) 
   {
      tree.insert( std::make_pair( derived, new node( derived ) ) );
   }
   auto base_found( tree.find( base ) );
   auto derived_found( tree.find( derived ) );
   bool base_exists( base_found != tree.end() );
   bool derived_exists( derived_found != tree.end() );

   if( base_exists && ! derived_exists )
   {
      /**
       * case where the base is already present, but
       * the derived class hasn't been entered yet.
       */
       node *parent( base_found->second );
       node *child( new node( derived, parent ) );
       tree.insert( std::make_pair( derived, child ) );
       return;
   }
   else if( ! base_exists && ! derived_exists )
   {
     /**
      * simple case, nothing exists so far,
      * create both and add them to the tree.
      */
      node *parent( new node( base ) );
      node *child( new node( derived, parent ) );
      tree.insert( std::make_pair( base, parent ) );
      tree.insert( std::make_pair( derived, child ) );
      return;
   }
   else if( ! base_exists && derived_exists )
   {
      /** 
       * case exists if we insert a derived 
       * class that has no existing parent, 
       * therefore we make one.  when we attempt
       * to add the base class we end up with a 
       * case where the derived class exists, but
       * with parent as self
       */
      node *parent( new node( base ) );
      node *child( derived_found->second );

      child->parent = parent;

      tree.insert( std::make_pair( base, parent ) );
      return;
   }
   else if( base_exists && derived_exists )
   {
      /** check and see if derived has base as a parent **/
      node *parent( base_found->second );
      node *child( derived_found->second );
      child->parent = parent;
      return;
   }
}

trace_client::token::token_type_t
class_tree::get_closest_to( const trace_client::token::token_type_t class_type,
                            trace_parser::function_map  &f_map )
{
   /**
    * note: you might get some strange errors with this function
    * if there is an astnode that is not in the tree, if you 
    * notice the first line we implicitly check to find an entry
    * point that is one of the leaves in the tree.  if it doesn't
    * exist then there's no entry point and zero is returned.  
    * this behavior can be guarded against by being very careful
    * about adding astnodes to the tree.  its okay to add them
    * more than once, just not zero times.
    */
   auto found( tree.find( class_type ) );

   if( found == tree.end() )
   {
     return( 0 );
   }
   /** else start **/
   node *node( found->second );
   do
   {
      const auto match( f_map.find( node->key ) );
      if( match != f_map.end() )
      {
         /* we've a winner */
         return( (*match) );
      }
      node = node->parent;
   }while( node->parent != node );
   /** no matching class hashes found, return zero **/
   return( 0 );
}

class_tree::node::node( trace_client::token::token_type_t key ) : parent( this ),
                                            key( key )
{

}

class_tree::node::node( trace_client::token::token_type_t key,
                        node   *parent ) : parent( parent ),
                                           key( key )
{

}
