/**
 * function_map.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 */
#ifndef _FUNCTION_MAP_HPP_
#define _FUNCTION_MAP_HPP_  1

#include <set>
#include <map>
#include <functional>
#include "token.hpp"
#include "visit_defs.hpp"

namespace trace_parser
{


using function_map = std::set< trace_client::token::token_type_t 
    /** keyed on class hash type **/ >;

using function_map_impl = std::map< trace_client::token::token_type_t,
                                    trace_parser::visit_func_t >;

}
#endif /* END _FUNCTION_MAP_HPP_ */
