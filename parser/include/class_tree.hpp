/**
 * @version: 2022-09-17T00:00:00
 * @author: Jonathan Beard
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _CLASSTREE_HPP_
#define _CLASSTREE_HPP_ 1

#include <ostream>
#include <map>
#include "function_map.hpp"
#include "token.hpp"

namespace trace_parser
{

class class_tree {
public:   
   class_tree() = default;
   virtual ~class_tree();


   /**
    * addRelation - adds a class relationship to this tree. If the 
    * base class hasn't been added yet, a node will be added for it.
    * When adding a class that has no parent class, simply use zero
    * for the base hash_code().
    * @param   base - const trace_client::token::token_type_t base hash_code()
    * @param   derived - const trace_client::token::token_type_t derived hash_code()
    */
   void     add_relation(   const trace_client::token::token_type_t base, 
                            const trace_client::token::token_type_t derived );
   /**
    * getClosestTo - given the derived class "class_type" find the closes
    * function call in "funciton_classes" that matches "class_type", if
    * no matching base class is found, zero is returned.
    * @param   class_type - const trace_client::token::token_type_t
    * @param   function_map - FunctionMap& with functions to match
    * @return  trace_client::token::token_type_t, closest match out of function_map
    */
   trace_client::token::token_type_t   
   get_closest_to(  const trace_client::token::token_type_t class_type, 
                    trace_parser::function_map &f_map );


private:
   /**
    * private node type, these keep the overall class relations
    * that are described for node types, these are not the "nodes"
    * themselves, just placeholders to keep the specified type 
    * relations. The type relations are specified via the constructors
    * and inheritance relationship. 
    */
   struct node
   {
      node( trace_client::token::token_type_t );

      node( trace_client::token::token_type_t key , node *parent );
      virtual ~node() = default;

      node     *parent;

      trace_client::token::token_type_t   key;
   };

   std::map< trace_client::token::token_type_t, node* > tree;
};

} /** end namespace trace_parser **/
#endif
