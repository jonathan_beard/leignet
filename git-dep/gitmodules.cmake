##
# SET SPECIFIC NEEDED OPTIONS
# - will be ignored by platforms that don't include these options
# so not a huge deal. 
##
#set( DEMANGLE_CUSTOM_NAMESPACE TRUE ) 
#set( DEMANGLE_NAMESPACE "fifo" )
#
#set( AFFINITY_CUSTOM_NAMESPACE TRUE )
#set( AFFINITY_NAMESPACE "fifo" )


##
# list git repo dependencies here
##
set( GIT_MODULES 
        #cmdargs 
        ipc
        #demangle 
        ) 
        #SystemClock )

