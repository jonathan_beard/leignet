/**
 * token.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef TOKEN_HPP
#define TOKEN_HPP  1
#include <cstdint>
#include <iostream>

#include "tracetype.hpp"
#include "ipc_cmds.hpp"
namespace trace_client
{



union token
{
    using token_type_t= std::uint64_t;
    
    constexpr token() = default;

    constexpr token( const trace_client::cmds           &&cmd,
                     const trace_client::trace_type_t   &&t,
                     const std::int32_t                 &&op ) : 
                        cmd( cmd ),
                        type( t  ),
                        op( op ){}

    constexpr token( const token_type_t &&t ) : all( t ){}

#if   defined __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#elif defined __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpedantic"
#endif
    struct
    {
        /** 
         * command, everything under this is a subset, see 
         * specific fields in trace_client::cmds to see the 
         * sub-types. 
         */
         trace_client::cmds          cmd             = trace_client::cmd_not_set; 
        /**
         * this is another 8b unsigned field and the 
         * contents are subsets of the cmd (basically
         * more specific cmds. 
         */
         trace_client::trace_type_t  type            = trace_client::trace_type_not_set;
        /**
         * this isn't currently used, but, not to say 
         * that we won't need an additional 16b in the
         * future. 
         */
         std::uint16_t       reserved        = 0;
        /** opcode **/
         std::int32_t        op              = 0;
    }; 
    token_type_t       all;
#if   defined __GNUC__    
#pragma GCC diagnostic pop
#elif defined __clang__
#pragma clang diagnostic pop
#endif
};
    
/** helper function to get around union not a type **/
template < trace_client::cmds           cmd,
           trace_client::trace_type_t   t,
           std::int32_t                 opcode > 
constexpr static trace_client::token::token_type_t build_token()
{
    trace_client::token::token_type_t out = 0;
    out |= opcode;
    out = out << 24;
    out |= t;
    out = out << 8;
    out |= cmd;
    return( out );
}



} /** end namsepace trace_client **/    
#endif /* END TOKEN_HPP */
