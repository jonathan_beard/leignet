/**
 * @version: 2022-09-17T00:00:00
 * @author: Jonathan Beard
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef IPC_CMDS_H
#define IPC_CMDS_H

#include <cstdint>

namespace trace_client
{

enum cmds : std::uint8_t 
{
    cmd_not_set                 = 0,
    /** 
     * references for data and non_data
     * instructions are in tracetype.hpp 
     * under insn_type_t.
     */
    instruction                 = 1,
    /**
     * indicates actual data operations, 
     * either read/write or neither. 
     */
    data_operation              = 2,
    /**
     * this indicates a code marker region
     * added either by the programmer with
     * the instrumented app or by the trace
     * client.
     */
    marker                      = 3,
    /**
     * indicates end of trace for the 
     * current thread.
     */
    end_of_trace                = 4
};

} /** end namespace traceclient **/
    
#endif /** end IPC_CMDS_H **/
