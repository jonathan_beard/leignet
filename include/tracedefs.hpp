/**
 * tracedefs.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef TRACEDEFS_HPP
#define TRACEDEFS_HPP  1

#include <buffer>

namespace trace_client
{
    /** 
     * definition of segment between both the receiver
     * and trace_client.
     */
    static constexpr ipc::channel_id_t sync_segment_channel = 1;
}
#endif /* END TRACEDEFS_HPP */
