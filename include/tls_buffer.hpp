/**
 * tls_buffer.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef TLS_BUFFER_HPP
#define TLS_BUFFER_HPP  1
#include <atomic>
#include <cstdio>
#include <cstdint>
#include "reference.hpp"
#include <buffer>
#include <sys/types.h>
#include <limits>
#include "shm_segment.hpp"

namespace trace_client
{

/**
 * byte and file_t are defined in globals_api.h,
 * hopefully included by dr_api.h
 */
struct tls_buffer
{
    std::uint8_t                *seg_base   = nullptr;
    trace_client::ref_t         *buf_base   = nullptr;
    FILE                        *logf       = nullptr;
    std::uint64_t               num_refs    = 0;
    ipc::thread_local_data      *ipc_tls    = nullptr;
    trace_client::shm_segment   *sync_seg   = nullptr;
    pid_t                       thread_id   = std::numeric_limits< pid_t >::min();
};

}

#endif /* END TLS_BUFFER_HPP */
