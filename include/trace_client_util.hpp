/**
 * @version: 2022-09-17T00:00:00
 * @author: Jonathan Beard
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ostream>
#include <string>


namespace trace_client
{

/**
 * error - pretty prints an error message with the 
 * file name F, line number W, to the stream at 
 * "stream", with a detailed error message "str"
 * @param F         - should be a string, char, or something 
 * withn a << operator overload to 
 * produce a string and assign it to a ostream.
 * @param W         - "where", needs to be something convertable to an integer, 
 * or produce an integer on an ostream with a << overload. 
 * @param stream    - stream to write to
 * @param str       - detailed error message as a string. 
 */
template < class FF, class WW >
static void 
error(  FF F, 
        const WW W,
        std::ostream &stream = std::cerr,  
        const std::string str = "none" )
{
    stream << "ERROR @file(" << F << ") at line (" << W << "), ";
    stream << "detailed error message: " << str << "\n";
    return;
}

} /** end namespace trace_client **/
