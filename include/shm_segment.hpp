/**
 * shm_segment.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef SHM_SEGMENT_HPP
#define SHM_SEGMENT_HPP  1
#include <atomic>
#include <cstdint>
namespace trace_client
{
struct shm_segment
{
    shm_segment();
    ~shm_segment();
    
    static void init( void *ptr, const std::uint32_t num_cores );

    static void add_new_thread( trace_client::shm_segment *seg );

    static void remove_thread( trace_client::shm_segment *seg );

    std::uint64_t                   sim_time                =   0;
    std::uint64_t                   num_cycles              =   0;
    std::uint32_t                   num_cores               =   0;
    std::atomic< std::uint32_t >    trace_threads_attached  =   0; 
    std::atomic< std::uint32_t >    new_thread_flag         =   0;
};
} /** end namespace trace_client **/

#endif /* END SHM_SEGMENT_HPP */
