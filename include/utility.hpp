/**
 * utility.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef UTILITY_HPP
#define UTILITY_HPP  1
#include <iostream>
#include "reference.hpp"

namespace trace_client
{
struct utility
{
    static void print_references(   void *buffer_seg_start, 
                                    void *buffer_seg_end,
                                    void *data,
                                    const bool use_ipc );

                                    
    static void print_record( const trace_client::ref_t &trace_ref, void *data = nullptr );


}; /** end struct utility **/
} /** end namespace trace_client **/

std::ostream& operator << ( std::ostream &stream, const trace_client::ref_t &trace_ref );
#endif /* END UTILITY_HPP */
