/**
 * reference.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef REFERENCE_HPP
#define REFERENCE_HPP  1

#include <cstdint>
#include <cinttypes>
#include "token.hpp"
#include "tracetype.hpp"
#include "ipc_cmds.hpp"

namespace trace_client
{

enum mem_access_t : std::uint8_t {
    REF_TYPE_READ = 0,
    REF_TYPE_WRITE = 1,
};

struct ref_t 
{
    trace_client::token       token;

    /* r(0), w(1) */
    std::uint8_t        dir             = 0; 
    /* opcode/insn size */
    std::uint16_t       op_size         = 0; 
    /* memory size */
    std::uint16_t       mem_size        = 0; 
    /* addr **/
    std::uintptr_t      addr            = 0;
    /* mem ref addr or instr pc */
    std::uintptr_t      pc              = 0;
    char                op_name[ 16 ]   = {'\0'};
}; 



} /** end trace_client **/

#endif /* END REFERENCE_HPP */
