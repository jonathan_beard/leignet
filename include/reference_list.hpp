/**
 * reference_list.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef REFERENCE_LIST_HPP
#define REFERENCE_LIST_HPP  1
#include "ipc_cmds.hpp"

namespace trace_client
{

/** forward delcare reference.hpp structure **/
class ref_t;

class reference_list
{
public:
    
    reference_list();
    /**
     * constructor, supply a valid trace_client::ref_t pointer
     * as a void pointer. This class is ephemeral (meaning it
     * probably won't live long, it's sole purpose in life
     * is to allow the user to return some begin/end pointers.
     */
    reference_list( void *reference_record );

    /**
     * default destructor, doesn't do much at all.
     */
    ~reference_list()   = default;

    /**
     * returns if the list is at the end or if the 
     * internal record pointer is also null
     */
    bool is_end_of_list( trace_client::cmds &cmd );
    /**
     * returns the actual reference
     */
    trace_client::ref_t& ref();
    
    /** 
     * peek at the next op, useful for sanity checking memory ops
     * to be consumed with an instruction.
     */
    trace_client::ref_t& peek_next_ref();

    /**
     * increments the position in the list. 
     */
    void inc();
    
    /** 
     * provide a new record to iterate over.
     */
    void set_record( void *r );
    
    /**
     * returns the raw record pointer, can be 
     * null. 
     */
    trace_client::ref_t*    get_raw_record();

private:
    trace_client::ref_t     *record = nullptr;
    trace_client::ref_t     *record_start_ptr = nullptr;
};

} /** end namespace trace_client **/
#endif /* END REFERENCE_LIST_HPP */
