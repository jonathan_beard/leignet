/**
 * @version: 2022-09-17T00:00:00
 * @author: Jonathan Beard
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _CLIENT_H_ 
#define _CLIENT_H_ 1
#include <string>
#include <cstdint>
#include <buffer>
#include "utildefs.hpp"

#include "dr_api.h"
#include "ipc_cmds.hpp"
#include "tls_buffer.hpp"



#define MINSERT instrlist_meta_preinsert

namespace trace_client
{

using tls_type = tls_buffer;

/** 
 * initialize - function in namespace trace_client that
 * calls the initalization stuff inside the main client. It'd 
 * be a bit silly to go through all the work of encapsulation of 
 * stuff and yet require the programmer looking at higher level
 * stuff to understand and know the order of everything that 
 * is supposed to be called at some point in the future. 
 */
void
initialize();


class trace_events
{
private:
    /**
     * just in case dynamorio decides to overwride the default thread
     * id, and because DR doesn't seem to implement a TID specific overload
     * (it does have a PID overload), let's write our own here. 
     */
    static pid_t    get_tid();
public:

/** 
 * Max number of mem_ref a buffer can have. It should be big enough
 * to hold all entries between clean calls. Right now we assume that a 
 * basic block is less than or equal to max_num_refs for simplicity
 */
static constexpr std::size_t max_num_refs           = 
    (1<<13);
static constexpr std::size_t local_buffer_size      = 
    sizeof( trace_client::ref_t ) * ( max_num_refs );

 /*
 * keep in this compilation unit only
 * this describes where we actually want
 * the TLS field to go....
 */
static  int             tls_field;
static  reg_id_t        tls_seg;
static  uint            tls_offs;
static  client_id_t     client_id;
static  bool            use_ipc_buffer;
static  shm_key_t       ipc_key_name;
static  std::string     pipe_name;
static  ipc::buffer     *ipc_buffer;

enum trace_slots : int 
{
    tls_offset_buff_ptr,
    tls_slot_count
};


    inline static void**
    tls_slot( byte *base, const trace_client::trace_events::trace_slots slot )
    {
        return( (void**)(((byte*)(base)) + trace_client::trace_events::tls_offs + slot ));
    }
   
    template < class T >  static T**
    buf_ptr( byte *base )
    {
        return( (T **) trace_client::trace_events::tls_slot( base, 
                                                             tls_offset_buff_ptr ) );
    }

    /**
     * called by clean call. 
     */
    static void
    trace(void *drcontext, const bool force);


    /**
     * implement clean call, TODO, double check on function signature
     * because it looks like we should be able to do any function sig
     * that we like.
     */
    static 
    void clean_call();

    static void
    insert_load_buf_ptr(    void *drcontext, 
                            instrlist_t *ilist, 
                            instr_t *where, 
                            reg_id_t &reg_ptr );

    static void
    insert_update_buf_ptr(  void *drcontext, 
                            instrlist_t *ilist, 
                            instr_t *where,
                            reg_id_t &reg_ptr, 
                            int adjust );
    
    static void
    insert_save_type(    void                               *drcontext, 
                           instrlist_t                      *ilist, 
                           instr_t                          *where, 
                           reg_id_t                         base,
                           reg_id_t                         scratch, 
                           const trace_client::trace_type_t type );
    
    static void
    insert_save_cmd(       void        *drcontext, 
                           instrlist_t *ilist, 
                           instr_t     *where, 
                           reg_id_t    base,
                           reg_id_t    scratch, 
                           const trace_client::cmds   cmd );


    static void
    insert_zero_save_reg( void *drcontext, 
                          instrlist_t *ilist, 
                          instr_t *where, 
                          reg_id_t base,
                          reg_id_t scratch,
                          const bool is_source );

    static void
    reserve_registers( void *drcontext,
                       instrlist_t *ilist,
                       instr_t     *where,
                       reg_id_t    &a,
                       reg_id_t    &b,
                       reg_id_t    &c );
    
    static void
    unreserve_registers(   void *drcontext,
                           instrlist_t *ilist,
                           instr_t     *where,
                           reg_id_t    &a,
                           reg_id_t    &b,
                           reg_id_t    &c );

    static void
    insert_save_register( void *drcontext, 
                          instrlist_t *ilist, 
                          instr_t *where, 
                          reg_id_t base,
                          reg_id_t scratch,
                          reg_id_t reg /** reg to save **/,
                          ushort   reg_count,
                          const bool is_source );

    static void
    insert_save_opcode(   void *drcontext, 
                        instrlist_t *ilist, 
                        instr_t *where, 
                        reg_id_t base,
                        reg_id_t scratch, 
                        std::int32_t opcode );
    
    static void
    insert_save_zero(   void *drcontext, 
                        instrlist_t *ilist, 
                        instr_t *where, 
                        reg_id_t base,
                        reg_id_t scratch ); 
    
    static void
    insert_save_direction(   void *drcontext, 
                             instrlist_t *ilist, 
                             instr_t *where, 
                             reg_id_t base,
                             reg_id_t scratch, 
                             const trace_client::mem_access_t d );

    static void
    insert_save_op_size(   void *drcontext, 
                           instrlist_t *ilist, 
                           instr_t *where, 
                           reg_id_t base,
                           reg_id_t scratch, 
                           const std::uint16_t size );
    
    static void
    insert_save_mem_size(   void *drcontext, 
                            instrlist_t *ilist, 
                            instr_t *where, 
                            reg_id_t base,
                            reg_id_t scratch, 
                            const std::uint16_t size );

    static void
    insert_save_pc( void *drcontext, 
                    instrlist_t *ilist, 
                    instr_t *where, 
                    reg_id_t base,
                    reg_id_t scratch, 
                    app_pc   _pc );


#if 0
    static void
    insert_insn(      void *drcontext, 
                      instrlist_t *ilist, 
                      instr_t   *where, 
                      reg_id_t  base,
                      reg_id_t  scratch, 
                      char      b[64],
                      std::size_t length );
#endif
    
    static void
    insert_insn_name(   void *drcontext, 
                        instrlist_t *ilist, 
                        instr_t   *where, 
                        reg_id_t  base,
                        reg_id_t  scratch, 
                        const char *opcode_buffer );

    static void
    insert_save_addr(   void *drcontext, 
                        instrlist_t *ilist, 
                        instr_t *where, 
                        opnd_t ref,
                        reg_id_t reg_ptr, 
                        reg_id_t reg_addr,
                        reg_id_t scratch );

    static void
    instrument_instr(   void *drcontext, 
                        instrlist_t *ilist, 
                        instr_t *where,
                        instr_t *instr,
                        reg_id_t &base,
                        reg_id_t &scratch );


    static void
    instrument_mem( void *drcontext, 
                    instrlist_t *ilist, 
                    instr_t *where, 
                    opnd_t ref,
                    bool write,
                    reg_id_t reg_ptr,
                    reg_id_t reg_tmp,
                    reg_id_t reg_tmp2 );
    
    
    /** 
     * Interestingly enough, DR calls this on every single darned 
     * instruction. So....we have to use a special call within this
     * which is "drmgr_is_first_instr(drcontext, inst)" to determine
     * if this is the first instruction within a basic block, at which
     * point we can do our bb instrumentation, otherwise, we keep going.
     * -event_app_instruction 
     */
    static 
    dr_emit_flags_t
    event_instruction_callback(  void        *drcontext, 
                                 void        *tag,
                                 instrlist_t *instruction_list,
                                 instr_t     *inst,
                                 bool        for_trace,
                                 bool        translating,
                                 void        *user_data );
 


    /**
     * event_bb_app2app - entirely borrowed from example in memtrace_simple.c
     * line 329, the documentation says that this function when registered
     * is called before the other functions and/or intstrumentation and is
     * supposed to be used to assist in said instrumentation. 
     * at the moment all this function does is call another helper 
     * function that "unrolls" loop instructions like rep/repne to 
     * make it easier for DR to get the addresses (which is one thing
     * we're trying to do here.
     */
    static
    dr_emit_flags_t
    event_bb_app2app(   void *drcontext, 
                        void *tag, 
                        instrlist_t *bb, 
                        bool for_trace,
                        bool translating,
                        void **user_data );

    static
    dr_emit_flags_t
    event_bb_analysis(  void *drcontext, 
                        void *tag, 
                        instrlist_t *bb, 
                        bool for_trace, 
                        bool translating, 
                        void *user_data );
    

    static
    dr_emit_flags_t
    event_bb_instru2instru(  void *drcontext, 
                             void *tag, 
                             instrlist_t *bb, 
                             bool for_trace, 
                             bool translating, 
                             void *user_data );
    
    /**
     * fork_init - 
     */
    static
    void 
    fork_init( void *drcontext );
    
    /** 
     * event_thread_init - called on each thread initialization,
     */
    static
    void
    event_thread_init( void *drcontext );
    
    
    static 
    void 
    event_thread_exit( void *drcontext );
    

    /**
     * event_exit - do things that need to happen on exit, 
     * do things like unregister functions, etc. opposite
     * of initialize call with drmgr_init. 
     */
    static 
    void event_exit( void );

};
    
    
} /** end namespace trace_client **/

#endif                    
