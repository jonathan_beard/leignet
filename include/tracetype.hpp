/**
 * tracetype.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef TRACETYPE_HPP
#define TRACETYPE_HPP  1
#include <cstdint>

namespace trace_client
{

/** set as 8b unsigned integer **/
using trace_type_t  = std::uint8_t;

/** 
 * static global so we don't have to go everywhere to 
 * change the unset value 
 */
/**
 * FIXME - move this to a more generic header file, set all
 * "not_set" type of vars that are IPC commands to this. 
 */
constexpr static trace_client::trace_type_t trace_type_not_set = 0;

enum insn_type_t : std::uint8_t 
{
    /** not set, self explanatory **/
    insn_not_set             = trace_client::trace_type_not_set,
    /** instructions that are not memory operations **/
    insn_not_mem        = 1,
    /** an actual memory instruction (but not the read/write VA)**/
    insn_mem            = 2
};



}

#endif /* END TRACETYPE_HPP */
