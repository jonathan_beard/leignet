/**
 * @version: 2022-09-17T00:00:00
 * @author: Jonathan Beard
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <cassert>
#include <exception>
#include <buffer>

#include <unistd.h>
#include <sys/syscall.h>

#include "utility.hpp"
#include "dr_api.h"
#include "drmgr.h"
#include "drx.h"
#include "drreg.h"
#include "drutil.h"


#include "client.hpp"
#include "reference.hpp"
#include "trace_client_util.hpp"
#include "tracedefs.hpp"


int             trace_client::trace_events::tls_field       =   -1;
reg_id_t        trace_client::trace_events::tls_seg         =   DR_REG_NULL;
uint            trace_client::trace_events::tls_offs        =   0;
client_id_t     trace_client::trace_events::client_id       =   0;
shm_key_t       trace_client::trace_events::ipc_key_name    =   { shm_initial_key } ;
std::string     trace_client::trace_events::pipe_name       =   "";
bool            trace_client::trace_events::use_ipc_buffer  =   false;
ipc::buffer     *trace_client::trace_events::ipc_buffer     =   nullptr;


pid_t
trace_client::trace_events::get_tid()
{
    return( ::syscall( SYS_gettid ) );
}

void 
trace_client::trace_events::trace( void *drcontext, const bool force )
{
    auto *data = 
        (trace_client::tls_buffer*) drmgr_get_tls_field( drcontext, 
        trace_client::trace_events::tls_field );
    DR_ASSERT( data != nullptr );
    auto **seg_base = 
        trace_client::trace_events::buf_ptr< trace_client::ref_t >( data->seg_base );
    //check size
    const int ref_count = std::distance( data->buf_base, *seg_base );
    //bulk up IPC
    if( ref_count >= 4096 || force )
    {
        
        auto *trace_ref = (trace_client::ref_t*)data->buf_base;
        auto *trace_ref_end = (trace_client::ref_t*)*seg_base;
        
        //add null terminator
        reinterpret_cast< trace_client::ref_t* >( data->buf_base )
        [ 
            std::distance( trace_ref, trace_ref_end )
        ].token.cmd = trace_client::cmd_not_set;
        
        while( 
            ipc::buffer::send_record( data->ipc_tls, 
                                      data->thread_id  /** channel_id **/, 
                                      (void**)&data->buf_base ) != ipc::tx_success );
        
        //allocate new IPC record for our thread 
        void *output = nullptr;
        while( output == nullptr )
        {
            output = 
                     ipc::buffer::allocate_record( data->ipc_tls, 
                                                   trace_client::trace_events::local_buffer_size,
                                                   data->thread_id  /** channel_id **/ );
        }
        //assign that new record to our channel buf_base
        data->buf_base = (trace_client::ref_t*)output;
        
        auto temp_ptr = 
            trace_client::trace_events::buf_ptr< trace_client::ref_t >(data->seg_base);
        *temp_ptr = data->buf_base;
    }
}

void 
trace_client::trace_events::clean_call()
{
    auto *drcontext = dr_get_current_drcontext();
    trace_client::trace_events::trace( drcontext, false );
    /** thats a wrap **/
    return;
}

void
trace_client::trace_events::insert_load_buf_ptr(    void        *drcontext, 
                                                    instrlist_t *ilist, 
                                                    instr_t     *where, 
                                                    reg_id_t    &reg_ptr )
{
    dr_insert_read_raw_tls
    ( drcontext, 
      ilist, 
      where, 
      trace_client::trace_events::tls_seg,
      trace_client::trace_events::tls_offs + 
        trace_client::trace_events::tls_offset_buff_ptr, 
                            reg_ptr );
    return;
}

void
trace_client::trace_events::insert_update_buf_ptr(  void        *drcontext, 
                                                    instrlist_t *ilist, 
                                                    instr_t     *where,
                                                    reg_id_t    &reg_ptr, 
                                                    int         adjust )
{
    MINSERT(
        ilist, 
        where,
        XINST_CREATE_add(   drcontext, 
                            opnd_create_reg( reg_ptr ), 
                            OPND_CREATE_INT16( adjust ) ) );

    dr_insert_write_raw_tls(    drcontext, 
                                ilist, 
                                where, 
                                trace_client::trace_events::tls_seg,
                                trace_client::trace_events::tls_offs + 
                                trace_client::trace_events::tls_offset_buff_ptr, 
                                reg_ptr );
    return;
}

void
trace_client::trace_events::insert_save_type(    void        *drcontext, 
                                                    instrlist_t *ilist, 
                                                    instr_t     *where, 
                                                    reg_id_t    base,
                                                    reg_id_t    scratch, 
                                                    const trace_type_t       type)
{
    scratch = reg_resize_to_opsz(scratch, OPSZ_1);
    MINSERT(ilist, where,
            XINST_CREATE_load_int(  drcontext, 
                                    opnd_create_reg(scratch),
                                    OPND_CREATE_INT8( type ) ) );
    MINSERT(ilist, where,
            XINST_CREATE_store_1byte(drcontext,
                                      OPND_CREATE_MEM8(base, 
                                        offsetof(trace_client::ref_t, token.type)),
                                      opnd_create_reg(scratch)));
    return;
}
    
void
trace_client::trace_events::insert_save_cmd(       void                         *drcontext, 
                                                   instrlist_t                  *ilist, 
                                                   instr_t                      *where, 
                                                   reg_id_t                     base,
                                                   reg_id_t                     scratch, 
                                                   const trace_client::cmds     cmd )
{
    scratch = reg_resize_to_opsz(scratch, OPSZ_1);
    MINSERT(ilist, where,
            XINST_CREATE_load_int(  drcontext, 
                                    opnd_create_reg(scratch),
                                    OPND_CREATE_INT8( cmd ) ) );
    MINSERT(ilist, where,
            XINST_CREATE_store_1byte(drcontext,
                                      OPND_CREATE_MEM8(base, 
                                        offsetof(trace_client::ref_t, token.cmd)),
                                      opnd_create_reg(scratch)));
    return;
}



void
trace_client::trace_events::insert_save_opcode(   void *drcontext, 
                                                  instrlist_t *ilist, 
                                                  instr_t *where, 
                                                  reg_id_t base,
                                                  reg_id_t scratch, 
                                                  std::int32_t opcode )
{
    scratch = reg_resize_to_opsz(scratch, OPSZ_4);
    MINSERT(ilist, where,
            XINST_CREATE_load_int(drcontext, opnd_create_reg(scratch),
                                  OPND_CREATE_INT32(opcode)));
    MINSERT(ilist, where,
            XINST_CREATE_store(drcontext,
                               OPND_CREATE_MEM32(base, offsetof(trace_client::ref_t, token.op )),
                               opnd_create_reg(scratch)));
    return;
}
    
void
trace_client::trace_events::insert_save_zero(   void *drcontext, 
                                                instrlist_t *ilist, 
                                                instr_t *where, 
                                                reg_id_t base,
                                                reg_id_t scratch )
{
    scratch = reg_resize_to_opsz(scratch, OPSZ_2);
    MINSERT(ilist, where,
            XINST_CREATE_load_int(drcontext, opnd_create_reg(scratch),
                                  OPND_CREATE_INT16( 0 )));
    MINSERT(ilist, where,
            XINST_CREATE_store_2bytes(drcontext,
                                      OPND_CREATE_MEM16(base, 
                                        offsetof( trace_client::ref_t, token.reserved )),
                                      opnd_create_reg(scratch)));
}

void
trace_client::trace_events::insert_save_op_size(   void *drcontext, 
                                                   instrlist_t *ilist, 
                                                   instr_t *where, 
                                                   reg_id_t base,
                                                   reg_id_t scratch, 
                                                   const std::uint16_t size )
{
    scratch = reg_resize_to_opsz(scratch, OPSZ_2);
    MINSERT(ilist, where,
            XINST_CREATE_load_int(drcontext, opnd_create_reg(scratch),
                                  OPND_CREATE_INT16(size)));
    MINSERT(ilist, where,
            XINST_CREATE_store_2bytes(drcontext,
                                      OPND_CREATE_MEM16(base, 
                                        offsetof( trace_client::ref_t, op_size)),
                                      opnd_create_reg(scratch)));
    return;
}

void
trace_client::trace_events::insert_save_mem_size(   void *drcontext, 
                                                    instrlist_t *ilist, 
                                                    instr_t *where, 
                                                    reg_id_t base,
                                                    reg_id_t scratch, 
                                                    const std::uint16_t size )
{
    scratch = reg_resize_to_opsz(scratch, OPSZ_2);
    MINSERT(ilist, where,
            XINST_CREATE_load_int(drcontext, opnd_create_reg(scratch),
                                  OPND_CREATE_INT16(size)));
    MINSERT(ilist, where,
            XINST_CREATE_store_2bytes(drcontext,
                                      OPND_CREATE_MEM16(base, 
                                      offsetof(trace_client::ref_t, mem_size)),
                                      opnd_create_reg(scratch)));
    return;
}

void
trace_client::trace_events::insert_save_pc(     void *drcontext, 
                                                instrlist_t *ilist, 
                                                instr_t *where, 
                                                reg_id_t base,
                                                reg_id_t scratch, 
                                                app_pc _pc )
{
    instrlist_insert_mov_immed_ptrsz(   drcontext, 
                                        (ptr_int_t)_pc, 
                                        opnd_create_reg(scratch),
                                        ilist, 
                                        where, 
                                        nullptr, 
                                        nullptr);
    MINSERT(ilist, 
            where,
            XINST_CREATE_store(drcontext,
                               OPND_CREATE_MEMPTR(base, 
                                                  offsetof( trace_client::ref_t, 
                                                            pc )
                                                 ),
                               opnd_create_reg(scratch)
                               )
           );
    return;
}
  
#if 0
//use if you want to dump the full decode to output
void
trace_client::trace_events::insert_insn(        void *drcontext, 
                                                instrlist_t *ilist, 
                                                instr_t *where, 
                                                reg_id_t reg_ptr,
                                                reg_id_t scratch, 
                                                char b[64],
                                                std::size_t length )
{
    int index = 0;
    while( length -= 8 > 0 )
    {
        const auto value = (*(std::uint64_t*)(&b[index]));
        instrlist_insert_mov_immed_ptrsz(   drcontext, 
                                            value, 
                                            opnd_create_reg(scratch),
                                            ilist, 
                                            where, 
                                            nullptr, 
                                            nullptr);
        const auto offset = offsetof( trace_client::ref_t, buffer ) + index;
        MINSERT(ilist, 
                where,
                XINST_CREATE_store(drcontext,
                                   OPND_CREATE_MEMPTR(reg_ptr, offset ), 
                                   opnd_create_reg(scratch)
                                   )
           );
        index += 8;
    }
    return;
}
#endif


void
trace_client::trace_events::insert_insn_name(        void *drcontext, 
                                                instrlist_t *ilist, 
                                                instr_t *where, 
                                                reg_id_t reg_ptr,
                                                reg_id_t scratch, 
                                                const char *opcode_buffer )
{
    int index = 0;
    int length = 16;
    while( length -= 8 > 0 )
    {
        const auto value = (*(std::uint64_t*)(&opcode_buffer[index]));
        instrlist_insert_mov_immed_ptrsz(   drcontext, 
                                            value, 
                                            opnd_create_reg(scratch),
                                            ilist, 
                                            where, 
                                            nullptr, 
                                            nullptr);
        const auto offset = offsetof( trace_client::ref_t, op_name ) + index;
        MINSERT(ilist, 
                where,
                XINST_CREATE_store(drcontext,
                                   OPND_CREATE_MEMPTR(reg_ptr, offset ), 
                                   opnd_create_reg(scratch)
                                   )
           );
        index += 8;
    }
    return;
}

void
trace_client::trace_events::insert_save_addr(   void *drcontext, 
                                                instrlist_t *ilist, 
                                                instr_t *where, 
                                                opnd_t ref,
                                                reg_id_t reg_ptr, 
                                                reg_id_t reg_addr,
                                                reg_id_t scratch )
{
    /* we use reg_ptr as scratch to get addr */
    const auto ok = drutil_insert_get_mem_addr(drcontext, ilist, where, ref, reg_addr, scratch);
    DR_ASSERT(ok);
    MINSERT(    ilist, 
                where,
                XINST_CREATE_store( drcontext,
                                    OPND_CREATE_MEMPTR( reg_ptr, 
                                                        offsetof( trace_client::ref_t, addr ) ),
                                                        opnd_create_reg( reg_addr ) ) );
    return;
}


void
trace_client::trace_events::reserve_registers(  void *drcontext,
                                                instrlist_t *list,
                                                instr_t     *where,
                                                reg_id_t    &a,
                                                reg_id_t    &b,
                                                reg_id_t    &c )
{
    if (drreg_reserve_register( drcontext, 
                                list, 
                                where, 
                                nullptr, 
                                &a) != DRREG_SUCCESS )
    {
        DR_ASSERT(false); /* cannot recover */
        return;
    }

    if( drreg_reserve_register( drcontext, 
                                list, 
                                where, 
                                nullptr, 
                                &b ) != DRREG_SUCCESS )
    {
        DR_ASSERT(false); /* cannot recover */
        return;
    }
    if( drreg_reserve_register( drcontext, 
                                list, 
                                where, 
                                nullptr, 
                                &c ) != DRREG_SUCCESS )
    {
        DR_ASSERT(false); /* cannot recover */
        return;
    }

}

void
trace_client::trace_events::unreserve_registers( void *drcontext,
                                                 instrlist_t *list,
                                                 instr_t     *where,
                                                 reg_id_t    &a,
                                                 reg_id_t    &b,
                                                 reg_id_t    &c )
{
    /* Restore scratch registers */
    if (drreg_unreserve_register(drcontext, list, where, a) != DRREG_SUCCESS ||
        drreg_unreserve_register(drcontext, list, where, b) != DRREG_SUCCESS || 
        drreg_unreserve_register(drcontext, list, where, c) != DRREG_SUCCESS )
    {
        DR_ASSERT(false);
    }
}

void
trace_client::trace_events::instrument_instr(   void *drcontext, 
                                                instrlist_t *ilist, 
                                                instr_t *where,
                                                instr_t *instr,
                                                reg_id_t    &reg_ptr,
                                                reg_id_t    &reg_tmp )
{
    /* we don't want to predicate this, because an instruction fetch always occurs */
    //instrlist_set_auto_predicate( ilist, DR_PRED_NONE );
            
     
    
    const std::int32_t insn_opcode = instr_get_opcode( instr );

    insert_save_opcode(   drcontext, 
                          ilist, 
                            where, 
                            reg_ptr, 
                            reg_tmp,
                            insn_opcode
                    );
    


    insert_insn_name(   drcontext, 
                        ilist, 
                        where, 
                        reg_ptr, 
                        reg_tmp,
                        decode_opcode_name( insn_opcode )
                    );
    
    insert_save_op_size(   drcontext, 
                           ilist, 
                           where, 
                           reg_ptr, 
                           reg_tmp,
                           (std::uint16_t) instr_length(  drcontext, 
                                                          instr )
                    );
    //decode_sizeof(void *    drcontext,      byte *  pc, nullptr )
    insert_save_pc( drcontext, 
                    ilist, 
                    where, 
                    reg_ptr, 
                    reg_tmp, 
                    instr_get_app_pc(instr) );
    

    //instrlist_set_auto_predicate(ilist, instr_get_predicate(where));
}

void
trace_client::trace_events::insert_save_direction(   void *drcontext, 
                                                instrlist_t *ilist, 
                                                instr_t *where, 
                                                reg_id_t base,
                                                reg_id_t scratch, 
                                                const trace_client::mem_access_t d )
{
    scratch = reg_resize_to_opsz(scratch, OPSZ_1);
    MINSERT(ilist, where,
            XINST_CREATE_load_int(drcontext, opnd_create_reg(scratch),
                                  OPND_CREATE_INT8(d)));
    MINSERT(ilist, where,
            XINST_CREATE_store_1byte(drcontext,
                                      OPND_CREATE_MEM8(base, offsetof(trace_client::ref_t, dir)),
                                      opnd_create_reg(scratch)));
    return;
}



/* insert inline code to add a memory reference info entry into the buffer */
void
trace_client::trace_events::instrument_mem( void *drcontext, 
                                            instrlist_t *ilist, 
                                            instr_t *where, 
                                            opnd_t ref,
                                            bool write,
                                            reg_id_t reg_ptr,
                                            reg_id_t reg_tmp, 
                                            reg_id_t reg_tmp2 )
{
    /* save_addr should be called first as reg_ptr or reg_tmp maybe used in ref */
    insert_save_addr( drcontext, ilist, where, ref, reg_ptr, reg_tmp, reg_tmp2 );
    
    insert_save_direction(   drcontext, 
                        ilist, 
                        where, 
                        reg_ptr, 
                        reg_tmp,
                        write ? REF_TYPE_WRITE : REF_TYPE_READ );

    insert_save_mem_size(   drcontext, 
                            ilist, 
                            where, 
                            reg_ptr, 
                            reg_tmp,
                            (std::uint16_t) drutil_opnd_mem_size_in_bytes(ref, where)); 
}


/**
 * this is the saem as event_app_instruction in most of the
 * DR examples, also with a different API.
 */
dr_emit_flags_t
trace_client::trace_events::event_instruction_callback(  
                    void        *drcontext, 
                    void        *tag,
                    instrlist_t *instruction_list,
                    instr_t     *where,
                    bool        for_trace,
                    bool        translating,
                    void        *user_data )
{
    /** 
     * Unfortunately we have to keep the signature the same, 
     * but...conflicts a bit if we want all modern C++ error 
     * checking on, so we use UNUSED to fix.
     * ALSO
     * FIXME - update so that we run through instruction_list
     * given it's a basic_block, and then we call clean_call
     * only once we're done with the block, doing this on 
     * every instruction is expensive. 
     */
    UNUSED( tag );
    UNUSED( for_trace );
    UNUSED( translating );
    UNUSED( user_data );
    
    
    auto instr = drmgr_orig_app_instr_for_fetch( drcontext );
    if( instr == nullptr )
    {
        return DR_EMIT_DEFAULT;
    }
    if(! instr_is_app(instr) )
    {
        return DR_EMIT_DEFAULT;
    }
    //we get here should be true 
    DR_ASSERT( instr_is_app( instr ) );
    /** get registers **/
    reg_id_t reg_base, reg_scratch, reg_scratch2;

    
    trace_client::trace_events::reserve_registers(   drcontext, 
                                                     instruction_list, 
                                                     where, 
                                                     reg_base, 
                                                     reg_scratch,
                                                     reg_scratch2 );
    
    /** insert code to add an entry for app instruction **/
    insert_load_buf_ptr( drcontext, 
                         instruction_list, 
                         where, 
                         reg_base );


    auto incr_buffer = [&]()
    {
        trace_client::trace_events::insert_update_buf_ptr(  
                            drcontext, 
                            instruction_list, 
                            where, 
                            reg_base, 
                            sizeof(trace_client::ref_t) );
    };

    auto exit_trace = [&](){
        trace_client::trace_events::unreserve_registers(    drcontext,
                                                            instruction_list,
                                                            where,
                                                            reg_base, 
                                                            reg_scratch,
                                                            reg_scratch2 );
                                                                    
    };
    

    instrument_instr(   drcontext, 
                        instruction_list, 
                        where, 
                        instr, 
                        reg_base, 
                        reg_scratch );

    //clear reserved bits in reference.token 
    insert_save_zero( drcontext,
                      instruction_list,
                      where,
                      reg_base,
                      reg_scratch );
     
    /** 
     * FIXME - once we have syscalls, calls and markers, add in more types here
     */
    /** cmd is default, change if call, mem ops are tallied below **/
    auto curr_cmd = trace_client::instruction; 
    insert_save_cmd(   drcontext, 
                       instruction_list,
                       where,
                       reg_base,
                       reg_scratch,
                       curr_cmd
                       );
    
    /**
     * what we want here is the specific command from 
     * ipc_cmds.h, which means we need to know the type
     * here, earlier than we'd originally planned. 
     */
    auto instr_operands = drmgr_orig_app_instr_for_operands( drcontext );
    
    const bool is_data_instruction( 
        instr_operands != nullptr && 
        ( instr_reads_memory( instr_operands ) || instr_writes_memory( instr_operands ) )
    );
    
    auto curr_type = trace_client::insn_not_set;
    if( is_data_instruction )
    {
        curr_type = trace_client::insn_mem; 
    }
    else
    {
        curr_type = trace_client::insn_not_mem;
    }
    
    insert_save_type(   drcontext, 
                        instruction_list,
                        where, 
                        reg_base,
                        reg_scratch,
                        curr_type );
    

     
    incr_buffer();
    
    if( ! is_data_instruction ) 
    {
        exit_trace();
        goto COMPLETE;
    }
        

    DR_ASSERT( instr_is_app( instr_operands ) );

    //else
    /* insert code to add an entry for each memory reference opnd */
    for( auto i = 0; i < instr_num_srcs( instr_operands ); i++) 
    {
        opnd_t src_op;
        if( opnd_is_memory_reference( (src_op = instr_get_src( instr_operands, i ) ) ) )
        {
            insert_save_cmd(   drcontext, 
                               instruction_list,
                               where,
                               reg_base,
                               reg_scratch,
                               trace_client::data_operation 
                               );
            instrument_mem(     drcontext, 
                                instruction_list, 
                                where,
                                src_op, 
                                false,
                                reg_base,
                                reg_scratch,
                                reg_scratch2 );
            incr_buffer();                                
        }
    }

    for( auto i = 0; i < instr_num_dsts(instr_operands); i++) 
    {
        opnd_t dst_op;
        if( opnd_is_memory_reference( (dst_op = instr_get_dst(instr_operands, i) ) ) )
        {
            insert_save_cmd(   drcontext, 
                               instruction_list,
                               where,
                               reg_base,
                               reg_scratch,
                               trace_client::data_operation 
                               );
            instrument_mem(     drcontext, 
                                instruction_list, 
                                where,
                                dst_op, 
                                true,
                                reg_base,
                                reg_scratch,
                                reg_scratch2 );
            incr_buffer();                                
        }
    }
    exit_trace();
COMPLETE:

    /* insert code to call clean_call for processing the buffer */
    if (/* XXX i#1698: there are constraints for code between ldrex/strex pairs,
         * so we minimize the instrumentation in between by skipping the clean call.
         * As we're only inserting instrumentation on a memory reference, and the
         * app should be avoiding memory accesses in between the ldrex...strex,
         * the only problematic point should be before the strex.
         * However, there is still a chance that the instrumentation code may clear the
         * exclusive monitor state.
         * Using a fault to handle a full buffer should be more robust, and the
         * forthcoming buffer filling API (i#513) will provide that.
         */
        IF_AARCHXX_ELSE(!instr_is_exclusive_store(instr_operands), true))
        dr_insert_clean_call(
            drcontext, 
            instruction_list, 
            where, 
            (void *)clean_call,
            false   /** save fp state **/ , 
            0       /** args **/
            );

    return( DR_EMIT_DEFAULT );
}

/**
 * event_bb_app2app - entirely borrowed from example in memtrace_simple.c
 * line 329, the documentation says that this function when registered
 * is called before the other functions and/or intstrumentation and is
 * supposed to be used to assist in said instrumentation. 
 * at the moment all this function does is call another helper 
 * function that "unrolls" loop instructions like rep/repne to 
 * make it easier for DR to get the addresses (which is one thing
 * we're trying to do here.
 */
dr_emit_flags_t
trace_client::trace_events::event_bb_app2app(   void *drcontext, 
                                                void *tag, 
                                                instrlist_t *bb, 
                                                bool for_trace,
                                                bool translating,
                                                void **user_data )
{
    UNUSED( tag         );
    UNUSED( for_trace   );
    UNUSED( translating );
    UNUSED( user_data   );
    if( ! drutil_expand_rep_string( drcontext, bb ) ) 
    {
        trace_client::error( __FILE__, 
                             __LINE__, 
                             std::cerr, 
                             "Failed to unroll string rep instructions, address collection might be skewed" );
        /** who really cares? it'll keep going right? **/ 
    }
    
    if( ! drx_expand_scatter_gather(drcontext, bb, nullptr)) 
    {
        trace_client::error( __FILE__, 
                             __LINE__, 
                             std::cerr, 
                             "Failed to expand gather/scatter  instructions, address collection might be skewed" );
    }
    return( DR_EMIT_DEFAULT );
}

void
trace_client::trace_events::fork_init( void *drcontext )
{
    
    /** initialize ipc buffer **/
    if( trace_client::trace_events::use_ipc_buffer )
    {
        trace_client::trace_events::ipc_buffer = 
            ipc::buffer::initialize( trace_client::trace_events::ipc_key_name );
        if( trace_client::trace_events::ipc_buffer == nullptr )
        {
            trace_client::error( __FILE__,
                                 __LINE__,
                                 std::cerr,
                                 "Failed to initialize IPC buffer" );
            exit( EXIT_FAILURE );
        }
    }
    trace_client::trace_events::event_thread_init( drcontext );
}


void
trace_client::trace_events::event_thread_init( void *drcontext )
{
    
    /**
     * NOTES: The control region should be there, it's already been
     * checked and verified by the 'dr_main' in traceclient.cpp. 
     * we could check again here, but why? Will add additional checks
     * if we have issues..but there shouldn't be any. 
     */

    auto *data = 
        reinterpret_cast< trace_client::tls_type* >( 
            dr_thread_alloc(  drcontext, 
                              sizeof( trace_client::tls_type ) ) );
    DR_ASSERT( data != nullptr );

    drmgr_set_tls_field( drcontext, 
                         trace_client::trace_events::tls_field,
                         data );
    /** 
     * set thread id
     */
    data->thread_id = trace_client::trace_events::get_tid();
    /* Keep seg_base in a per-thread data structure so we can get the TLS
     * slot and find where the pointer points to in the buffer.
     */
    data->seg_base = reinterpret_cast< byte* >( 
        dr_get_dr_segment_base( trace_client::trace_events::tls_seg ) 
    );

    //set base buffer here from ipc
#if 0   
    data->buf_base = (trace_client::ref_t*)
        dr_raw_mem_alloc( trace_client::trace_events::local_buffer_size, 
                          DR_MEMPROT_READ | DR_MEMPROT_WRITE, 
                          nullptr );
    std::memset( (void*)data->buf_base, 0x0, trace_client::trace_events::local_buffer_size );
#endif


    
    DR_ASSERT( trace_client::trace_events::ipc_buffer != nullptr );
    
    //open TLS structure for ipc buffer
    data->ipc_tls = ipc::buffer::get_tls_structure(
        trace_client::trace_events::ipc_buffer, 
        data->thread_id
    );
    DR_ASSERT( data->ipc_tls != nullptr );
    /**
     * wait till synchronization region is ready before adding new channels,
     * sync segment must be set up by the client that is initiating the transfer
     */
    ipc::buffer::has_channel( data->ipc_tls                         /** tls **/, 
                              trace_client::sync_segment_channel    /** fixed sync segment **/,
                              true                                  /** block till the channel is there **/ );
    
    if( ipc::buffer::add_shared_segment( data->ipc_tls,
                                         trace_client::sync_segment_channel ) == ipc::channel_err )
    {
        trace_client::error( __FILE__,
                             __LINE__,
                             std::cerr,
                             "Failed to add client synchronization channel, exiting!!" );
        DR_ASSERT( false );                                 
    }

    //open shared segment 
    if( ipc::buffer::open_shared_segment( data->ipc_tls,
                                          trace_client::sync_segment_channel,
                                          (void**)&data->sync_seg ) != ipc::tx_success )
    {
        trace_client::error( __FILE__,
                             __LINE__,
                             std::cerr,
                             "Failed to initialize client synchronization segment, exiting!!" );
        DR_ASSERT( false );                                 
    }
     
    /** tell downstream clients there are new threads waiting **/
    trace_client::shm_segment::add_new_thread( data->sync_seg );

    if( ipc::buffer::add_spsc_lf_record_channel( data->ipc_tls, 
                                                 data->thread_id,
                                                 ipc::producer ) == ipc::channel_err )
    {
        trace_client::error(    __FILE__, 
                                __LINE__, 
                                std::cerr, 
                                "failed to add channel" );
        DR_ASSERT( false );
    }
    void *output = nullptr;
    while( output == nullptr )
    {
        output = 
                 ipc::buffer::allocate_record( data->ipc_tls, 
                                               trace_client::trace_events::local_buffer_size,
                                               data->thread_id  /** channel_id **/ );
    }
    
    data->buf_base = (trace_client::ref_t*)output;
    
    /* put buf_base to TLS as starting buf_ptr */
    *trace_client::trace_events::buf_ptr< trace_client::ref_t >( data->seg_base ) = 
        data->buf_base;
    DR_ASSERT(data->seg_base != nullptr );
    DR_ASSERT(data->buf_base != nullptr );
}

void 
trace_client::trace_events::event_thread_exit(  void *drcontext ) 
{
    /** exit here **/
    /* dump any remaining buffer entries */
    trace_client::trace_events::trace( drcontext, true);
    auto *data = reinterpret_cast< trace_client::tls_type* >( 
        drmgr_get_tls_field(    drcontext, 
                                trace_client::trace_events::tls_field )
    );
    /* close logfile fd too */
    //send close signal
    void *output = nullptr;
    while( output == nullptr )
    {
        output = 
                 ipc::buffer::allocate_record( data->ipc_tls, 
                                               sizeof( trace_client::ref_t ),
                                               data->thread_id  /** channel_id **/ );
    }
    //last record, send one. 
    (*reinterpret_cast< trace_client::ref_t* >( output )).token.cmd = 
        trace_client::end_of_trace;
    while( ipc::buffer::send_record( data->ipc_tls, 
                                     data->thread_id  /** channel_id **/, 
                                     (void**)&output ) != ipc::tx_success );
    
    //remove thread
    trace_client::shm_segment::remove_thread(  data->sync_seg );
    ipc::buffer::unlink_channel( data->ipc_tls, data->thread_id );
    /**
     * release channel, close IPC TLS
     */
    ipc::buffer::close_tls_structure(          data->ipc_tls           );
    /**
     * close DR TLS
     */
    dr_thread_free( drcontext, 
                    data, 
                    sizeof( trace_client::tls_type) );
    return;
}

void
trace_client::trace_events::event_exit( void )
{
    /** put rest of shutdown code here **/
    if( ! dr_raw_tls_cfree( trace_client::trace_events::tls_offs, 
                            trace_client::trace_events::tls_slot_count )
      )
    {
        DR_ASSERT(false);
    }
    

    /**
     * when using 'ex' event registration, we have to use the corresponding
     * unregister functions. See documentation in 'group__drmgr.html'.
     */
    drmgr_unregister_bb_instrumentation_ex_event( 
        /** app2app func        **/
        trace_client::trace_events::event_bb_app2app,
        /** analysis_func       **/
        trace_client::trace_events::event_bb_analysis,                             
        /** insertion_event     **/
        trace_client::trace_events::event_instruction_callback, 
        /** instru2instru_func  **/ 
        trace_client::trace_events::event_bb_instru2instru
        );

    auto &stream = std::cerr;
    if( ! drmgr_unregister_tls_field( trace_client::trace_events::tls_field ) )
    {
        trace_client::error(    __FILE__, 
                                __LINE__, 
                                stream, 
                                "Failed to unregister TLS field" );
    }
    
    if( ! drmgr_unregister_thread_init_event( event_thread_init) )
    {
        trace_client::error(    __FILE__, 
                                __LINE__, 
                                stream, 
                                "Failed to unregister (event_thread_init)" );
    }
    if( ! drmgr_unregister_thread_exit_event(event_thread_exit) )
    {
        trace_client::error(    __FILE__, 
                                __LINE__, 
                                stream, 
                                "Failed to unregister (event_thread_exit)" );
    }

    if( drreg_exit() != DRREG_SUCCESS )
    {        
        DR_ASSERT(false);
    }
    drutil_exit();
    drmgr_exit();
    drx_exit();
    
    //destruct ipc buffer
    ipc::buffer::destruct( trace_client::trace_events::ipc_buffer, 
                           trace_client::trace_events::ipc_key_name, 
                           false /** receiver should unlink, not sender    **/,
                           false /** no unmap, causes crash, unmap on exit **/);
    
    return; /** void **/
}


    
/**
 * gotta keep this one as a passthrough for user-data
 */
dr_emit_flags_t
trace_client::trace_events::event_bb_analysis(  void *drcontext, 
                                                void *tag, 
                                                instrlist_t *bb, 
                                                bool for_trace, 
                                                bool translating, 
                                                void *user_data )
{
    UNUSED( drcontext   );
    UNUSED( tag         );
    UNUSED( bb          );
    UNUSED( for_trace   );
    UNUSED( translating );
    UNUSED( user_data   );
    return( DR_EMIT_DEFAULT );
}


/**
 * gotta keep this one as a passthrough for user-data
 */
dr_emit_flags_t
trace_client::trace_events::event_bb_instru2instru(   void *drcontext, 
                                                      void *tag, 
                                                      instrlist_t *bb, 
                                                      bool for_trace, 
                                                      bool translating, 
                                                      void *user_data )
{
    UNUSED( drcontext   );
    UNUSED( tag         );
    UNUSED( bb          );
    UNUSED( for_trace   );
    UNUSED( translating );
    UNUSED( user_data   );
    return( DR_EMIT_DEFAULT );
}


/**
 * this function basically kicks off everything you need
 * to get started, going with SHM to make it work initially
 * then we'll move on to other things. 
 */
void 
trace_client::initialize()
{

    const auto drmgr_init_ret( drmgr_init() );
    if( drmgr_init_ret == false )
    {
        trace_client::error( __FILE__, __LINE__ );
        exit( EXIT_FAILURE );
    }
    

    drreg_options_t ops = { sizeof( ops )       /** size of struct   **/,
                            3                   /** # of regs needed **/,
                            false               /** enable client to rely on values in case of fault **/,
                            nullptr             /** error callback, not going to provide one **/,
                            true                /** do not sum slots **/
                          };

    const auto drreg_init_ret( drreg_init( &ops ) );

    if( drreg_init_ret != DRREG_SUCCESS )
    {
        /** 
         * check follow-ups given this will kill our
         * chances for instrumentation.
         */
        /** set stream here so we don't have to do so over and over **/
        auto &stream = std::cerr;
        switch( drreg_init_ret )
        {
        
            case( DRREG_ERROR ):
            {
                trace_client::error( __FILE__, __LINE__, stream, "Operation failed" );
                break;
            }
            case( DRREG_ERROR_INVALID_PARAMETER ):
            {
                trace_client::error( __FILE__, __LINE__, stream, "Operation failed: invalid parameter" );
                break;
            }
            case(  DRREG_ERROR_FEATURE_NOT_AVAILABLE ):
            {
                trace_client::error( __FILE__, __LINE__, stream, "Operation failed: not available" );
                break;
            }
            case(  DRREG_ERROR_REG_CONFLICT ):
            {
                trace_client::error( __FILE__, __LINE__, stream, "Operation failed: register conflict" );
                break;
            }
            case(  DRREG_ERROR_IN_USE ):
            {
                trace_client::error( __FILE__, __LINE__, stream, "Operation failed: resource already in use" );
                break;
            }
            case(  DRREG_ERROR_OUT_OF_SLOTS ):
            {
                trace_client::error( __FILE__, __LINE__, stream, "Operation failed: no more TLS slots" );
                break;
            }
            case(  DRREG_ERROR_NO_APP_VALUE ):
            {
                std::stringstream ss;
                ss << "Operation failed: app value not available. ";
                ss << "Set conservative in drreg_options_t to avoid this error.";
                trace_client::error( __FILE__, __LINE__, stream, ss.str() );
                break;
            }
            default:
            {
                /** some other error code here, definitely not success **/
                 trace_client::error( __FILE__, __LINE__, stream );
            }
        }
        /** all cases above are definitely failure, so exit **/
        exit( EXIT_FAILURE );
    }

    if( ! drutil_init() )
    {
        trace_client::error(    __FILE__, 
                                __LINE__, 
                                std::cerr, 
                                "Failed to initialized drutil_init" );
        exit( EXIT_FAILURE );
    }
    if( ! drx_init() )
    {
        trace_client::error(    __FILE__, 
                                __LINE__, 
                                std::cerr, 
                                "Failed to initialized drx_init" );
        exit( EXIT_FAILURE );
    }
    
    dr_log(nullptr, DR_LOG_ALL, 1, "Client 'traceclient' done initializing\n");
    
    /**
     * callback registered for overall client exit
     */
    dr_register_exit_event( trace_client::trace_events::event_exit );
   
    /**
     * called on every thread exit 
     */
    const auto thread_exit_reg_ret( 
        drmgr_register_thread_exit_event( trace_client::trace_events::event_thread_exit )
    );
    if( thread_exit_reg_ret == false )
    {
        trace_client::error( __FILE__,
                             __LINE__,
                             std::cerr,
                             "Couldn't register thread exit callback function, fatal, exiting." );
        exit( EXIT_FAILURE );
    }
    /**
     * called on every thread initialization
     */
    const auto thread_init_reg_ret( 
        drmgr_register_thread_init_event( trace_client::trace_events::event_thread_init )
    );
    if( thread_init_reg_ret == false )
    {
        trace_client::error( __FILE__,
                             __LINE__,
                             std::cerr,
                             "Couldn't register thread init callback function, fatal, initing." );
        exit( EXIT_FAILURE );
    }

    
    dr_register_fork_init_event( 
        trace_client::trace_events::fork_init /** fork init event **/ 
    );
    
    /**
     * see documentation page group__drmgr.html for details on each function 
     * parameter. This is the main registration point for our instrumentation 
     * functions.
     * NOTES: 
     * - all below must be defined for this to actually work, carrier function
     *   will abort call and ret fail if any of the functions are null, which 
     *   is undocumented. All the other functions you can provide nullptr and
     *   that function call is skipped. 
     * - If the user wants to extend this and pass state throughout each of
     *   the function calls, they're called in order, the first function in 
     *   the series should allocate the data struct. Alloc using the dr intern
     *   function `dr_thread_alloc`.
     * - Last call in the  series ('instru2instru') should free this structure
     *   using 'dr_thread_free'.
     */
    const auto event_reg_ret = 
    drmgr_register_bb_instrumentation_ex_event( 
        trace_client::trace_events::event_bb_app2app            /** app2app func        **/,
        trace_client::trace_events::event_bb_analysis           /** analysis_func       **/,
        trace_client::trace_events::event_instruction_callback  /** insertion function  **/,
        trace_client::trace_events::event_bb_instru2instru      /**instru2instru_func   **/,
        nullptr /** priority **/ );
    if( event_reg_ret == false )
    {
        trace_client::error( __FILE__,
                             __LINE__,
                             std::cerr,
                             "Couldn't register instrumentation event" );
    }

    /**
     * register TLS fields
     */
    trace_client::trace_events::tls_field = drmgr_register_tls_field();
    if( trace_client::trace_events::tls_field == -1 )
    {
        trace_client::error( __FILE__,
                            __LINE__,
                            std::cerr,
                            "Failed to get a slot for the tls field, exiting with failure" );
        exit( EXIT_FAILURE );
    }
    

    /**
     * to facilitate a direct access TLS buffer, we need to use
     * `dr_raw_tls_calloc`, passing a tls_seg, tls_offset, then 
     * a count of number of slots we want, last param is an align
     * if 0 then auto alignment selected. tls_seg_reg (first param)
     * should be non-null, so that the function may set it. the offset
     * is set by yet another internal DR function call in ./unix/os.c
     * which is `os_tls_calloc`, this should have storage allcoated for
     * the target as well. 
     */
    const auto raw_tls_ret = 
        dr_raw_tls_calloc( &trace_client::trace_events::tls_seg, 
                           &trace_client::trace_events::tls_offs, 
                           trace_client::trace_events::tls_slot_count, 
                           0 );
    
    if( raw_tls_ret == false )
    {
        trace_client::error( __FILE__,
                            __LINE__,
                            std::cerr,
                            "Failed to get slots for raw tls" );
        DR_ASSERT( false );
    }
    
    if( trace_client::trace_events::use_ipc_buffer )
    {
        
        trace_client::trace_events::ipc_buffer = 
            ipc::buffer::initialize( trace_client::trace_events::ipc_key_name );
        if( trace_client::trace_events::ipc_buffer == nullptr )
        {
            trace_client::error( __FILE__,
                                 __LINE__,
                                 std::cerr,
                                 "Failed to initialize IPC buffer" );
            DR_ASSERT( false );
        }
    }
    //info message to the log to tell which client is executing
    dr_log(nullptr, DR_LOG_ALL, 1, "Client 'traceclient' done initializing\n");
    return;
}
