/**
 * reference_list.cpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "reference_list.hpp"
#include "reference.hpp"

#include <cassert>
    

trace_client::reference_list::reference_list()
{
    //record stays null
}

trace_client::reference_list::reference_list( void *reference_record ) : 
    record( reinterpret_cast< trace_client::ref_t* >( reference_record ) ),
    record_start_ptr( record )
{
    /**
     * NOTE: record is allowed to be null, you can 
     * set in the future with set_record.
     */
}

bool
trace_client::reference_list::is_end_of_list( trace_client::cmds &cmd )
{
    if( record == nullptr )
    {
        //by definition, empty is at end of list. 
        return( true );
    }
    cmd = record->token.cmd;
    return( cmd  == trace_client::cmd_not_set ||
            cmd  == trace_client::end_of_trace );
}

trace_client::ref_t&
trace_client::reference_list::ref()
{
    assert( record != nullptr );
    return( *record );
}

trace_client::ref_t& 
trace_client::reference_list::peek_next_ref()
{
    return( *(record + 1) );
}

void
trace_client::reference_list::inc()
{
    ++record;
}

void
trace_client::reference_list::set_record( void *r )
{
    assert( r != nullptr );
    record_start_ptr = reinterpret_cast< trace_client::ref_t* >( r );
    record           = reinterpret_cast< trace_client::ref_t* >( r );
}
    
trace_client::ref_t*    
trace_client::reference_list::get_raw_record()
{
    return( record_start_ptr );
}
