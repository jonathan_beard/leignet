/**
 * @version: 2022-09-17T00:00:00
 * @author: Jonathan Beard
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dr_api.h"
#include "droption.h"
#include "client.hpp"

static droption_t< int >   use_shm_ipc(
    DROPTION_SCOPE_CLIENT, 
    "use_shm_ipc", 
    1,
    "Use user-space shared memory IPC.",
    "Tell framework if we're planning to use inter-processor communication."
);


static droption_t< int > ipc_handle_name(
    DROPTION_SCOPE_CLIENT, 
    "ipc_handle", 
    42,
    "Use this as your ipc handle name, it must be an integer of some sort",
    "set custom IPC handle name/integer");

#ifdef __cplusplus
extern "C" 
{

DR_EXPORT void
dr_client_main( client_id_t id, int argc, const char **argv )
{
    if ( ! droption_parser_t::parse_argv(   DROPTION_SCOPE_CLIENT, 
                                            argc, 
                                            argv, 
                                            nullptr, 
                                            nullptr) )
    {
        DR_ASSERT(false);
    }
    trace_client::trace_events::client_id = id;
    
    if( use_shm_ipc.get_value() == 1 )       /** using native shm **/
    {
        /**
         * if we're here, we're not using a linux pipe to communicate,
         * it's shm of some type, use the shm functionality.
         */
        trace_client::trace_events::ipc_key_name    = ipc_handle_name.get_value();
        trace_client::trace_events::use_ipc_buffer  = true;
    }
    else                                /** using linux pipe **/
    {
        std::cerr << "using linux pipe deprecated, use SHM IPC instead, exiting!";
        exit( EXIT_FAILURE );
        /**
         * else we're using a linux pipe to communicate.
         */
        trace_client::trace_events::use_ipc_buffer  = false;
    }

    trace_client::initialize();
    return; /** void **/
}

}
#endif
