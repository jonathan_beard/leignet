/**
 * print.cpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstdint>
#include <cstdio>

#include <buffer>
#include <cstddef>
#include <cinttypes>
#include <cassert>
#include "reference.hpp"
#include "tracetype.hpp"
#include "utility.hpp"
#include "tls_buffer.hpp"

void 
trace_client::utility::print_references(    void *buffer_seg_start, 
                                            void *buffer_seg_end, 
                                            void *data,
                                            const bool use_ipc )
{
    auto *_data = (trace_client::tls_buffer*)(data);
    auto *trace_ref = (trace_client::ref_t*)buffer_seg_start;
    auto *trace_ref_end = (trace_client::ref_t*)buffer_seg_end;

    if( use_ipc )
    {
        auto length = std::distance( (char*)trace_ref, 
                                     (char*)trace_ref_end );
        //copy over data - will fix in a bit to single copy, just wanna get it workign
        void *output = nullptr;
        const auto alloc_length = length + sizeof( trace_client::ref_t);
        while( output == nullptr )
        {
            output = 
                     ipc::buffer::allocate_record( _data->ipc_tls, 
                                                   alloc_length,
                                                   _data->thread_id  /** channel_id **/ );
        }
        std::memcpy( output, trace_ref, length );
        
        reinterpret_cast< trace_client::ref_t* >( output )
        [ 
            std::distance( trace_ref, trace_ref_end )
        ].token.cmd = trace_client::cmd_not_set;
        
        while( 
            ipc::buffer::send_record( _data->ipc_tls, 
                                      _data->thread_id  /** channel_id **/, 
                                      (void**)&output ) != ipc::tx_success );
                                              
    }
    else
    {
        for( ;trace_ref < trace_ref_end; ++trace_ref )
        {
            trace_client::utility::print_record( (*trace_ref), data );
        }//end giant for loop
    
    }
}    

void 
trace_client::utility::print_record( const trace_client::ref_t &trace_ref, void *data )
{
    
    //didn't feel like changing all ptrs to refs for print test
    auto generic_print_insn = [&data]( const trace_client::ref_t &tr )
    {
          auto *_data = (trace_client::tls_buffer*)(data);
          fprintf( data == nullptr ? stdout : _data->logf,
                  "%" PRIu8 ", %" PRIu8 ", 0x%" PRIxPTR ", %" PRIu16 ", %s, %" PRIi32 "\n", 
                      tr.token.cmd,
                      tr.token.type,
                      tr.pc,
                      tr.op_size,
                      tr.op_name,
                      tr.token.op
                      );
    };

    auto generic_print_data_op = [&data]( const trace_client::ref_t &tr )
    {
          auto *_data = (trace_client::tls_buffer*)(data);
          fprintf( data == nullptr ? stdout : _data->logf,
              "%" PRIu8 ", %c, %" PRIu16 ", 0x%" PRIxPTR "\n", 
               tr.token.cmd,
               (tr.dir ? 'w' : 'r'),
               tr.mem_size,
               tr.addr
          );
          return;

    };
    switch( trace_ref.token.cmd )
    {
        case( trace_client::instruction ):
        {
            generic_print_insn( trace_ref );
        }
        break;
        case( trace_client::data_operation ):
        {
            generic_print_data_op( trace_ref );
        }
        break;
        case( trace_client::marker ):
        {
            generic_print_insn( trace_ref );
        }
        break;
        case( trace_client::cmd_not_set ):
        break;
        default:
        {
            //note, there's an "end of trace" token as well, shouldn't get here with that. 
            std::cerr << "invalid token found: " << (int)trace_ref.token.cmd << "\n";
            assert( false );
        }
    } /** end switch statement over trace_client::cmds **/
}    
    

std::ostream& operator << ( std::ostream &stream, const trace_client::ref_t &trace_ref )
{
    static constexpr auto buffer_length = 1024;
    char buffer[ buffer_length ];

    //didn't feel like changing all ptrs to refs for print test
    auto generic_print_insn = [&buffer]( const trace_client::ref_t &tr )
    {
          snprintf( buffer, buffer_length,
                  "%" PRIu8 ", %" PRIu8 ", 0x%" PRIxPTR ", %" PRIu16 ", %s, %" PRIi32 "", 
                      tr.token.cmd,
                      tr.token.type,
                      tr.pc,
                      tr.op_size,
                      tr.op_name,
                      tr.token.op
                      );
    };

    auto generic_print_data_op = [&buffer]( const trace_client::ref_t &tr )
    {
          snprintf( buffer, buffer_length,
              "%" PRIu8 ", %c, %" PRIu16 ", 0x%" PRIxPTR "", 
               tr.token.cmd,
               (tr.dir ? 'w' : 'r'),
               tr.mem_size,
               tr.addr
          );
          return;

    };
    switch( trace_ref.token.cmd )
    {
        case( trace_client::instruction ):
        {
            generic_print_insn( trace_ref );
        }
        break;
        case( trace_client::data_operation ):
        {
            generic_print_data_op( trace_ref );
        }
        break;
        case( trace_client::marker ):
        {
            generic_print_insn( trace_ref );
        }
        break;
        case( trace_client::cmd_not_set ):
        break;
        default:
        {
            //note, there's an "end of trace" token as well, shouldn't get here with that. 
            std::cerr << "invalid token found: " << (int)trace_ref.token.cmd << "\n";
            assert( false );
        }
    } /** end switch statement over trace_client::cmds **/
    stream << buffer;
    return( stream );
}
