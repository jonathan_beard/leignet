/**
 * shm_segment.cpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "shm_segment.hpp"
#include <atomic>
#include <new>
#include <iostream>
#include <cstdlib>
#include <cstdio>

trace_client::shm_segment::shm_segment()
{
    //nothing to do
}

trace_client::shm_segment::~shm_segment()
{
    //nothing to do 
}

void 
trace_client::shm_segment::init( void *ptr, const std::uint32_t num_cores )
{
    auto *seg = new (ptr) trace_client::shm_segment( );
    seg->num_cores = num_cores;
    seg->trace_threads_attached = 0;
    seg->new_thread_flag = 0;
}


void
trace_client::shm_segment::add_new_thread( trace_client::shm_segment *seg )
{
    seg->trace_threads_attached.fetch_add( 1, std::memory_order_release );
    seg->new_thread_flag.fetch_add( 1, std::memory_order_release );
}

void
trace_client::shm_segment::remove_thread( trace_client::shm_segment *seg )
{
    seg->trace_threads_attached.fetch_sub( 1, std::memory_order_acq_rel );
}
