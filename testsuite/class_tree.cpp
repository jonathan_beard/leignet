/**
 * class_tree.cpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <iostream>
#include <cstdint>
#include <cstdlib>

#include "base_node_list.hpp"
#include "getnodes.hpp"
#include "visitor.hpp"
#include "function_map.hpp"

int main()
{
    //need somewhere to hold the nodes, do here for now. 
    auto node_list = trace_parser::helper::get_nodes(); 
    trace_parser::function_map m;
    
    const auto call_ins_hashcode = 
         trace_parser::call_ins_node::token_hash; 

    const auto mem_scalar_hashcode = 
         trace_parser::mem_scalar_node::token_hash;
    
    const auto instruction_node_hashcode = 
         trace_parser::instruction_node::token_hash;

    const auto vscatterpf1dps_node_hashcode =
         trace_parser::vscatterpf1dps_node::token_hash;
    
    const auto monitorx_node_hashcode = 
         trace_parser::monitorx_node::token_hash;
    
    const auto wait_node_hashcode = 
         trace_parser::wait_node::token_hash;

    const auto cpu_state_node_hashcode = 
         trace_parser::cpu_state_node::token_hash;
  
    const auto mem_simd_node_hashcode = 
         trace_parser::mem_simd_node::token_hash;

    m.insert( mem_scalar_hashcode );
    m.insert( instruction_node_hashcode );
    m.insert( mem_simd_node_hashcode );
    
    for( auto val : m )
    {
        std::cout << "val: " << std::hex << val << std::dec << "\n";
    }
    
    const auto val_one = 
        trace_parser::node_base::relation_tree.get_closest_to( 
            mem_scalar_hashcode,
            m
        );

    if(val_one  != mem_scalar_hashcode )
    {
        std::cout << "mem_scalar, expected, self\n";
        std::cout << val_one << " found vs. expected " << mem_scalar_hashcode << 
            " for (" << mem_scalar_hashcode << ")\n";
        exit( EXIT_FAILURE );
    }
    const auto val_two = 
        trace_parser::node_base::relation_tree.get_closest_to( 
            vscatterpf1dps_node_hashcode,
            m
        );
    if(val_two  != mem_simd_node_hashcode )
    {
        std::cout << "vscatterpf1dps_node_hashcode, expected mem_simdn";
        std::cout << val_two << " found vs. expected " << mem_simd_node_hashcode << 
            " for (" << vscatterpf1dps_node_hashcode << ")\n";
        exit( EXIT_FAILURE );
    }
    
    const auto val_three = 
        trace_parser::node_base::relation_tree.get_closest_to( 
            monitorx_node_hashcode,
            m
        );
    
    if(val_three != instruction_node_hashcode )
    {
        std::cout << instruction_node_hashcode << " to " << cpu_state_node_hashcode << "\n";
        std::cout << cpu_state_node_hashcode << " to " <<  wait_node_hashcode << "\n";
        std::cout << wait_node_hashcode << " to " << monitorx_node_hashcode << "\n";
        std::cout << "mem_scalar: " << mem_scalar_hashcode << "\n";
        std::cout << "wait_node: " << wait_node_hashcode << "\n";
        std::cout << val_three << " found vs. expected " << instruction_node_hashcode << 
            " for (" << monitorx_node_hashcode << ")\n";
        exit( EXIT_FAILURE );
    }

    const auto val_four = 
        trace_parser::node_base::relation_tree.get_closest_to( 
            call_ins_hashcode,
            m );
    if( val_four != mem_scalar_hashcode )
    {
        std::cout << "failed\n";
    }

     
    std::cout << "success\n";
    return( EXIT_SUCCESS );
}
