/**
 * visitor_test.cpp - basically just ensures that the first op and 
 * last op are correct. 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstdlib>
#include <iostream>
#include <cstdint>
#include <sstream>
#include <iomanip>
#include <string>
#include <unistd.h>
#include <cstring>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstdint>
#include <cstddef>
#include <functional>
#include <buffer>
#include <shm>
#include <thread>
#include <unordered_map>
#include <queue>
#include <memory>
#include <utility>
#include <fstream>

#include "utility.hpp"
#include "tracedefs.hpp"
#include "reference.hpp"
#include "reference_list.hpp"
#include "shm_segment.hpp"
#include "getnodes.hpp"
#include "trace_visitor.hpp"


#ifndef gettid
#include <unistd.h>
#include <sys/syscall.h>
#define gettid() syscall(SYS_gettid)
#endif

#define DEBUG 1 
using thread_list_t = std::unordered_map< pid_t, 
                                          std::unique_ptr< trace_client::reference_list > >;

#if DEBUG
static void print_grid( std::int64_t *grid, std::ostream &stream )
{
    /**
     * don't care about perf for printing, so, minimize lines 
     * of code.
     */
    std::stringstream left;
    std::stringstream right;
    auto printer = [](  std::stringstream &in, 
                        std::int64_t *g, 
                        std::size_t  start, 
                        const bool p_middle )
    
    {
        in << (p_middle ? "page left: \t\t\t\t\t\n" : "page right: \t\t\t\n" );
        for( std::size_t  i = start; i < start + 512; i++ )
        {
            in << std::setw(5 ) << g[ i ];
            if( (i+1) % 8 == 0 && i != 0){ in << (p_middle ? "  *  \n" : "\n"); }
        }
    };
    printer( left,  grid, 0   , true);
    printer( right, grid, 512 , false );
    std::string line_left, line_right;
    while( std::getline( left, line_left) && std::getline( right, line_right) )
    {
        stream << line_left << line_right << "\n";
    }
    return;
}
#endif

static void worker( int val )
{
    std::ofstream ofs( "/dev/null" );
    std::int64_t *grid( nullptr );
    if( posix_memalign( (void**)&grid, (1<<12), (1<<13) ) != 0 )
    {
        std::cerr << "failed to allocate memory, exiting\n";
        exit( EXIT_FAILURE );
    }
    assert( grid != nullptr );
    //now run through memory in a predictable pattern
    /**
     * # or * = 8B
     * ########********
     * ########********
     * ########********
     * ########********
     * ########********
     * ########********
     * ########********
     * ########********
     * --32----########
     * ********########
     * ********########
     * ********########
     * ********########
     * ********########
     * ********########
     * ********########
     * --64----########
     */
    for( int i = 0, j = 512 ; i < (8*32); i++, j++ )
    {
        grid[ i ] = i + val;
        grid[ j ] = j - val;
        //low drag way of keeping these from being compiled out
        __asm__ volatile( "nop \n\
                           nop \n\
                           nop \n\
                           nop" : /** output **/ : "m" (grid[i]) /** input **/, "m" (grid[j]) : /** clobber **/ ); 
    }
#if DEBUG    
    print_grid( grid, ofs );
#endif    
    ofs.close();
    free( grid );
    return;
}


int main( int argc, char **argv, char **envp )
{
    shm_key_t ipc_handle;
    shm::gen_key( ipc_handle, 42);
    auto num_threads = std::thread::hardware_concurrency();
    if( argc == 2 )
    {
        auto val = atoi( argv[ 1 ] );
        std::vector< std::thread* > threads;
        threads.reserve( num_threads );
        for( decltype( num_threads ) thread_id = 0; thread_id < num_threads; thread_id++ )
        {
            threads.push_back( new std::thread( worker, val + thread_id ) );
        }
        for( auto *th : threads )
        {
            th->join();
        }
        exit( EXIT_SUCCESS );
    }
    auto child = fork();
    switch( child )
    {
        case( 0 /** child **/ ):
        {   
            static const auto dr_bin_path( "/bin64/drrun" );
            static const std::string dr_client_flag( "-c" );
            static const std::string  client_path( DR_CLIENT_PATH );
            auto *directory = getcwd( nullptr, 0 );
            
            std::stringstream application;
            application << DR_DIR_PATH << dr_bin_path;
            std::stringstream client;
            client << "/" << client_path;
            
            std::stringstream test_app;
            test_app << directory << "/" << argv[ 0 ];
            /**
             * instrumentation can blow-up basic-block length, so we set these
             * so it doesn't get carried away, 64 seems to be a good number.
             */
            //-max_bb_instrs
            //-max_trace_bbs

            /**
             * args
             *   0 - application name (dynamorio) - application.str().c_str();
             *   1 - -max_bb_instrs 
             *   2 - 64
             *   3 - -max_trace_bbs
             *   4 - 64
             *   5 - "-c" - dr_client_flag
             *   6 - client - client.str().c_str()
             *   7 - "-ipc_handle" - to tell the app that the next arg is the handle
             *   8 - varg - thehandle
             *   9 - "--" - separator 
             *  10 - test_app - test_app.str().c_str()
             *  11 - '1' - dummy argument for client to know it's the forked one
             *  12 - '\0'
             */
             char **args = nullptr;
             args = (char **) malloc( sizeof( char** ) * 13 );
             //MEMORY LEAK, but...we're exec'ing, so, not like it'll stick around
             args[ 0  ] = strdup( application.str().c_str() );
             args[ 1  ] = strdup( "-max_bb_instrs" );
             args[ 2  ] = strdup( "64" );
             args[ 3  ] = strdup( "-max_trace_bbs" );
             args[ 4  ] = strdup( "64" );
             args[ 5  ] = strdup( dr_client_flag.c_str() );
             args[ 6  ] = strdup( client.str().c_str() );
             args[ 7  ] = strdup( "-ipc_handle" );
             args[ 8  ] = strdup( std::to_string( ipc_handle ).c_str() );
             args[ 9  ] = strdup( "--" );
             args[ 10 ] = strdup( test_app.str().c_str() );
             args[ 11 ] = strdup( "1" );
             args[ 12 ] = nullptr; 
             
             if( execve( application.str().c_str() /** dynamorio **/,
                     (char * const*) args,
                     envp ) == -1 )
             {
                std::cerr << "something really bad has happened, exit\n";
                exit( EXIT_FAILURE );
             }
                    
        }
        break;
        case( -1 /** error, back to parent **/ ):
        {
            std::cerr << "error forking, exiting with failure\n";
            exit( EXIT_FAILURE );
        }
        break;
        default:
        {
            thread_list_t           thread_list;
            std::queue< pid_t >     thread_removal_queue;
            static ipc::buffer *buffer          = nullptr;
            std::ofstream ofs( "/dev/null" );
            //need somewhere to hold the nodes, do here for now. 
            auto node_list = trace_parser::helper::get_nodes(); 
            trace_parser::trace_visitor v;
            v.pre_process_tree( node_list ); 
            
            ipc::buffer::register_signal_handlers(); 
            buffer = ipc::buffer::initialize( ipc_handle );
            //get TLS structure to use 
            auto *trace_tool_tls =  ipc::buffer::get_tls_structure( buffer, gettid() );
            
            auto cleanup = [&]()
            {
                ipc::buffer::close_tls_structure( trace_tool_tls );
                ipc::buffer::destruct( buffer, ipc_handle );
            };

            /** bind function so it matches the correct function signature for init **/
            auto init_function = 
                std::bind( trace_client::shm_segment::init,
                           std::placeholders::_1,
                           num_threads );


            /** set up shared memory segment **/
            if( ipc::buffer::add_shared_segment( trace_tool_tls,
                                                 trace_client::sync_segment_channel,
                                                 sizeof( trace_client::shm_segment ),
                                                 init_function ) == ipc::channel_err )
            {
                std::cerr << "channel error in consumer\n";
                assert( false );
            }
            trace_client::shm_segment *shared_seg = nullptr;
            if( ipc::buffer::open_shared_segment( trace_tool_tls, 
                                                  trace_client::sync_segment_channel, 
                                                  (void**)&shared_seg ) != ipc::tx_success )
            {
                std::cerr << "failed to open shared segment in consumer\n";
                assert( false );
            }

            
            /** spin to wait for first thread, then enter loop below **/
            std::uint32_t flag_value = 0;
            while( flag_value  == 0 )
            {
                flag_value = shared_seg->new_thread_flag.load();
            }
            bool threads_available = true;
            /**
             * basically this will be true as long as master 
             * thread in traced application is running, there
             * will always be at least one thread running. We'll
             * set thread_available = false once the thread_list
             * is emptied. 
             */
            while( threads_available )
            {
                if( shared_seg->new_thread_flag.load() > 0 )
                {
                    /** if shared memory channel says new thread then  **/
                    auto channel_list = ipc::buffer::get_channel_list( trace_tool_tls );
                    /** now see which channels to add **/
                    for( auto &p : (*channel_list ) )
                    {
                        /** we currently only care about these types of records **/
                        if( p.second == ipc::spsc_record )
                        {
                            const auto found_thread = thread_list.find( (pid_t) p.first );
                            if( found_thread == thread_list.end() )
                            {
                                //decrement from new_thread_flag list
                                shared_seg->new_thread_flag.fetch_sub( 1, 
                                                                       std::memory_order_acq_rel );
                                const auto thread_id_as_channel = (ipc::channel_id_t) p.first;
                                /** open channel **/
                                if( ipc::buffer::add_spsc_lf_record_channel( trace_tool_tls,
                                                                             thread_id_as_channel,
                                                                             ipc::consumer ) 
                                        < 0 )
                                        {
                                            std::cerr << "failed to add channel: " << 
                                                thread_id_as_channel << "\n";
                                        }
                                /** add to channel list **/
                                thread_list.insert(
                             std::make_pair( p.first /** thread id **/,
                                             std::make_unique< trace_client::reference_list >() )
                                );
                            
                            } /** end found **/
                        } /** end spsc record check **/
                    } /** end channel list loop **/
                }
                
                /** right now do one record for each thread **/ 
                for( auto &thread_trace : thread_list )
                {
                    void *record = nullptr;
                    auto status = ipc::tx_success;
                    if( (status = ipc::buffer::receive_record( trace_tool_tls, 
                                                     thread_trace.first, 
                                                     &record ) ) == ipc::tx_success )
                    {
                    
                        auto &ref_list = (*thread_trace.second);
                        ref_list.set_record( record ); 
                        trace_client::cmds curr_cmd = trace_client::cmd_not_set;
                        while( ! ref_list.is_end_of_list( curr_cmd ) )
                        {
                            auto &data = ref_list.ref();
                            auto found_node = node_list.find( data.token.all );
                            if( found_node != node_list.end() )
                            {
                                ofs << thread_trace.first << ": ";
                                v.visit_instruction( *(*found_node).second /** node **/, 
                                                     data,
                                                     ref_list,
                                                     &ofs );  
                            }
                            ref_list.inc();
                        }
                        if( curr_cmd == trace_client::end_of_trace )
                        {
                            //add thread to thread list to be removed, can't do while iterating
                            thread_removal_queue.emplace( thread_trace.first );
                        }
                        ipc::buffer::free_record( trace_tool_tls, record /** curr record **/ );
                    }
                
                }
                /** if no more instructions, e.g., end, remove from thread list **/
                while( thread_removal_queue.size() > 0 )
                {
                    const auto tid = thread_removal_queue.front();
                    thread_list.erase( tid );
                    thread_removal_queue.pop();
                    ipc::buffer::unlink_channel( trace_tool_tls, tid );
                    /** 
                     * only need to update threads_available here, no other place,
                     * you'll always have at least one thread running in profiled app,
                     * otherwise it's not running anymore. 
                     **/
                    threads_available = (thread_list.size() > 0);
                }
            } /** end worker while loop, threads_available controls **/
            /**
             * technically this should just return, but just in case, wait till 
             * child  has finished then exit.
             */
            int status( 0 );
            waitpid( -1, &status, 0 );
            cleanup();
            fprintf( stderr, "done\n" );
            ofs.close();
        } /** end parent switch code **/
    }

    return( EXIT_SUCCESS );
}
