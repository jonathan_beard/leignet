/**
 * visitor_test.cpp - basically just ensures that the first op and 
 * last op are correct. 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstdlib>
#include <iostream>
#include <cstdint>
#include <sstream>
#include <iomanip>
#include <string>
#include <unistd.h>
#include <cstring>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstdint>
#include <cstddef>
#include <functional>
#include <buffer>
#include <shm>
#include <fstream>
#include "utility.hpp"
#include "tracedefs.hpp"
#include "reference.hpp"
#include "reference_list.hpp"
#include "shm_segment.hpp"
#include "getnodes.hpp"
#include "trace_visitor.hpp"

#ifndef gettid
#include <unistd.h>
#include <sys/syscall.h>
#define gettid() syscall(SYS_gettid)
#endif


static ipc::buffer *buffer          = nullptr;

#if DEBUG
static void print_grid( std::int64_t *grid, std::ostream &stream )
{
    /**
     * don't care about perf for printing, so, minimize lines 
     * of code.
     */
    std::stringstream left;
    std::stringstream right;
    auto printer = [](  std::stringstream &in, 
                        std::int64_t *g, 
                        std::size_t  start, 
                        const bool p_middle )
    
    {
        in << (p_middle ? "page left: \t\t\t\t\t\n" : "page right: \t\t\t\n" );
        for( std::size_t  i = start; i < start + 512; i++ )
        {
            in << std::setw(5 ) << g[ i ];
            if( (i+1) % 8 == 0 && i != 0){ in << (p_middle ? "  *  \n" : "\n"); }
        }
    };
    printer( left,  grid, 0   , true);
    printer( right, grid, 512 , false );
    std::string line_left, line_right;
    while( std::getline( left, line_left) && std::getline( right, line_right) )
    {
        stream << line_left << line_right << "\n";
    }
    return;
}
#endif

static void worker( int val )
{
    std::int64_t *grid( nullptr );
    if( posix_memalign( (void**)&grid, (1<<12), (1<<13) ) != 0 )
    {
        std::cerr << "failed to allocate memory, exiting\n";
        exit( EXIT_FAILURE );
    }
    //now run through memory in a predictable pattern
    /**
     * # or * = 8B
     * ########********
     * ########********
     * ########********
     * ########********
     * ########********
     * ########********
     * ########********
     * ########********
     * --32----########
     * ********########
     * ********########
     * ********########
     * ********########
     * ********########
     * ********########
     * ********########
     * --64----########
     */
    for( int i = 0, j = 512 ; i < (8*32); i++, j++ )
    {
        grid[ i ] = i + val;
        grid[ j ] = j - val;
        //low drag way of keeping these from being compiled out
        __asm__ volatile( "nop \n\
                           nop \n\
                           nop \n\
                           nop" : /** output **/ : "m" (grid[i]) /** input **/, "m" (grid[j]) : /** clobber **/ ); 
    }
#if DEBUG    
    print_grid( grid, std::cout );
#endif    
    free( grid );
    return;
}


int main( int argc, char **argv, char **envp )
{
    shm_key_t ipc_handle;
    shm::gen_key( ipc_handle, 42);

    if( argc == 2 )
    {
        auto val = atoi( argv[ 1 ] );
        for( auto i = 0; i < 10; i++ )
        {
            worker( val );
        }
        exit( EXIT_SUCCESS );
    }
    auto child = fork();
    switch( child )
    {
        case( 0 /** child **/ ):
        {   
            static const auto dr_bin_path( "/bin64/drrun" );
            static const std::string dr_client_flag( "-c" );
            static const std::string  client_path( DR_CLIENT_PATH );
            auto *directory = getcwd( nullptr, 0 );
            
            std::stringstream application;
            application << DR_DIR_PATH << dr_bin_path;
            std::stringstream client;
            client << "/" << client_path;
            
            std::stringstream test_app;
            test_app << directory << "/" << argv[ 0 ];
            /**
             * instrumentation can blow-up basic-block length, so we set these
             * so it doesn't get carried away, 64 seems to be a good number.
             */
            //-max_bb_instrs
            //-max_trace_bbs

            /**
             * args
             *   0 - application name (dynamorio) - application.str().c_str();
             *   1 - -max_bb_instrs 
             *   2 - 64
             *   3 - -max_trace_bbs
             *   4 - 64
             *   5 - "-c" - dr_client_flag
             *   6 - client - client.str().c_str()
             *   7 - "-ipc_handle" - to tell the app that the next arg is the handle
             *   8 - varg - thehandle
             *   9 - "--" - separator 
             *  10 - test_app - test_app.str().c_str()
             *  11 - '1' - dummy argument for client to know it's the forked one
             *  12 - '\0'
             */
             char **args = nullptr;
             args = (char **) malloc( sizeof( char** ) * 13 );
             //MEMORY LEAK, but...we're exec'ing, so, not like it'll stick around
             args[ 0  ] = strdup( application.str().c_str() );
             args[ 1  ] = strdup( "-max_bb_instrs" );
             args[ 2  ] = strdup( "64" );
             args[ 3  ] = strdup( "-max_trace_bbs" );
             args[ 4  ] = strdup( "64" );
             args[ 5  ] = strdup( dr_client_flag.c_str() );
             args[ 6  ] = strdup( client.str().c_str() );
             args[ 7  ] = strdup( "-ipc_handle" );
             args[ 8  ] = strdup( std::to_string( ipc_handle ).c_str() );
             args[ 9  ] = strdup( "--" );
             args[ 10 ] = strdup( test_app.str().c_str() );
             args[ 11 ] = strdup( "1" );
             args[ 12 ] = nullptr; 
             
             if( execve( application.str().c_str() /** dynamorio **/,
                     (char * const*) args,
                     envp ) == -1 )
             {
                std::cerr << "something really bad has happened, exit\n";
                exit( EXIT_FAILURE );
             }
                    
        }
        break;
        case( -1 /** error, back to parent **/ ):
        {
            std::cerr << "error forking, exiting with failure\n";
            exit( EXIT_FAILURE );
        }
        break;
        default:
        {

            //need somewhere to hold the nodes, do here for now. 
            auto node_list = trace_parser::helper::get_nodes(); 
            std::ofstream ofs( "/dev/null" );
            trace_parser::trace_visitor v;
            v.pre_process_tree( node_list ); 
            
            ipc::buffer::register_signal_handlers(); 
            buffer = ipc::buffer::initialize( ipc_handle );
            //get TLS structure to use 
            auto *tls_consumer =  ipc::buffer::get_tls_structure( buffer, gettid() );
            
            auto cleanup = [&]()
            {
                ipc::buffer::close_tls_structure( tls_consumer );
                ipc::buffer::destruct( buffer, ipc_handle );
            };

            /** bind function so it matches the correct function signature for init **/
            auto init_function = 
                std::bind( trace_client::shm_segment::init,
                           std::placeholders::_1,
                           8 );


            /** set up shared memory segment **/
            if( ipc::buffer::add_shared_segment( tls_consumer,
                                                 trace_client::sync_segment_channel,
                                                 sizeof( trace_client::shm_segment ),
                                                 init_function ) == ipc::channel_err )
            {
                std::cerr << "channel error in consumer\n";
                assert( false );
            }
            trace_client::shm_segment *shared_seg = nullptr;
            if( ipc::buffer::open_shared_segment( tls_consumer, 
                                                  trace_client::sync_segment_channel, 
                                                  (void**)&shared_seg ) != ipc::tx_success )
            {
                std::cerr << "failed to open shared segment in consumer\n";
                assert( false );
            }

            /** spin **/
            std::uint32_t flag_value = 0;
            while( flag_value  == 0 )
            {
                flag_value = shared_seg->new_thread_flag.load();
            }

            /** now we're out, we need to start checking the channels we have **/
           

            auto channel_list = ipc::buffer::get_channel_list( tls_consumer );
            //for this test should be size == 1
            if( channel_list->size() != 2 )
            {
                std::cerr << "incorrect channel list size \"" << channel_list->size() << "\"\n";
                for( auto &p : (*channel_list) )
                {
                    std::cerr << p.first << " - " << ipc::channel_type_names[ p.second ] << "\n";
                }
                kill( child, SIGKILL );
                int status( 0 );
                waitpid( -1, &status, 0 );
                cleanup();
                exit( EXIT_FAILURE );
            }
            /**
             * else, we could loop over every channel, but it's only one right now, 
             * let's just grab it and use it. 
             */
            const auto our_data_channel = (*++channel_list->begin()).first;
            void *record    = nullptr;

            auto channel_status = ipc::tx_success;
            if( ipc::buffer::add_spsc_lf_record_channel( tls_consumer, 
                                                         our_data_channel, 
                                                         ipc::consumer ) == ipc::channel_err )
            {
                std::cerr << "failed\n";
                exit( EXIT_FAILURE );
            }

            //keep trying to receive data until
            bool keep_going = true;
            
            auto  ref_list     = trace_client::reference_list( record );
            while( keep_going )
            {
                if( (channel_status = ipc::buffer::receive_record( tls_consumer, 
                                                    our_data_channel, 
                                                    &record )) == ipc::tx_success )
                {
                
                    ref_list.set_record( record );
                    trace_client::cmds cmd;
                    while( ! ref_list.is_end_of_list( cmd /** don't need this for now **/ ) )
                    {
                        auto &data = ref_list.ref();
                        auto found_node = node_list.find( data.token.all );
                        if( found_node != node_list.end() )
                        {
                            v.visit_instruction( *(*found_node).second /** node **/, 
                                                 data,
                                                 ref_list,
                                                 &ofs );  
                        }
                        else
                        {
                            //std::cerr << "ERROR: didn't capture (" << std::hex << data.token.all << std::dec << ") -> ";
                            //trace_client::utility::print_record( data );
                        }
                        //increment to next instruction 
                        ref_list.inc();
                    }

                    ipc::buffer::free_record( tls_consumer, record );
                }
                bool has_producers = ipc::buffer::channel_has_producers( tls_consumer, 
                                                                         our_data_channel );

                const std::size_t s = ipc::buffer::channel_has_data( tls_consumer, 
                                                                     our_data_channel );
                keep_going = has_producers || (s > 0 );
            }                    
            //parent, but no need to wait for child 
            cleanup();
            fprintf( stderr, "done\n" );
            ofs.close();
        }
    }

    return( EXIT_SUCCESS );
}
