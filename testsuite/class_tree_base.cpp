/**
 * class_tree_base.cpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <array>
#include <set>

#include "class_tree.hpp"

int main()
{
    trace_parser::class_tree tree;
    std::array< size_t, 5 > arr = {1,2,3,4,5};
    std::set< size_t > set;
    set.insert( arr.begin(), arr.end() );
    tree.add_relation( 1 , 2 );
    tree.add_relation( 1, 3 );
    tree.add_relation( 1, 4 );
    tree.add_relation( 0, 1 );
    tree.add_relation( 0, 5 );
    tree.add_relation( 9 , 10 );
    tree.add_relation( 5, 8 );
    tree.add_relation( 5, 9 );

    auto good_a( tree.get_closest_to( 10, set ) );
    size_t ans_a( 5 );
    auto good_b( tree.get_closest_to( 2, set ) );
    size_t ans_b( 2 );
    auto good_c( tree.get_closest_to( 1, set ) );
    size_t ans_c( 1 );
    auto bad_d( tree.get_closest_to( 11 , set ) );
    size_t ans_d( 0 );

    std::cout << "Closest to 10 ( 5 ) : " << good_a << "\n";
    std::cout << "Closest to 2  ( 2 ) : " << good_b << "\n";
    std::cout << "Closest to 1  ( 1 ) : " << good_c << "\n";
    std::cout << "Closest to 11 ( 0 ) : " << bad_d  << "\n";
    
    if( ans_a != good_a )
    {
        exit( EXIT_FAILURE );
    }
    if( ans_b != good_b )
    {
        exit( EXIT_FAILURE );
    }
    if( ans_c != good_c )
    {
        exit( EXIT_FAILURE );
    }
    if( ans_d != bad_d )
    {
        exit( EXIT_FAILURE );
    }

    return( EXIT_SUCCESS );
}
