# README

Leignet was first built as a bridge to connect multiple simulation tools,
this second revision adds a trace tool framework based on both Leignet
and an open source tool, [DynamoRIO](https://dynamorio.org). Instead
of autogenerating the trace format, this revision aims to speed construction
of useful trace tools through a visitor pattern. This allows the 
programmer to focus on the tool vs. writing thousands of lines of 
switch statements to parse out specific classes of instructions. For
how this works, please see the documentation. 

## Instructions to build
### Prerequisite software
```
pkg-config cmake ninja-build build-essential
``` 
### Build instructions
* Build dynamorio for the platform you're on, checkout the [DynamoRIO source](https://dynamorio.org) somewhere and build (e.g., dr-build directory described next).  
``` 
cd dr-build && cmake <path to dynamorio> -Wno-dev -GNinja -DCMAKE_BUILD_TYPE=<Release|Debug> -DBUILD_DOCS=0 && ninja
```

* Now move to the leignet directory.
```
cd ..
```
* Now build the leignet and the traceclient itself (and test cases)
```
cd build
cmake ../ -GNinja -Wno-dev -DDR_BUILD_DIR=`pwd`/../dr-build
ninja
```

## To use
- see [usage](docs/usage.md)


## Basic idea
- You have DynamoRIO which is an open-source dynamic binary instrumentation tool,
- This library/tool set implements a trace tool that writes into a 
use-space IPC lock-free buffer that uses a fixed format (unlike the first 
version of leignet, however, future versions will likely re-enable autogeneration 
of the format). That buffer enables
multi-threading, multi-process on both the sender/receiver side. 
- We implement a trace parser class that enables the user to only implement
the instructions that they want, in as much detail as they want while ignoring
the rest. This is done using the visitor pattern. For details on this see 
```<base>/parser/visitor```. 
- There is a basic visitor now that simply prints out the trace, however, 
you can do much more with it. 
- There is also a "driver" class object that can be linked and used that 
abstracts all the IPC buffer management. Enabling the user to write a 
pretty powerful tool in just a few lines of code (see usage). 

## To define new instructions
- The instructions for this trace tool are captured using a "visitor" pattern. 
- Visitors, nodes, and relationships between nodes are defined in the ```parser```
subdirectory.
- For adding new version of an ISA, see documentation in ```<base>/parser/node/```.


### Nodes
- see [nodes](docs/nodes.md)

### Visitors
- see [visitors](docs/visitors.md)

### IPC Buffer
- see [ipc buffer basics](docs/ipc.md)
