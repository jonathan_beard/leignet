/**
 * driver.hpp - The driver is kindof like the kernel, it's scheduling trace threads 
 * to simulated cores and generally keeping things going every tick. Within the 
 * simulated package itself, this class basically
 * runs and blocks till no more trace thread data is incoming. 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DRIVER_HPP
#define DRIVER_HPP  1

#include <cstdlib>
#include <buffer>
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include <memory>
#include <map>
#include <unordered_map>
#include <tuple>
#include <cstdint>
#include <queue>
#include <shm>
#include <mutex>

#include "reference_list.hpp"
#include "shm_segment.hpp"
#include "tracedefs.hpp"
#include "getnodes.hpp"

namespace trace_client
{

class driver
{
public:
    using thread_list_t = std::unordered_map< pid_t, 
                                              std::unique_ptr< trace_client::reference_list > >;

    
    /** basic constructor **/
    driver( const shm_key_t &given_ipc_handle );
    

    /** basic destructor **/
    virtual ~driver();


    /** 
     * base implementations will need access to the node list, 
     * we only want one copy. 
     */
    trace_parser::node_map_t& get_node_list();
         
    /** 
     * cleanup - helper function called when you basically 
     * want to shut things for this driver and the IPC bridge
     * down. 
     */
    void cleanup();

    /** 
     * init - function that initializes internal data structures
     * needed to synchronize multiple threads between DynamoRIO
     * and the client using this driver. This function blocks 
     * until the first (main) thread is running on the DynamoRIO
     * side. 
     */
    void init();

    /**
     * check_for_threads - checks the ipc buffer and shared segment
     * for new threads. If there are new threads, this function adds
     * those threads to the thread_list. This function is thread safe
     * in that it could be called from multiple threads safely (uses
     * a mutex.
     */
    void check_for_threads();

    driver::thread_list_t::iterator begin();

    driver::thread_list_t::iterator end();

    bool thread_has_next( const pid_t thread_id );

    bool get_next( const pid_t thread_id, 
                   trace_parser::node_base **n,
                   trace_client::ref_t     &ref );

    bool check_completed_threads();
    
private:
    shm_key_t               ipc_handle;
    thread_list_t           thread_list;
    std::queue< pid_t >     thread_removal_queue;
    ipc::buffer      *buffer          = nullptr;
    trace_parser::node_map_t              node_list;
    /**
     * Note: for right now, driver is single threaded, however, 
     * we could use the same interface and keep thread-local 
     * TLS for each worker thread...TBD. 
     */
    ipc::thread_local_data *trace_tool_tls     = nullptr; 
    /**
     * Note, if you go multithreaded with the driver, this will
     * probably be okay to have one of these, but beware of 
     * any non-atomic exchanges in the shared segment once 
     * open....
     * - KEEP IN MIND, when closing the shared segment, the 
     * main thread that opened it must also close it. An alternative
     * design decision is to have a driver TLS that is given to each
     * thread and each thread then opens/closes the shared segment. 
     */
    trace_client::shm_segment *shared_seg = nullptr;
    std::mutex                 thread_list_mutex;
    std::mutex                 thread_list_removal_mutex;
};  

} /** end namespace trace **/
#endif /* END DRIVER_HPP */
