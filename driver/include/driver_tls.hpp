/**
 * driver_tls.hpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DRIVER_TLS_HPP
#define DRIVER_TLS_HPP  1
namespace trace_client
{

struct driver_tls
{
    
    /** where the clock sync goes **/
    trace_client::shm_segment               *shared_segment =   nullptr;
    
    /** set in constructor **/
    const std::string                       the_ipc_handle;
    
    /**
     * ipc_tls - keeps local channel mapping, actual data from traces are 
     * kept in a global mutex guarded map. 
     */
    ipc::thread_local_data                  *ipc_tls        =   nullptr;
};

} /** end namespace trace_client **/

#endif /* END DRIVER_TLS_HPP */
