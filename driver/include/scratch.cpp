/**
 * @version: 2022-09-17T00:00:00
 * @author: Jonathan Beard
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


    
            add_thread_event_t  add_thread          = ladder_hills::driver::round_robin_add,
            remove_thread_event_t   remove_thread   = ladder_hills::driver::simple_remove );

    /**
     * programmer definable "scheduling" function to add a thread
     * to a specific set of cores. The default one just does rr
     * to the next core without thought as to how things should
     * really be arranged. 
     */
    using add_thread_event_t    = std::function< void ( const pid_t, core_list& ) >;
    using remove_thread_event_t = std::function< bool ( const pid_t, core_list& ) >;

    static void round_robin_add( const pid_t trace_thread_id, core_list &core_list );

    static void simple_remove( const pid_t trace_thread_id, core_list &core_list );

    /**
     * each of these functions is provided in the constructor, 
     * they basically allow the ref_list to map to a core_id, so
     * when a new thread comes in, this registers the call back
     * and the core that its assigned to is updated. 
     */
    add_thread_event_t                      add_thread_f    = nullptr; 
    remove_thread_event_t                   remove_thread_f = nullptr;
