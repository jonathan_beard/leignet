/**
 * driver.cpp - 
 * @author: Jonathan Beard
 * @version: 2022-09-17T00:00:00
 * 
 * Copyright 2022 Arm Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "driver.hpp"
#include "getnodes.hpp"    
#include <buffer>

#ifndef gettid
#include <unistd.h>
#include <sys/syscall.h>
#define gettid() syscall(SYS_gettid)
#endif

trace_client::driver::driver( const shm_key_t &input_ipc_handle )
{
    //one of the few global variables, no sense having two of these
    node_list = trace_parser::helper::get_nodes();
    shm::key_copy( ipc_handle, input_ipc_handle );
    ipc::buffer::register_signal_handlers(); 
    buffer = ipc::buffer::initialize( ipc_handle );
    trace_tool_tls =  ipc::buffer::get_tls_structure( buffer, gettid() );
}

trace_client::driver::~driver()
{
    cleanup();
}

trace_parser::node_map_t& 
trace_client::driver::get_node_list()
{
    return( node_list );
}

void
trace_client::driver::cleanup()
{
    ipc::buffer::close_tls_structure( trace_tool_tls );
    ipc::buffer::destruct( buffer, ipc_handle );
    return;
}
            
void
trace_client::driver::init()
{
    /** bind function so it matches the correct function signature for init **/
    auto init_function = 
        std::bind( trace_client::shm_segment::init,
                   std::placeholders::_1,
                   0 );
    
    
    /** set up shared memory segment **/
    if( ipc::buffer::add_shared_segment( trace_tool_tls,
                                         trace_client::sync_segment_channel,
                                         sizeof( trace_client::shm_segment ),
                                         init_function ) == ipc::channel_err )
    {
        std::cerr << "channel error in consumer\n";
        assert( false );
    }
    if( ipc::buffer::open_shared_segment( trace_tool_tls, 
                                          trace_client::sync_segment_channel, 
                                          (void**)&shared_seg ) != ipc::tx_success )
    {
        std::cerr << "failed to open shared segment in consumer\n";
        assert( false );
    }
    
    
    /** spin to wait for first thread, then enter loop below **/
    std::uint32_t flag_value = 0;
    while( flag_value  == 0 )
    {
        flag_value = shared_seg->new_thread_flag.load( std::memory_order_relaxed );
    }
    return;
}

void
trace_client::driver::check_for_threads(  )
{
    /**
     * NOTES: 
     * - keep shared segment, left note in main driver.hpp file that
     * all access to this structure must be atomic, e.g., using std::atomic,
     * type access. 
     * - input param is a thread_local_data segment, initially this will be the
     * same for all. We'll also add a mutex around the thread_list. 
     */
    if( shared_seg->new_thread_flag.load() > 0 )
    {
        /** if shared memory channel says new thread then  **/
        auto channel_list = ipc::buffer::get_channel_list( trace_tool_tls );
        /** now see which channels to add **/
        for( auto &p : (*channel_list ) )
        {
            /** we currently only care about these types of records **/
            if( p.second == ipc::spsc_record )
            {
                //get lock for scope on thread_list
                std::lock_guard< std::mutex > lock( thread_list_mutex ); 
                const auto found_thread = thread_list.find( (pid_t) p.first );
                if( found_thread == thread_list.end() )
                {
                    //decrement from new_thread_flag list
                    shared_seg->new_thread_flag.fetch_sub( 1, 
                                                           std::memory_order_acq_rel );
                    const auto thread_id_as_channel = (ipc::channel_id_t) p.first;
                    /** open channel **/
                    if( ipc::buffer::add_spsc_lf_record_channel( trace_tool_tls,
                                                                 thread_id_as_channel,
                                                                 ipc::consumer ) < 0 )
                    {
                        std::cerr << "failed to add channel: " << 
                            thread_id_as_channel << "\n";
                    }
                    /** add to channel list **/
                    thread_list.insert(
                 std::make_pair( p.first /** thread id **/,
                                 std::make_unique< trace_client::reference_list >() )
                    );
                } /** end found - thread_list lock cleared at end of scope **/
            } /** end spsc record check **/
        } /** end channel list loop **/
    }
    return;
}
    

trace_client::driver::thread_list_t::iterator trace_client::driver::begin()
{
    return( thread_list.begin() );
}

trace_client::driver::thread_list_t::iterator trace_client::driver::end()
{
    return( thread_list.end() );
}
    

bool 
trace_client::driver::thread_has_next( const pid_t thread_id )
{
    auto &thread_trace = thread_list[ thread_id ];
    auto &ref_list = *thread_trace;
    auto curr_cmd = trace_client::cmd_not_set;
    if( ! ref_list.is_end_of_list( curr_cmd ) )
    {
        return( true );
    }
    else if( curr_cmd != trace_client::end_of_trace )
    {
        void *record = ref_list.get_raw_record();
        if( record != nullptr )
        {
            /**
             * return previously allocated memory back to the 
             * buffer.
             */
            ipc::buffer::free_record( trace_tool_tls, 
                                      record /** curr record **/ );
        }
        auto status  = ipc::tx_success;
        if( (status = ipc::buffer::receive_record( trace_tool_tls, 
                                         thread_id,
                                         &record ) ) == ipc::tx_success )
        {
            ref_list.set_record( record ); 
        }
        //looks a bit odd but we just need  the current command set one more time
        ref_list.is_end_of_list( curr_cmd );
    }
    if( curr_cmd == trace_client::end_of_trace )
    {
        std::lock_guard< std::mutex > lock( thread_list_removal_mutex );
        thread_removal_queue.emplace( thread_id );
    }
    return( false );
}

bool
trace_client::driver::get_next( const pid_t             thread_id, 
                                trace_parser::node_base **n,
                                trace_client::ref_t     &ref )
{
    auto &ref_list = *thread_list[ thread_id ];
    auto &data = ref_list.ref();
    auto found_node = node_list.find( data.token.all );
    if( found_node != node_list.end() )
    {
        *n    = (*found_node).second;
        ref  = data;
        ref_list.inc();
        return( true );
    }
    //for some reason we don't have a node for this instruction, 
    //continue
    *n = nullptr;
    ref_list.inc();
    return( false );
}

bool
trace_client::driver::check_completed_threads()
{
    std::lock_guard< std::mutex > lock( thread_list_mutex ); 
    std::lock_guard< std::mutex > lock_2( thread_list_removal_mutex );
    while( thread_removal_queue.size() > 0 )
    {
        const auto tid = thread_removal_queue.front();
        thread_list.erase( tid );
        thread_removal_queue.pop();
        ipc::buffer::unlink_channel( trace_tool_tls, tid );
        /** 
         * only need to update threads_available here, no other place,
         * you'll always have at least one thread running in profiled app,
         * otherwise it's not running anymore. 
         **/
    }
    return( thread_list.size() > 0 );
}
